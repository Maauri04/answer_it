﻿using Answer_It.Models;
using Answer_It.ViewModels;
using IBM.WatsonDeveloperCloud.Conversation.v1;
using IBM.WatsonDeveloperCloud.Conversation.v1.Model;
using Negocio;
using Newtonsoft.Json;
using Plugin.TextToSpeech;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using XFWatsonDemo.Models;

namespace Answer_It
{
    public class WatsonActions
    {
        private static dynamic _context; //dynamic
        private static ConversationService Conversation;
        private static ObservableCollection<ChatMessage> Messages;
        private static bool verAtributosExtra;
        private List<MercadoLibreViewModel> mlViewModel;
        private MercadoLibreRequests ml;
        private List<Valoraciones> ListaValoracionesProducto = new List<Valoraciones>(); //Informacion que se muestra en la listview.
        private List<Recomendaciones> ListaRecomendacionesProducto = new List<Recomendaciones>(); //Informacion que se muestra en la listview.
        private List<Productos> ListaProductos = new List<Productos>();
        RestClient client = new RestClient();


        public async void SendMessageToWatson(string OutGoingText, string optionSelected, ObservableCollection<ChatMessage> _messages, ConversationService _conversation, bool EsBienvenida)
        {
            if (EsBienvenida)
            {
                if (_messages != null)
                {
                    Messages = _messages;
                }
                MessageRequest mr = new MessageRequest()
                {
                    Input = new InputData()
                    {
                        Text = "Hola"
                    },
                    Context = _context
                };

                var mensaje_saludo = new ChatMessage
                {
                    //Busco productos la primera vez, sin perfeccionar la búsqueda.
                    Text = "Hola " + App.Usuario.nombre + ", soy Watson. Fui entrenado para recomendar productos, o buscar las opiniones que otros usuarios han dejado sobre ellos.",
                    TitleText = null,
                    TitleIsVisible = false,
                    ImageHeight = 0,
                    ImageWidth = 0,
                    VerDetallesIsVisible = false,
                    IsIncoming = true,
                    MessageDateTime = DateTime.Now,
                    CellType = "Botcell_Message",
                    OptionsAreVisible = false,
                    ShowAtributesAreVisible = false,
                    ProductReviewsAreVisible = false,
                };
                Messages.Add(mensaje_saludo);
                CrossTextToSpeech.Current.Speak(mensaje_saludo.Text, null, 0.35f, 1.4f, 5);

                if (_conversation != null) //Si es la primera vez, inicializo la conversacion
                {
                    Conversation = _conversation;
                }

                await Task.Run(() =>
                {
                    var res = Conversation.Message("931458a7-ea61-442f-be41-7b36a7a0438e", mr);
                    if (_context == null)
                    {
                        _context = res.Context;
                    }
                    else
                    {
                        res.Context = _context;
                    }
                    OnWatsonMessagerecieved(JsonConvert.SerializeObject(res, Formatting.Indented), Messages);
                });
            }

            if (!string.IsNullOrEmpty(OutGoingText) || !string.IsNullOrEmpty(optionSelected))
            {
                if (string.IsNullOrEmpty(OutGoingText))
                {
                    OutGoingText = optionSelected;
                }
                if (_messages != null)
                {
                    Messages = _messages;
                }
                Messages.Add(new ChatMessage { Text = OutGoingText, IsIncoming = false, MessageDateTime = DateTime.Now });
                string temp;
                if (App.quiereBuscarUnProducto)
                {
                    temp = "Busco recomendaciones para este producto: " + OutGoingText; //Agrego "Busco recomendaciones para este producto: " para que el asistente reconozca que se trata de un producto.
                }
                else
                {
                    temp = OutGoingText;
                }

                if (App.quiereBuscarOpiniones)
                {
                    temp = "Quiero valoraciones para este producto: " + OutGoingText; 
                }
                else
                {
                    if (!App.quiereBuscarUnProducto)
                    {
                        temp = OutGoingText;
                    }
                }
                App.quiereBuscarOpiniones = false;
                App.quiereBuscarUnProducto = false;

                OutGoingText = string.Empty;
                MessageRequest mr = new MessageRequest()
                {
                    Input = new InputData()
                    {
                        Text = temp
                    },
                    Context = _context
                };

                if (_conversation != null) //Si es la primera vez, inicializo la conversacion
                {
                    Conversation = _conversation;
                }

                await Task.Run(() =>
                {
                    var res = Conversation.Message("931458a7-ea61-442f-be41-7b36a7a0438e", mr);
                    if (_context == null)
                    {
                        _context = res.Context;
                    }
                    else
                    {
                        res.Context = _context;
                    }
                    OnWatsonMessagerecieved(JsonConvert.SerializeObject(res, Formatting.Indented), Messages);
                });
            }
        }

        private void OnWatsonMessagerecieved(string data, ObservableCollection<ChatMessage> Messages)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                WatsonMessage message = JsonConvert.DeserializeObject<WatsonMessage>(data);

                for (int i = 0; i < message.Intents.Length; i++)
                {
                    if (message.Intents[i].IntentIntent.ToString() == "Search_Product" || message.Intents[i].IntentIntent.ToString() == "User_Wants_To_Buy" || message.Intents[i].IntentIntent.ToString() == "User_Doesnt_Know_The_Product")
                    {
                        App.quiereBuscarUnProducto = true;
                    }
                    if (message.Intents[i].IntentIntent.ToString() == "Buy_Product")
                    {
                        App.buscarProductoEnMeli = true;
                    }
                    if (message.Intents[i].IntentIntent.ToString() == "User_Not_Satisfied")
                    {
                        verAtributosExtra = true;
                        App.BuscarAtributosExtra = true;
                    }
                    if (message.Intents[i].IntentIntent.ToString() == "User_Wants_Opinions")
                    {
                        App.BuscarValoraciones = false;
                        App.buscarProductoEnMeli = false;
                        App.quiereBuscarUnProducto = false;
                        App.quiereBuscarOpiniones = true;
                    }
                    if (message.Intents[i].IntentIntent.ToString() == "Search_Opinions")
                    {
                        App.BuscarValoraciones = true;
                    }
                }

                if (App.buscarProductoEnMeli)
                {
                    ml = new MercadoLibreRequests();
                    mlViewModel = ml.GetProducts(Messages[Messages.Count - 1].Text); //Obtengo el ultimo mensaje del usuario (el producto que quiere)
                    var asd = 123;
                }

                var chatMessage = new ChatMessage
                {
                    IsIncoming = true,
                    MessageDateTime = DateTime.Now,
                    CellType = "Botcell_Message",
                };

                if (message.Output.Generic != null)
                {
                    foreach (var item in message.Output.Generic)
                    {
                        if (item.ResponseType.Equals("image"))
                        {
                            chatMessage.Image = item.Source.ToString();
                            chatMessage.ImageHeight = 100;
                            chatMessage.ImageWidth = 100;
                            chatMessage.OptionsAreVisible = false;
                            chatMessage.ShowAtributesAreVisible = false;
                            chatMessage.ProductReviewsAreVisible = false;
                        }
                        if (item.ResponseType.Equals("text"))
                        {
                            chatMessage.TitleText = null;
                            chatMessage.TitleIsVisible = false;
                            chatMessage.Text = item.Text;
                            chatMessage.ImageHeight = 0;
                            chatMessage.ImageWidth = 0;
                            chatMessage.VerDetallesIsVisible = false;
                            chatMessage.OptionsAreVisible = false;
                            chatMessage.ShowAtributesAreVisible = false;
                            chatMessage.ProductReviewsAreVisible = false;
                        }
                        if (item.ResponseType.Equals("option")) //Es una opcion
                        {
                            chatMessage.Text = item.Title; //El texto a mostrar del bot, se llama title en IBM cuando es una opcion.
                            chatMessage.TitleText = null;
                            chatMessage.TitleIsVisible = false;
                            chatMessage.ImageHeight = 0;
                            chatMessage.ImageWidth = 0;
                            chatMessage.VerDetallesIsVisible = false;
                            chatMessage.OptionsAreVisible = true;
                            chatMessage.ShowAtributesAreVisible = false;
                            chatMessage.ProductReviewsAreVisible = false;
                            foreach (var opt in item.Options)
                            {
                                chatMessage.OptionsList.Add(opt.Label);
                            }
                        }
                        if (item.ResponseType.Equals("suggestion")) //Hay una ambiguedad. El usuario eligira la intent y no el bot.
                        {
                            chatMessage.Text = item.Title;
                            chatMessage.TitleText = null;
                            chatMessage.TitleIsVisible = false;
                            chatMessage.ImageHeight = 0;
                            chatMessage.ImageWidth = 0;
                            chatMessage.VerDetallesIsVisible = false;
                            chatMessage.ShowAtributesAreVisible = false;
                            chatMessage.ProductReviewsAreVisible = false;
                        }
                    }
                    App.LastMessage = chatMessage;
                }

                if (verAtributosExtra)
                {
                    //Busco productos luego de la primera vez, ya mostrando atributos y perfeccionando la búsqueda.
                    var mensaje_completar_atributos = new ChatMessage
                    {
                        Text = "Lo entiendo, buscaré mejor. ¿Podrías completar algunas de estas características sobre el producto que buscas? ¡Sólo las que sepas!",
                        TitleText = null,
                        TitleIsVisible = false,
                        ImageHeight = 0,
                        ImageWidth = 0,
                        VerDetallesIsVisible = false,
                        IsIncoming = true,
                        MessageDateTime = DateTime.Now,
                        CellType = "Botcell_Message",
                        OptionsAreVisible = false,
                        ShowAtributesAreVisible = true,
                        ProductReviewsAreVisible = false,
                    };
                    Messages.Add(mensaje_completar_atributos);
                    CrossTextToSpeech.Current.Speak(mensaje_completar_atributos.Text, null, 0.35f, 1.4f, 5);
                    verAtributosExtra = false;
                }

                if (App.buscarProductoEnMeli)
                {
                    var mensaje_esto_encontre = new ChatMessage
                    {
                        //Busco productos la primera vez, sin perfeccionar la búsqueda.
                        Text = "Genial " + App.Usuario.nombre + ", esto fue lo que encontré",
                        TitleText = null,
                        TitleIsVisible = false,
                        ImageHeight = 0,
                        ImageWidth = 0,
                        VerDetallesIsVisible = false,
                        IsIncoming = true,
                        MessageDateTime = DateTime.Now,
                        CellType = "Botcell_Message",
                        OptionsAreVisible = false,
                        ShowAtributesAreVisible = false,
                        ProductReviewsAreVisible = false,
                    };
                    Messages.Add(mensaje_esto_encontre);

                    CrossTextToSpeech.Current.Speak(mensaje_esto_encontre.Text, null, 0.35f, 1.4f, 5);

                    //ChatMessage del producto de mercadolibre
                    foreach (var item in mlViewModel)
                    {
                        Messages.Add(new ChatMessage
                        {
                            ProductId = item.id,
                            IsIncoming = true,
                            TitleText = item.title,
                            TitleIsVisible = true,
                            VerDetallesIsVisible = true,
                            Text = "Su precio es de " + item.currency_id + " " + item.price + "\n" + "Es un usuario " + item.power_seller_status + " dentro del sitio. " + "\n" + "Tiene " + item.transactions_rating_positive + " de valoraciones positivas.",
                            MessageDateTime = DateTime.Now,
                            Image = item.thumbnail.Replace("http://", "https://"),
                            ImageHeight = 100,
                            ImageWidth = 100,
                            OptionsAreVisible = false,
                            CellType = "Botcell_Product",
                            ProductReviewsAreVisible = false,
                        });

                        //Cargo info extra para el popup de detalles
                        App.ProductDetails.Add(new MercadoLibreViewModel
                        {
                            id = item.id,
                            title = item.title,
                            power_seller_status = item.power_seller_status,
                            level_id = item.level_id,
                            transactions_completed = item.transactions_completed,
                            transactions_rating_positive = item.transactions_rating_positive,
                            price = item.price,
                            currency_id = item.currency_id,
                            available_quantity = item.available_quantity,
                            condition = item.condition,
                            thumbnail = item.thumbnail.Replace("http://", "https://"),
                            city_name = item.city_name,
                            state_name = item.state_name,
                            free_shipping = item.free_shipping,
                            permalink = item.permalink,
                            installments_quantity = item.installments_quantity,
                            installments_amount = item.installments_amount,
                            installments_rate = item.installments_rate,
                            installments_currency_id = item.installments_currency_id,
                            attributes = item.attributes
                        });
                    }
                    App.buscarProductoEnMeli = false;
                }

                if (App.BuscarValoraciones)
                {
                    App.buscarProductoEnMeli = false;
                    await CargarProductos(Messages[Messages.Count - 1].Text);
                    foreach (var item in ListaProductos)
                    {
                        await CargarValoraciones(); //Cargo las valoraciones para todos los productos que encontre.
                        await CargarRecomendaciones();
                    }
                    if (ListaProductos.Count > 0)
                    {
                        var mensaje_estos_productos_encontre = new ChatMessage
                        {
                            Text = "Genial " + App.Usuario.nombre + ", estos son los productos más similares que encontré.",
                            TitleText = null,
                            TitleIsVisible = false,
                            ImageHeight = 0,
                            ImageWidth = 0,
                            VerDetallesIsVisible = false,
                            IsIncoming = true,
                            MessageDateTime = DateTime.Now,
                            CellType = "Botcell_Message",
                            OptionsAreVisible = false,
                            ShowAtributesAreVisible = false,
                            ProductReviewsAreVisible = false,
                        };
                        Messages.Add(mensaje_estos_productos_encontre);

                        CrossTextToSpeech.Current.Speak(mensaje_estos_productos_encontre.Text, null, 0.35f, 1.4f, 5);

                        foreach (var item in ListaProductos)
                        {
                            double valoracionPromedio = CalcularRatingPromedio(item.id);
                            Messages.Add(new ChatMessage
                            {
                                ProductId = item.id.ToString(),
                                Text = "",
                                TitleText = null,
                                TitleIsVisible = false,
                                ImageHeight = 0,
                                ImageWidth = 0,
                                VerDetallesIsVisible = false,
                                IsIncoming = true,
                                MessageDateTime = DateTime.Now,
                                CellType = "Botcell_Message",
                                OptionsAreVisible = false,
                                ShowAtributesAreVisible = false,
                                ProductReviewsAreVisible = true,
                                TituloProductoValoracion = item.nombre,
                                TextoSinValIsVisible = valoracionPromedio == 0 ? true : false,
                                RatingProductoValIsVisible = valoracionPromedio == 0 ? false : true,
                                ValoracionProducto = (Math.Truncate(100 * valoracionPromedio) / 100),
                                CantValoraciones = ListaValoracionesProducto.Count(x => x.id_producto == item.id),
                                CantRecomendaciones = ListaRecomendacionesProducto.Count(x => x.id_producto_recomendado == item.id),
                                EsTendencia = ListaProductos.OrderByDescending(x => x.valoracion).Take(10).Where(x => x.id == item.id) != null ? true : false,
                                ImagenProductoValoracion = ImageSource.FromFile(await CargarFotoProducto(item.id))
                            });
                            App.BorrarImagenLocalmente(Messages[Messages.Count - 1].ImagenProductoValoracion);
                        }
                    }
                    else
                    {
                        var mensaje_no_encontre_productos_valoracion = new ChatMessage
                        {
                            Text = "Lo siento, no encontré ningún producto con ese nombre en la comunidad.",
                            TitleText = null,
                            TitleIsVisible = false,
                            ImageHeight = 0,
                            ImageWidth = 0,
                            VerDetallesIsVisible = false,
                            IsIncoming = true,
                            MessageDateTime = DateTime.Now,
                            CellType = "Botcell_Message",
                            OptionsAreVisible = false,
                            ShowAtributesAreVisible = false,
                            ProductReviewsAreVisible = false,
                        };
                        Messages.Add(mensaje_no_encontre_productos_valoracion);
                        CrossTextToSpeech.Current.Speak(mensaje_no_encontre_productos_valoracion.Text, null, 0.35f, 1.4f, 5);
                    }
                    App.BuscarValoraciones = false;
                }

                CrossTextToSpeech.Current.Speak(chatMessage.Text, null, 0.35f, 1.4f, 5);

                //ChatMessage proveniente del bot
                Messages.Add(chatMessage);
            });
        }

        private async Task<bool> CargarValoraciones()
        {
            try
            {
                ListaValoracionesProducto = await client.GetAll<Valoraciones>("http://www.recommenditws.somee.com/api/ValoracionesApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarProductos(string producto_usuario)
        {
            try
            {
                producto_usuario = producto_usuario.ToLower();
                ListaProductos = await client.GetAll<Productos>("http://www.recommenditws.somee.com/api/ProductosApi");
                ListaProductos = ListaProductos.Where(x => x.nombre.ToLower().Contains(producto_usuario)).Take(3).ToList(); //Mejores 3 coincidencias de productos
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private double CalcularRatingPromedio(int idProducto)
        {
            List<Valoraciones> ListaValProd = ListaValoracionesProducto.Where(x => x.id_producto == idProducto).ToList();
            if (ListaValProd.Count > 0)
            {
                double sumaValoracionesProd = ListaValProd.Sum(x => x.puntaje);
                double ratingPromedioProd = sumaValoracionesProd / ListaValProd.Count();
                return ratingPromedioProd;
            }
            else
            {
                return 0;
            }
        }

        private async Task<bool> CargarRecomendaciones()
        {
            try
            {
                ListaRecomendacionesProducto = await client.GetAll<Recomendaciones>("http://www.recommenditws.somee.com/api/RecomendacionesApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<string> CargarFotoProducto(int id_producto)
        {
            string pathFinal;
            ImagenesProductos imgDB = await client.GetById<ImagenesProductos>("http://www.recommenditws.somee.com/api/ImagenesProductosApi/" + id_producto);
            var pathImgDescargada = imgDB == null ? null : await App.DescargarImagenCloudinary(imgDB.nombre_imagen);
            if (imgDB != null)
            {
                pathFinal = pathImgDescargada;
            }
            else
            {
                pathFinal = "product_default.png";
            }
            return pathFinal;
        }
    }
}
