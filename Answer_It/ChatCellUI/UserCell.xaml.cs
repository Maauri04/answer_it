﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Answer_It;
using Answer_It.Models;
using Negocio;

namespace XFWatsonDemo.ChatCellUI
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class UserCell : ViewCell
	{
		RestClient client = new RestClient();

		public UserCell ()
		{
			InitializeComponent ();
			imgPerfilUsuario.Source = App.Usuario.foto;
			if (App.Usuario.foto == null)
			{
				imgPerfilUsuario.Source = "usuario_default.png";
			}
		}
	}
}