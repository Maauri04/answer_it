﻿using Answer_It;
using Answer_It.ViewModels;
using Answer_It.Views;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Syncfusion.XForms.Buttons;
using XFWatsonDemo.Models;
using System.Windows.Input;

namespace XFWatsonDemo.ChatCellUI
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BotCell : ViewCell
    {        
        public BotCell()
        {
            InitializeComponent();

            ChatMessage lastMessage = App.LastMessage;
            if (lastMessage.OptionsAreVisible) //recibimos un mensaje con opciones
            {
                foreach (var opt in lastMessage.OptionsList)
                {
                    Button btnOption = new Button
                    {
                        Text = opt,
                        TextColor = Color.FromHex("#FF8000"),
                        BackgroundColor = Color.Black,
                        BorderColor = Color.LightGray,
                        CornerRadius = 15
                    };
                    stackOptions.Children.Add(btnOption);
                    btnOption.Clicked += BtnOption_Clicked;
                }
            }
            if (App.BuscarAtributosExtra) //Tenemos atributos extras para mostrar
            {
                pickerAttrCondicion.ItemsSource = new string[] { "Nuevo", "Usado", "Reformado" };
                pickerAttrCondicion.SelectedIndex = 0;
                btnMejorarQuery.Clicked += BtnMejorarQuery_Clicked;
                App.BuscarAtributosExtra = false;
            }
        }

        private void BtnMejorarQuery_Clicked(object sender, EventArgs e)
        {
            //App.ImprovedQuery = App.BasicQuery + " " + (string.IsNullOrEmpty(txtAttrTipoProd.Text) ? "" : txtAttrTipoProd.Text + " ") + (string.IsNullOrEmpty(txtAttrMarca.Text) ? "" : txtAttrMarca.Text + " ") + (string.IsNullOrEmpty(txtAttrModelo.Text) ? "" : txtAttrModelo.Text + " ") + (string.IsNullOrEmpty(editorAttrExtra.Text) ? "" : editorAttrExtra.Text + " ") + pickerAttrCondicion.SelectedItem.ToString();
            App.ImprovedQuery = (string.IsNullOrEmpty(txtAttrTipoProd.Text) ? "" : txtAttrTipoProd.Text + " ") + (string.IsNullOrEmpty(txtAttrMarca.Text) ? "" : txtAttrMarca.Text + " ") + (string.IsNullOrEmpty(txtAttrModelo.Text) ? "" : txtAttrModelo.Text + " ") + (string.IsNullOrEmpty(editorAttrExtra.Text) ? "" : editorAttrExtra.Text + " ") + pickerAttrCondicion.SelectedItem.ToString();
            WatsonActions watsonAct = new WatsonActions();
            App.buscarProductoEnMeli = true;
            App.quiereBuscarUnProducto = true;
            watsonAct.SendMessageToWatson(null, deserializeQuery(App.ImprovedQuery), null, null, false);
        }

        private string deserializeQuery(string query)
        {
            string result = query.Trim();
            return result.Replace("+", " ");
        }

        private void BtnOption_Clicked(object sender, EventArgs e)
        {
            string respuesta = (sender as Button).Text;
            WatsonActions watsonAct = new WatsonActions();
            watsonAct.SendMessageToWatson(null, respuesta, null, null, false);            
        }

        private async void OnProduct_Tapped(object sender, EventArgs e)
        {
            var param = ((TappedEventArgs)e).Parameter;
            if (param.ToString() == "Botcell_Product")
            {
                MercadoLibreViewModel ProductoSeleccionado = new MercadoLibreViewModel();
                foreach (var item in App.ProductDetails)
                {
                    if (item.id == ProductId.Text)
                    {
                        ProductoSeleccionado.id = item.id;
                        ProductoSeleccionado.title = item.title;
                        ProductoSeleccionado.thumbnail = item.thumbnail;
                        ProductoSeleccionado.price = item.price;
                        ProductoSeleccionado.currency_id = item.currency_id;
                        ProductoSeleccionado.power_seller_status = item.power_seller_status;
                        ProductoSeleccionado.transactions_rating_positive = item.transactions_rating_positive;
                        ProductoSeleccionado.available_quantity = item.available_quantity;
                        ProductoSeleccionado.condition = item.condition;
                        ProductoSeleccionado.level_id = item.level_id;
                        ProductoSeleccionado.attributes = item.attributes;
                        ProductoSeleccionado.permalink = item.permalink;

                        ProductoSeleccionado = Validaciones(ProductoSeleccionado);
                        break;
                    }                  
                }
                await PopupNavigation.Instance.PushAsync(new PopupProducto(ProductoSeleccionado));              
            }
        }


        private async void OnProductReview_Tapped(object sender, EventArgs e)
        {
            var frame_product_review = sender as Frame;
            if (frame_product_review != null)
            {
                var stack_product_review = frame_product_review.Content as StackLayout;
                Label lblProduct = stack_product_review.Children.Where(x => x.ClassId == "productid").FirstOrDefault() as Label;
                string product_id = lblProduct.Text;
                App.MasterD.Detail = new NavigationPage(new ValoracionesProducto(Convert.ToInt32(product_id))) { BarTextColor = Color.FromHex("#FF8000") };
            }
        }

        private MercadoLibreViewModel Validaciones(MercadoLibreViewModel producto)
        {
            if (producto.currency_id == "ARS")
            {
                producto.price = "$ " + producto.price;
            }
            else
            {
                producto.price = producto.currency_id + producto.price;
            }
            if (producto.condition == "new")
            {
                producto.condition = "Nuevo";
            }
            else
            {
                if (producto.condition == "used")
                {
                    producto.condition = "Usado";
                }
                else
                {
                    producto.condition = "No especifica";
                }
            }
            if (String.IsNullOrEmpty(producto.power_seller_status))
            {
                producto.power_seller_status = "Vendedor aún sin clasificación";
            }

            producto.level_id = producto.level_id.Split('_')[0]; //Le paso al popup solo el nivel

            return producto;
        }
    }
}