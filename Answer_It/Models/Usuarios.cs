﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Answer_It.Models
{
    public class Usuarios
    {
        public int id { get; set; }
        public string email { get; set; }
        public string nombre_usuario { get; set; }
        public string contrasena { get; set; }
        public string apellido { get; set; }
        public string nombre { get; set; }
        public int id_rol { get; set; }
        public string pais { get; set; }
        public string localidad { get; set; }
        public int reputacion { get; set; }
        public int nivel { get; set; }
        public ImageSource foto { get; set; }
        public bool eliminado { get; set; }
        public DateTime? fecha_registro { get; set; }
        public int? r_points { get; set; }
        public int? visitas_al_perfil { get; set; }
        public int? votos_positivos { get; set; }
        public int? votos_negativos { get; set; }
        public bool? es_verificado { get; set; }
        public int? edad { get; set; }
    }
}
