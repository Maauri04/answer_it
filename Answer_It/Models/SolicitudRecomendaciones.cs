﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Answer_It.Models
{
    public class SolicitudRecomendaciones
    {
        public int id { get; set; }
        public string titulo { get; set; } //Necesidad basica
        public string leyenda { get; set; } //Leyenda que se muestra en la desc. del listview
        public int id_usuario { get; set; }
        public int? visitas { get; set; }
        public bool eliminado { get; set; }
        public DateTime? fecha { get; set; }
        public int reputacion_otorgada { get; set; }
        public bool sin_skill { get; set; }
        public bool skill_prisa { get; set; }
        public bool skill_destacado { get; set; }
        public int? nivel_requerido { get; set; }
        public string contenido { get; set; } //Consideraciones especiales para la recomendacion
        public string tipo_producto { get; set; }
    }
}
