﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Answer_It.Models
{
    public class NotificacionesTipo
    {
        public int Id { get; set; }
        public string Tipo { get; set; }
    }
}
