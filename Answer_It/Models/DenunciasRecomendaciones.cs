﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Answer_It.Models
{
    public class DenunciasRecomendaciones
    {
        public int id { get; set; }
        public int id_recomendacion { get; set; }
        public int id_usuario_denunciante { get; set; }
        public int id_usuario_denunciado { get; set; }
        public string motivo { get; set; }
        public int reputacion_descontada { get; set; }
        public DateTime? fecha_denuncia { get; set; }
    }
}
