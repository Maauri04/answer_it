﻿using Answer_It.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;

namespace Answer_It.Models
{
    public class MercadoLibreRequests
    {
        public List<MercadoLibreViewModel> GetProducts(string query)
        {
            string queryToMeli = "";
            if (String.IsNullOrEmpty(App.BasicQuery))
            {
                //App.BasicQuery = query; //Guardo la consulta inicial
                queryToMeli = clearQuery(query);
                queryToMeli = serializeQuery(queryToMeli);
            }
            else
            {
                query = deserializeQuery(query);
                queryToMeli = clearQuery(query);
                queryToMeli = serializeQuery(queryToMeli);
            }
            WebClient wc = new WebClient();
            var jsonData = wc.DownloadString("https://api.mercadolibre.com/sites/MLA/search?q=" + queryToMeli);
            var jsonParsed = JObject.Parse(jsonData);

            List<MercadoLibreProductAttr> ProductAttrList = new List<MercadoLibreProductAttr>();
            List<MercadoLibreViewModel> listaProductosMeli = new List<MercadoLibreViewModel>();
            for (int i = 0; i < 3; i++) //tomo los primeros 2 productos
            {
                foreach (var attr in jsonParsed["results"][i]["attributes"])
                {
                    ProductAttrList.Add(new MercadoLibreProductAttr
                    {
                        name = attr["name"].ToString(),
                        value_name = attr["value_name"].ToString()
                    });
                }

                MercadoLibreViewModel mlViewModel = new MercadoLibreViewModel //TO DO: CHECK CAMPOS NULOS. EJ: installments_quantity error en q=casa
                {
                    id = jsonParsed["results"][i]["id"].ToString(),
                    title = jsonParsed["results"][i]["title"].ToString(),
                    power_seller_status = jsonParsed["results"][i]["seller"]["seller_reputation"]["power_seller_status"].ToString(),
                    level_id = jsonParsed["results"][i]["seller"]["seller_reputation"]["level_id"].ToString(),
                    transactions_completed = jsonParsed["results"][i]["seller"]["seller_reputation"]["transactions"]["completed"].ToString(),
                    transactions_rating_positive = ((Convert.ToDouble(jsonParsed["results"][i]["seller"]["seller_reputation"]["transactions"]["ratings"]["positive"]) * 100) + "%").ToString(),
                    price = jsonParsed["results"][i]["price"].ToString(),
                    currency_id = jsonParsed["results"][i]["currency_id"].ToString(),
                    available_quantity = jsonParsed["results"][i]["available_quantity"].ToString(),
                    condition = jsonParsed["results"][i]["condition"].ToString(),
                    thumbnail = jsonParsed["results"][i]["thumbnail"].ToString(),
                    city_name = jsonParsed["results"][i]["address"]["city_name"].ToString(),
                    state_name = jsonParsed["results"][i]["address"]["state_name"].ToString(),
                    free_shipping = jsonParsed["results"][i]["shipping"]["free_shipping"].ToString(),
                    permalink = jsonParsed["results"][i]["permalink"].ToString(),
                    installments_quantity = jsonParsed["results"][i]["installments"]["quantity"].ToString(),
                    installments_amount = jsonParsed["results"][i]["installments"]["amount"].ToString(),
                    installments_rate = jsonParsed["results"][i]["installments"]["rate"].ToString(),
                    installments_currency_id = jsonParsed["results"][i]["installments"]["currency_id"].ToString(),
                    attributes = ProductAttrList
                };
                listaProductosMeli.Add(mlViewModel);
            }

            ////Cargo atributos de sugerencia en caso de tener que perfeccionar la busqueda por no conformidad de usuario
            //for (int i = 0; i < 10; i++) //jsonParsed.Count, hago la prueba con algunos
            //{
            //    foreach (var attr in jsonParsed["results"][i]["attributes"])
            //    {
            //        if (!App.GeneralProductAttrList.Exists(x => x.name.ToString() == attr["name"].ToString()) && App.GeneralProductAttrList.Count <= 4)
            //        {
            //            //No tengo el atributo cargado, lo agrego como nuevo y su primer valor del atributo
            //            MercadoLibreGeneralProductAttr generalAttr = new MercadoLibreGeneralProductAttr();
            //            generalAttr.name = attr["name"].ToString();
            //            App.GeneralProductAttrList.Add(generalAttr);
            //        }
            //    }
            //}

            return listaProductosMeli;
        }

        private string serializeQuery(string query)
        {
            string result = query.Trim();
            return result.Replace(" ", "+");
        }

        private string deserializeQuery(string query)
        {
            string result = query.Trim();
            return result.Replace("+", " ");
        }

        private string clearQuery(string query)
        {
            //Este metodo intentará quitar palabras que "ensucie" la query y dejar unicamente las palabras claves para buscar en ML
            List<string> dirtyList = new List<string>();
            dirtyList.AddRange(new string[] { "quiero comprar", "estoy buscando", "me gustaría comprar", "me gustaria comprar", "busco comprar", "necesito comprar", "tengo que" });
            dirtyList.AddRange(new string[] { "quiero", "necesitaría", "necesitaria", "necesito", "comprar", "busco" });
            string queryOptimized = "";
            string[] queryWordList = null;
            query = query.ToLower();
            queryWordList = query.Split(' ');
            foreach (var dirtyPhrase in dirtyList)
            {
                string[] tempPhrase = dirtyPhrase.Split(' ');
                if (tempPhrase.Length > 1) //Frase compuesta (mas de una palabra)
                {
                    if (query.Contains(dirtyPhrase))
                    {
                        queryOptimized = query.Replace(dirtyPhrase, "");
                        break;
                    }
                }
                else //Frase simple (una sola palabra: Busco, quiero, Necesito)
                {
                    if (tempPhrase[0].ToString() == queryWordList[0])
                    {
                        queryWordList[0] = "";
                        for (int i = 0; i < queryWordList.Length; i++)
                        {
                            queryOptimized += " " + queryWordList[i];
                        }
                        break;
                    }
                }
            }
            if (String.IsNullOrEmpty(queryOptimized))
            {
                queryOptimized = query;
            }
            return queryOptimized.Trim();
        }
    }
}
