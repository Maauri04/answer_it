﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace XFWatsonDemo.Models
{
    public class ChatMessage
    {
        public string ProductId { get; set; }
        public string TitleText { get; set; } //Texto del titulo del producto
        public bool TitleIsVisible { get; set; } //Titulo del producto es visible
        public bool VerDetallesIsVisible { get; set; }
        public string Text { get; set; } //Texto escrito por el bot
        public DateTime MessageDateTime { get; set; }
        public bool IsIncoming { get; set; }
        public int ImageHeight { get; set; }
        public int ImageWidth { get; set; }
        public string Image { get; set; }
        public List<string> OptionsList = new List<string>();
        public bool OptionsAreVisible { get; set; } //Se muestran opciones si la pregunta viene con opciones
        public bool ShowAtributesAreVisible { get; set; } //Se muestran atributos para perfeccionar la busqueda del usuario
        public string CellType { get; set; } //Tipo de cellUI: Botcell_Product, Botcell_Message

        //Propiedades para valoraciones de productos
        public bool ProductReviewsAreVisible { get; set; }
        public string TituloProductoValoracion { get; set; }
        public ImageSource ImagenProductoValoracion { get; set; }
        public bool TextoSinValIsVisible { get; set; }
        public bool RatingProductoValIsVisible { get; set; }
        public double ValoracionProducto { get; set; }
        public int CantValoraciones { get; set; }
        public int CantRecomendaciones { get; set; }
        public bool EsTendencia { get; set; }
    }
}
