﻿using Nancy.Json;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
    public class RestClient
    {
        public async Task<List<T>> GetAll<T>(string url)  //Cualquier peticion de tipo GET entra aca.
        {
            try
            {
                HttpClient client = new HttpClient();
                var response = await client.GetAsync(url); //Le pasamos la URL del JSON 
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    
                    var jsonstring = await response.Content.ReadAsStringAsync();
                    return Newtonsoft.Json.JsonConvert.DeserializeObject<List<T>>(jsonstring);
                }
            }
            catch (Exception e)
            {
                throw e;
            }


            return default(List<T>);
        }

        public async Task<T> GetById<T>(string url)  //Cualquier peticion de tipo GET entra aca.
        {
            try
            {
                HttpClient client = new HttpClient();
                var response = await client.GetAsync(url); //Le pasamos la URL del JSON 
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    var jsonstring = await response.Content.ReadAsStringAsync();
                    return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(jsonstring);
                }
            }
            catch (Exception e)
            {
                throw e;
            }


            return default(T);
        }

        public async Task<bool> Post<T>(string url, object objeto)  
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Method = "POST";
            httpWebRequest.Accept = "application/json; charset=utf-8";
            bool resultado = false;
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string loginjson = new JavaScriptSerializer().Serialize(objeto); 

                streamWriter.Write(loginjson);
                streamWriter.Flush();
                streamWriter.Close();

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    resultado = Convert.ToBoolean(streamReader.ReadToEnd());
                }
            }
            return resultado;
        }

        public async Task<int> PostAndGetId<T>(string url, object objeto) //Permite devolver un id generado, en vez de un bool
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Method = "POST";
            httpWebRequest.Accept = "application/json; charset=utf-8";
            int resultado = 0;
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string loginjson = new JavaScriptSerializer().Serialize(objeto);

                streamWriter.Write(loginjson);
                streamWriter.Flush();
                streamWriter.Close();

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    resultado = Convert.ToInt32(streamReader.ReadToEnd());
                }
            }
            return resultado;
        }

        public async Task<bool> Put<T>(string url, object objetoActualizado) //Le paso el objeto ya actualizado
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Method = "PUT";
            httpWebRequest.Accept = "application/json; charset=utf-8";
            bool resultado = false;
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string loginjson = new JavaScriptSerializer().Serialize(objetoActualizado);

                streamWriter.Write(loginjson);
                streamWriter.Flush();
                streamWriter.Close();

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    resultado = Convert.ToBoolean(streamReader.ReadToEnd());
                }
            }
            return resultado;
        }

        public async Task<bool> Delete<T>(string url) //Le paso el id
        {
            try
            {
                HttpClient client = new HttpClient();
                var response = await client.DeleteAsync(url); //Le pasamos la URL del JSON 
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var jsonstring = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<bool>(jsonstring);
                }
            }
            catch (Exception e)
            {
                throw e;
            }


            return default;
        }

        //public async Task<bool> Delete<T>(string url, int id) //Le paso el id
        //{
        //    var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
        //    httpWebRequest.ContentType = "application/json; charset=utf-8";
        //    httpWebRequest.Method = "DELETE";
        //    httpWebRequest.Accept = "application/json; charset=utf-8";
        //    bool resultado = false;
        //    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
        //    {
        //        string loginjson = new JavaScriptSerializer().Serialize(id);

        //        streamWriter.Write(loginjson);
        //        streamWriter.Flush();
        //        streamWriter.Close();

        //        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
        //        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
        //        {
        //            resultado = Convert.ToBoolean(streamReader.ReadToEnd());
        //        }
        //    }
        //    return resultado;
        //}
    }
}
