﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Answer_It.Models
{
    public class Denuncias
    {
        public int id { get; set; }
        public int motivo { get; set; }
        public int? reputacion_descontada { get; set; }
    }
}
