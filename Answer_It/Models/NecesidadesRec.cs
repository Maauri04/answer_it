﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Answer_It.Models
{
    public class NecesidadesRec
    {
        public int id { get; set; }
        public string nombre_necesidad { get; set; }
        public int id_solicitud_recomendacion { get; set; }
        public bool es_necesidad_primaria { get; set; }
    }
}
