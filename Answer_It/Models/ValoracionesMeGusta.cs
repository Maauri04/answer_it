﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Answer_It.Models
{
    public class ValoracionesMeGusta
    {
        public int id { get; set; }
        public int id_valoracion { get; set; }
        public int id_usuario_like { get; set; }
        public bool me_gusta { get; set; }
        public bool no_me_gusta { get; set; }
    }
}
