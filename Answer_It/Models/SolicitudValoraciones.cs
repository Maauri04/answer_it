﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Answer_It.Models
{
    public class SolicitudValoraciones
    {
        public int id { get; set; }
        public string leyenda { get; set; }
        public int id_usuario { get; set; }
        public int? visitas { get; set; }
        public bool eliminado { get; set; }
        public DateTime? fecha { get; set; }
        public int cant_recomendaciones { get; set; }
        public int reputacion_otorgada { get; set; }
        public bool sin_skill { get; set; }
        public bool skill_prisa { get; set; }
        public bool skill_destacado { get; set; }
        public int id_producto { get; set; }
        public string aspecto_especial { get; set; }
        public int? r_points_recompensa { get; set; }
    }
}
