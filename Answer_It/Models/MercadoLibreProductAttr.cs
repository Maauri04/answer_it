﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Answer_It.Models
{
    public class MercadoLibreProductAttr
    {
        public string name { get; set; } //Nombre del atributo (ej: marca)
        public string value_name { get; set; } //Valor del atributo (ej: ray-ban)
    }
}
