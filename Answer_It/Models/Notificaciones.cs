﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Answer_It.Models
{
    public class Notificaciones
    {
        public int id { get; set; }
        public int id_usuario_origen { get; set; }
        public int id_usuario_destino { get; set; }
        public string descripcion { get; set; }
        public bool leida { get; set; }
        public int id_tipo { get; set; }
        public int? id_publicacion { get; set; }
        public DateTime? fecha { get; set; }
    }
}
