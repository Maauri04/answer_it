﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Answer_It.ViewModels
{
    public class AdministrarDenunciasViewModel
    {
        public int id_val_rec { get; set; } //Id de la valoracion/recomendacion. Lo almaceno para poder eliminarlo.
        public int id_denuncia { get; set; } //Almaceno el id de la denuncia para poder eliminarla.
        public string texto_valoracion_recomendacion { get; set; } //Palabra valoracion o recomendacion hardcodeada.
        public string comentario_denunciado { get; set; } //Comentario denunciado.
        public string nombre_usuario_denunciado { get; set; } //Usuario denunciado (que hizo el comentario).
        public string nombre_usuario_denunciante { get; set; } //Usuario que denuncio el comentario.
        public string motivo_denuncia { get; set; }
        public DateTime fecha_denuncia { get; set; } //fecha en la cual fue denunciado el comentario.
    }
}
