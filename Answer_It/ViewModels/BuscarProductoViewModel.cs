﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Answer_It.ViewModels
{
    public class BuscarProductoViewModel
    {
        public int id_producto { get; set; }
        public string titulo { get; set; }
        public double valoracion { get; set; } //1,2,3,4 o 5 estrellas.
        public int coincidencias { get; set; } //Cantidad de palabras buscadas por el usuario que coinciden con el titulo del producto (me permite ordenar los productos por cant de coincidencias)
        public int cant_valoraciones { get; set; } //Cantidad de opiniones que tiene este producto en toda la comunidad.
        public int cant_recomendaciones { get; set; } //Cantidad de veces que fue recomendado a otros usuarios.
        public bool es_tendencia { get; set; } //El producto esta en el top 10 mas valorado.
        public ImageSource imagen { get; set; }

        //No puedo acceder a estos elementos desde el back ya que estan definidos dentro de una listview. Los tengo que bindear como una prop.
        public bool sin_valoraciones_visible { get; set; } //Si no tiene valoraciones, mostraremos este atributo.
        public bool rating_visible { get; set; } //Si no tiene valoraciones, ocultaremos el rating.
    }
}
