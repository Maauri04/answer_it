﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Answer_It.ViewModels
{
    public class ProductosViewModel
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public double valoracion { get; set; }
        public int cant_votos { get; set; }
    }
}
