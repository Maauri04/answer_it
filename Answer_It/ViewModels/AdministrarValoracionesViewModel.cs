﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Answer_It.ViewModels
{
    public class AdministrarValoracionesViewModel
    {
        public int id_val { get; set; }
        public string nombre_usuario_creador { get; set; }
        public string titulo_val { get; set; }
        public string titulo_prod { get; set; }
        public string descripcion { get; set; }
        public int me_gusta { get; set; } //Numero entero (positivo o negativo) que representa los mg que tuvo en la comunidad.
        public string esta_denunciada { get; set; }
        public DateTime fecha { get; set; }
    }
}
