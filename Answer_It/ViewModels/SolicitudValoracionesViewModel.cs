﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Answer_It.ViewModels
{
    public class SolicitudValoracionesViewModel
    {
        public int id { get; set; }
        public string nombre_usuario { get; set; }
        public DateTime fecha_publicacion { get; set; }
        public string descripcion_publicacion { get; set; } //Descripcion adicional que el usuario solicitante hace en la comunidad
        public int cant_valoraciones { get; set; } //Cantidad de valoraciones hechas por los usuarios
        public int cant_recomendaciones { get; set; } //Cantidad de veces que fue recomendado a otros usuarios.
        public bool skill_prisa { get; set; } //El usuario que publica, canjeo skill points por prisa. Sus posts apareceran encima de todos.
        public bool skill_destacado { get; set; } //El usuario que publica, canjeo skill points por destacar su post. Sus posts apareceran encima de todos (debajo de prisa).
        public bool sin_skill { get; set; }//El usuario no tiene activo ningun skill.
        public int reputacion_otorgada { get; set; } //Reputacion que otorga a los usuarios al responder la solicitud de valoracion actual.
        public string leyenda_publicacion { get; set; } //Leyenda que se muestra en cada solicitud de valoracion de productos.  
        public int visitas { get; set; }
        public ImageSource imagen_usuario { get; set; }
        public string imagen_reputacion { get; set; }

        //Caracteristicas del producto
        public int id_producto { get; set; }
        public string nombre_producto { get; set; }
        public decimal valoracion_producto { get; set; }
        public ImageSource imagen_producto { get; set; }
    }
}
