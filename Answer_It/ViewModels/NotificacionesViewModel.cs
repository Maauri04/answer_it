﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Answer_It.ViewModels
{
    public class NotificacionesViewModel
    {
        public int id { get; set; }
        public int id_usuario_origen { get; set; }
        public int id_usuario_destino { get; set; }
        public string nombre_usuario_origen { get; set; }
        public string descripcion { get; set; }
        public bool leida { get; set; }
        public int id_tipo_notificacion { get; set; }
        public string nombre_tipo_notificacion { get; set; }
        public string imagen_notificacion { get; set; }
        public DateTime fecha { get; set; }
        public int id_publicacion { get; set; }
        public string color_notificacion { get; set; }
    }
}
