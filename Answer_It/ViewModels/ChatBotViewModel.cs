﻿using Answer_It;
using Answer_It.Models;
using Answer_It.ViewModels;
using IBM.WatsonDeveloperCloud.Conversation.v1;
using IBM.WatsonDeveloperCloud.Conversation.v1.Model;
//using IBM.Watson.Assistant.v1;
//using IBM.Watson.Assistant.v1.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.TextToSpeech;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using XFWatsonDemo.Models;

namespace XFWatsonDemo
{
    public class ChatBotViewModel : BindableObject
    {
        private ConversationService _conversation;
        private string _outGoingText;
        public ObservableCollection<ChatMessage> Messages { get; }

        public ChatBotViewModel()
        {
            Messages = new ObservableCollection<ChatMessage>();
            OutGoingText = string.Empty;
            ConnectToWatson();
        }

        private void ConnectToWatson()
        {
            _conversation = new ConversationService("apikey", "SYwr2-8Aggx9gHsvegDdvoE8svDtUd5YXAcgyicy4Bri", "2022-03-15");
            _conversation.SetEndpoint("https://api.us-south.assistant.watson.cloud.ibm.com/instances/3c85e33f-b8ee-42b0-b650-bfaeef920ad4");
            App.buscarProductoEnMeli = false;
            App.BuscarValoraciones = false;
            App.quiereBuscarOpiniones = false;
            App.quiereBuscarUnProducto = false;
            WatsonActions wa = new WatsonActions();
            wa.SendMessageToWatson(null, null, Messages, _conversation, true); //Lanzo un mensaje de bienvenida para iniciar la conversacion.
            //var mensaje_bienvenida = new ChatMessage
            //{
            //    Text = "¡Hola " + App.Usuario.nombre + "! Soy Watson. Fui entrenado para recomendar productos, o buscar las opiniones que otros usuarios han dejado sobre ellos. ¿Qué puedo hacer por tí?",
            //    TitleText = null,
            //    TitleIsVisible = false,
            //    ImageHeight = 0,
            //    ImageWidth = 0,
            //    VerDetallesIsVisible = false,
            //    IsIncoming = true,
            //    MessageDateTime = DateTime.Now,
            //    CellType = "Botcell_Message"
            //};
            //CrossTextToSpeech.Current.Speak(mensaje_bienvenida.Text, null, 0.35f, 1.4f, 5);
            //Messages.Add(mensaje_bienvenida);
        }

        public string OutGoingText
        {
            get
            {
                return _outGoingText;
            }
            set
            {
                _outGoingText = value;
                OnPropertyChanged();
            }
        }

        public ICommand SendCommand => new Command(SendMessage);

        public void SendMessage()
        {
            WatsonActions watsonAct = new WatsonActions();
            watsonAct.SendMessageToWatson(OutGoingText, null, Messages, _conversation, false);
        }

        //

        
    }
}

