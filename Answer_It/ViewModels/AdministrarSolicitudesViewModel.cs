﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Answer_It.ViewModels
{
    public class AdministrarSolicitudesViewModel
    {
        public int id_sol { get; set; }
        public string titulo { get; set; }
        public string nombre_usuario { get; set; } //Usuario creador de la sol_rec
        public DateTime fecha_creacion { get; set; } //Fecha creacion de la sol_rec
    }
}
