﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Answer_It.ViewModels
{
    public class BuscarUsuarioViewModel
    {
        public string nombre_usuario { get; set; }
        public ImageSource imagen { get; set; }
        public int ranking_usuario { get; set; }
        public int nivel_usuario { get; set; }
        public int votos_totales { get; set; } //votos positivos - votos negativos
        public bool es_verificado { get; set; }
        public int coincidencias { get; set; } //Cantidad de palabras buscadas por el usuario que coinciden con el titulo del producto.
    }
}
