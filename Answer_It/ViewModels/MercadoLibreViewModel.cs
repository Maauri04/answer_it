﻿using System;
using System.Collections.Generic;
using System.Text;
using Answer_It.Models;

namespace Answer_It.ViewModels
{
    public class MercadoLibreViewModel
    {
        public string id { get; set; }
        public string title { get; set; }
        public string seller_id { get; set; }
        public string power_seller_status { get; set; } //null, Silver, Platinum, Gold
        public string level_id { get; set; } //Termometro ML: 1_red, 2_orange, 3_yellow, 4_light_gren, 5_green
        public string transactions_completed { get; set; } //Total ventas completadas
        public string transactions_rating_positive { get; set; } //Porcentaje de valoraciones positivas
        public string price { get; set; }
        public string currency_id { get; set; } //ARS (pesos argentinos)
        public string available_quantity { get; set; }
        public string condition { get; set; } //new, used
        public string thumbnail { get; set; } //Pequeña imagen del producto
        public string city_name { get; set; }
        public string state_name { get; set; }
        public string free_shipping { get; set; }
        public string permalink { get; set; } //link al producto
        public string installments_quantity { get; set; } //cantidad de cuotas admitidas
        public string installments_amount { get; set; } //valor aplicando cuotas
        public string installments_rate { get; set; } //tasa
        public string installments_currency_id { get; set; } //moneda de la cuota
        public List<MercadoLibreProductAttr> attributes { get; set; } //Lista de atributos del producto (name = Nombre del attr; value_name = Valor del attr)
    }
}
