﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Answer_It.ViewModels
{
    public class ValoracionesViewModel
    {
        public int id { get; set; }
        public string titulo { get; set; }
        public string contenido { get; set; }
        public int id_usuario { get; set; }
        public string nombre_usuario { get; set; }
        public int id_producto { get; set; }
        public double puntaje { get; set; } //PONDERAR POR REPUTACION DEL USUARIO!!
        public bool eliminado { get; set; }
        public DateTime? fecha { get; set; }
        public int me_gusta { get; set; }
        public string estado_cara { get; set; }
    }
}
