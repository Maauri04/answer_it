﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Answer_It.ViewModels
{
    public class MisInteraccionesViewModel
    {
        public int id_val_rec { get; set; } //Id de la valoracion o recomendacion.
        public string titulo_val_rec { get; set; } //2 valores posibles: "Valoracion" o "Recomendacion".
        public string titulo_publicacion { get; set; } //Titulo de la valoracion o recomendacion.
        public DateTime? fecha_interaccion { get; set; }
        public int me_gusta { get; set; }
        public string estado_cara { get; set; }
    }
}
