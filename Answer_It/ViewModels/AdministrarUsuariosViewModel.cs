﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Answer_It.ViewModels
{
    public class AdministrarUsuariosViewModel
    {
        public int id_usu { get; set; }
        public string nombre_usuario { get; set; }
        public bool es_verificado { get; set; }
        public int ranking_usuario { get; set; }
        public int votos_totales { get; set; }
        public bool permite_borrar { get; set; } //Indica si el usuario puede ser eliminado o no.
    }
}
