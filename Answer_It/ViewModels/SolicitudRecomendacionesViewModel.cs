﻿using Answer_It.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Answer_It.ViewModels
{
    public class SolicitudRecomendacionesViewModel
    {
        public int id { get; set; }
        public string nombre_usuario { get; set; }
        public DateTime fecha_publicacion { get; set; }
        public string titulo { get; set; }
        public List<NecesidadesRec> lista_necesidades = new List<NecesidadesRec>(); //Necesidades que se van a mostrar a los usuarios
        public bool skill_prisa { get; set; } //El usuario que publica, canjeo skill points por prisa. Sus posts apareceran encima de todos.
        public bool skill_destacado { get; set; } //El usuario que publica, canjeo skill points por destacar su post. Sus posts apareceran encima de todos (debajo de prisa).
        public bool sin_skill { get; set; }//El usuario no tiene activo ningun skill.
        public int reputacion_otorgada { get; set; } //Reputacion que otorga a los usuarios al responder la solicitud de valoracion actual.
        public string leyenda_publicacion { get; set; } //Leyenda que se muestra en cada solicitud de valoracion de productos.
        public int cant_recomendaciones { get; set; }
        public string contenido { get; set; } //Descripcion adicional que el usuario hace para completar su solicitud y dar mas detalles.
        public int nivel_requerido { get; set; } //Nivel de reputacion requerido para valorar el producto solicitado.
        public int visitas { get; set; }
        public string tipo_producto { get; set; } //Tipo de producto que el usuario solicitante elije para recibir recomendaciones.
        public ImageSource imagen_usuario { get; set; }
        public string imagen_reputacion { get; set; }
    }
}
