﻿using System;
using System.Collections.Generic;
using System.Text;
using Answer_It.Models;

namespace Answer_It.ViewModels
{
    public class RecomendacionesViewModel
    {
        public int id { get; set; }
        public string contenido { get; set; }
        public int id_usuario { get; set; }
        public int id_solicitud_recomendacion { get; set; }
        public int me_gusta { get; set; }
        public DateTime? fecha { get; set; }
        public string estado_cara { get; set; }
        public bool eliminado { get; set; }
        public Productos producto_recomendado = new Productos();
    }
}
