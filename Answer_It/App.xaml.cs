﻿using Answer_It.MasterDetail;
using Answer_It.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XFWatsonDemo;
using System.Collections.Generic;
using Negocio;
using Answer_It.ViewModels;
using System.Collections.ObjectModel;
using XFWatsonDemo.Models;
using Answer_It.Models;
using System.IO;
using Plugin.Permissions.Abstractions;
using System.Threading.Tasks;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using System.Net;
using Plugin.Connectivity;
using System.Linq;

namespace Answer_It
{
    public partial class App : Application
    {
        public static MasterDetailPage MasterD { get; set; }
        public static Usuarios Usuario { get; set; } //Usuario de la sesion.
        public static List<NivelesBeneficios> ListaNiveles = new List<NivelesBeneficios>();
        public static List<MercadoLibreViewModel> ProductDetails = new List<MercadoLibreViewModel>();
        public static ChatMessage LastMessage = new ChatMessage();
        public static string ImprovedQuery; //Chatbot - Query mejorada para perfeccionar la busqueda
        public static string BasicQuery; //Chatbot - Query inicial
        public static bool BuscarAtributosExtra { get; set; } //Chatbot - Indica si el usuario quiere perfeccionar su busqueda
        public static bool buscarProductoEnMeli { get; set; }
        public static bool quiereBuscarUnProducto { get; set; } //Chatbot - Indica que el usuario quiere buscar un producto, paso previo al #Buy_Product intent.
        public static bool quiereBuscarOpiniones { get; set; } //Chatbot - Indica que el usuario quiere buscar opiniones en la comu, paso previo al #Search_Opinions intent.
        public static bool BuscarValoraciones { get; set; } //Chatbot - Indica si el usuario busca valoraciones en la comunidad
        private static string IdImagen = ""; //Id de la imagen subida a cloudinary



        //BORRAR CUANDO USE EL WS

        //public static List<NecesidadesViewModel> ListaNecesidades = new List<NecesidadesViewModel>();
        //public static List<NotificacionesViewModel> ListaNotificaciones = new List<NotificacionesViewModel>();
        //public static List<Roles> ListaRoles = new List<Roles>();
        //public static List<ValoracionesMeGusta> ListaValoracionesMeGusta = new List<ValoracionesMeGusta>();

        public App()
        {
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MTMxNDk1QDMxMzcyZTMyMmUzMEJGK3BmZTBVSHBvak50L0cwYWk5dDRHL29QYTFHSTcydmdFYzdJTGc0YWc9");
            InitializeComponent();
            CargarNiveles();

            MainPage = new NavigationPage(new SplashPage());
        }



        public static bool HaySesion()
        {
            if (App.Usuario != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static byte[] ConvertirImagenABytes(Stream streamImage)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                streamImage.CopyTo(ms);
                return ms.ToArray();
            }
        }

        public static string MeGustaEnIcono(int me_gusta) //Transforma los me gusta en una cara, que representa un nivel de felicidad.
        {
            if (me_gusta > 0)
            {
                if (me_gusta > 3)
                {
                    if (me_gusta > 5)
                    {
                        return "ic_feliz_3.png";
                    }
                    else
                    {
                        return "ic_feliz_2.png";
                    }
                }
                else
                {
                    return "ic_feliz_1.png";
                }
            }
            else
            {
                if (me_gusta == 0)
                {
                    return "ic_neutral.png";
                }
                else
                {
                    if (me_gusta > -5)
                    {
                        return "ic_triste_1.png";
                    }
                    else
                    {
                        return "ic_triste_2.png";
                    }
                }
            }
        }

        //Metodo alternativo de chequear permisos
        //public async Task<PermissionStatus> CheckAndRequestLocationPermission()
        //{
        //    //Solicito permisos de MediaLibrary, Photos y Phone (memoria interna)
        //    var status = await Plugin.Permissions.CrossPermissions.Current.CheckPermissionStatusAsync(Permission.MediaLibrary); /*Permissions.CheckStatusAsync<Permissions.LocationWhenInUse>();*/

        //    if (status == PermissionStatus.Granted)
        //        return status;

        //    if (status == PermissionStatus.Denied && DeviceInfo.Platform == DevicePlatform.iOS)
        //    {
        //        // Prompt the user to turn on in settings
        //        // On iOS once a permission has been denied it may not be requested again from the application
        //        return status;
        //    }

        //    if (Permissions.ShouldShowRationale<Permissions.LocationWhenInUse>())
        //    {
        //        // Prompt the user with additional information as to why the permission is needed
        //    }

        //    status = await Permissions.RequestAsync<Permissions.LocationWhenInUse>();

        //    return status;
        //}

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        public async static Task<string> SubirImagenCloudinary(string nombreImg, Stream streamImage)
        {
            try
            {
                Account account = new Account("recommendit", "988849462435128", "tE9eWvURd2rP6RcEFNUru-4Ucqw");
                Cloudinary cloudinary = new Cloudinary(account);
                var uploadParameters = new ImageUploadParams()
                {
                    File = new FileDescription(nombreImg, streamImage)
                };
                ImageUploadResult uploadResult = await cloudinary.UploadAsync(uploadParameters);
                IdImagen = uploadResult.PublicId; //ID GENERADO DE LA IMG SUBIDA.
                return IdImagen; //Guardar en la base de datos del otro lado, cuando la use.
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async static Task<string> DescargarImagenCloudinary(string imgBaseDeDatos)
        {
            try
            {
                string pathDescargado = "";
                using (var client = new WebClient())
                {
                    Account account = new Account("recommendit", "988849462435128", "tE9eWvURd2rP6RcEFNUru-4Ucqw");
                    Cloudinary cloudinary = new Cloudinary(account);
                    //var imgOptimizada = cloudinary.Api.UrlImgUp.Transform(new Transformation().Height(520).Quality("auto:best").Crop("scale")).BuildUrl(imgBaseDeDatos);
                    var imgOptimizada = cloudinary.Api.UrlImgUp.Transform(new Transformation().Width(800).Height(520).Quality("auto:best").Crop("limit")).BuildUrl(imgBaseDeDatos);
                    await client.DownloadFileTaskAsync(imgOptimizada, Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), imgBaseDeDatos));
                    //Descargara la imagen para consumirla en /data/data/YourAppName/files/ LUEGO HAY QUE BORRARLA!
                    pathDescargado = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), imgBaseDeDatos);
                }
                return pathDescargado;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static void BorrarImagenLocalmente(string pathImg)
        {
            if (File.Exists(pathImg))
            {
                File.Delete(pathImg);
            }
        }

        public static void BorrarImagenLocalmente(ImageSource pathImg)
        {
            if (File.Exists(pathImg.ToString()))
            {
                File.Delete(pathImg.ToString());
            }
        }

        //var imgOptimizada = cloudinary.Api.UrlImgUp.Transform(new Transformation().Height(520).Quality("auto:best").Crop("scale")).BuildUrl(uploadResult.PublicId);

        public static bool TieneConexion()
        {
            return CrossConnectivity.Current.IsConnected;
        }

        public static int ObtenerRankingUsuario(int id_usuario, List<Usuarios> listaUsu)
        {
            listaUsu = listaUsu.OrderByDescending(x => x.reputacion).ToList();
            for (int i = 0; i < listaUsu.Count; i++)
            {
                if (id_usuario == listaUsu[i].id)
                {
                    return (i + 1);
                }
            }
            return 0;
        }

        public void CargarNiveles()
        {
            ListaNiveles.Add(new NivelesBeneficios { id = 1, nivel = 1, reputacion_necesaria = 100, beneficios = "Valorar y recomendar en la comunidad.\r\nInteractuar con Watson." });
            ListaNiveles.Add(new NivelesBeneficios { id = 2, nivel = 2, reputacion_necesaria = 250, beneficios = "Valorar comentarios de otros usuarios." });
            ListaNiveles.Add(new NivelesBeneficios { id = 3, nivel = 3, reputacion_necesaria = 500, beneficios = "Destacar una solicitud de\r\nvaloración o recomendación." });
            ListaNiveles.Add(new NivelesBeneficios { id = 4, nivel = 4, reputacion_necesaria = 1000, beneficios = "Crear solicitud de valoración \"con prisa\"." });
            ListaNiveles.Add(new NivelesBeneficios { id = 5, nivel = 5, reputacion_necesaria = 5000, beneficios = "Ofrecer R! Points como recompensa\r\npara recibir opiniones rápidas." });
            ListaNiveles.Add(new NivelesBeneficios { id = 6, nivel = 6, reputacion_necesaria = 10000, beneficios = "Elegir qué nivel de usuarios\r\nte pueden recomendar." });
            ListaNiveles.Add(new NivelesBeneficios { id = 7, nivel = 7, reputacion_necesaria = 20000, beneficios = "Cargar hasta 7 necesidades extras\r\nen \"Solicitar una recomendación\"." });
            ListaNiveles.Add(new NivelesBeneficios { id = 8, nivel = 8, reputacion_necesaria = 40000, beneficios = "Ver cantidad de visitas a mi perfil." });
            ListaNiveles.Add(new NivelesBeneficios { id = 9, nivel = 9, reputacion_necesaria = 75000, beneficios = "Moderador en potencia: Ya podés\r\nsuspender publicaciones que no\r\ncumplan alguna regla de la comunidad." });
            ListaNiveles.Add(new NivelesBeneficios { id = 10, nivel = 10, reputacion_necesaria = 100000, beneficios = "¡Sos moderador! Ya podés suspender\r\nusuarios, comentarios y publicaciones\r\nque no cumplan alguna regla de la comunidad" });
        }
    }
}
