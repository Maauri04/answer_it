﻿using Answer_It.Models;
using Negocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Answer_It.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SolicitarRecomendacion_3 : ContentPage
    {
        public string TipoProducto { get; set; }
        public string NecesidadBasica { get; set; }
        public List<string> ListaNecesidadesAdicionales { get; set; }
        public string Leyenda { get; set; } = "Esta publicación aún no tiene recomendaciones." +
                                                " Sé el primero en recomendar y obtendrás reputación extra";
                                          
        //Consideraciones -> editorConsideraciones
        //Nivel requerido para valorar -> pickerNivelRequerido.SelectedItem
        //R Points de recompensa -> pickerRecompensas.SelectedItem
        //skill prisa -> switchPrisa
        //skill destacado -> switchDestacado

        RestClient client = new RestClient();

        public SolicitarRecomendacion_3(string tipoProducto, string necesidadBasica, List<string> listaNecesidadesAdicionales)
        {
            InitializeComponent();
            TipoProducto = tipoProducto;
            NecesidadBasica = necesidadBasica;
            ListaNecesidadesAdicionales = listaNecesidadesAdicionales;
            UsuarioPuedeElegirNivel();
            InicializarComboRecompensas();
        }

        private void InicializarComboRecompensas()
        {
            List<int> listaRecompensasPosibles = new List<int> { 10, 50, 100 };
            pickerRecompensas.ItemsSource = listaRecompensasPosibles;
            pickerRecompensas.SelectedIndex = 0;
        }

        private void UsuarioPuedeElegirNivel()
        {
            if (App.Usuario.nivel >= 6)
            {
                List<int> listaNiveles = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
                pickerNivelRequerido.ItemsSource = listaNiveles;
                pickerNivelRequerido.SelectedIndex = 0;
                pickerNivelRequerido.IsEnabled = true;
                lblLeyendaSinReputacion.IsVisible = false;
            }
            else
            {
                pickerNivelRequerido.IsEnabled = false;
                lblLeyendaSinReputacion.IsVisible = true;
            }
        }

        private void InfoNivelRequerido_Tapped(object sender, EventArgs e)
        {
            DisplayAlert("Nivel requerido de los usuarios", "Tú eliges que rango de usuarios pueden recomendarte productos.\r\n \r\nRecuerda que puedes tener pocas valoraciones si colocas un nivel muy alto.. ¡Tú decides!", "Aceptar");
        }

        private void InfoRecompensas_Tapped(object sender, EventArgs e)
        {
            DisplayAlert("Otorgar R! Points como recompensa", "¿Buscas una opinión rápida? ¡Esta función es para tí!\r\n \r\nEsta función permite dar como recompensa parte de tus R! Points acumulados al usuario que responda primero, por ser el primero en colaborar contigo.", "Aceptar");
        }

        protected override bool OnBackButtonPressed()
        {
            Navigation.PopAsync();
            return true;
        }

        private void InfoPrisa_Tapped(object sender, EventArgs e)
        {
            DisplayAlert("¡Tengo prisa!", "Si solicitas una recomendación con prisa, tu solicitud se verá por encima del resto de las publicaciones, ¡incluso de aquellas destacadas! \r\n \r\n Se necesitan 500 R! Points", "Aceptar");
        }

        private void InfoDestacado_Tapped(object sender, EventArgs e)
        {
            DisplayAlert("Destacar solicitud", "Si destacas tu solicitud, la misma se verá por encima del resto de las publicaciones, pero debajo de aquellas que tengan prisa. \r\n \r\n Se necesitan 370 R! Points", "Aceptar");
        }

        private void switchPrisa_Toggled(object sender, ToggledEventArgs e)
        {
            if (App.Usuario.nivel >= 4)
            {
                if (stackPrisa.BackgroundColor == Color.Gray)
                {
                    if (stackDestacado.BackgroundColor == Color.OrangeRed)
                    {
                        switchDestacado.IsToggled = false;
                        stackDestacado.BackgroundColor = Color.Gray;
                    }
                    stackPrisa.BackgroundColor = Color.DodgerBlue;
                }
                else
                {
                    stackPrisa.BackgroundColor = Color.Gray;
                }
            }
            else
            {
                switchPrisa.IsToggled = false;
                int reputacionNecesaria = App.ListaNiveles.Where(x => x.nivel == 4).FirstOrDefault().reputacion_necesaria;
                int reputacionFaltante = reputacionNecesaria - App.Usuario.reputacion;
                DisplayAlert("¡Reputación insuficiente!", "¡Para utilizar este beneficio, necesitas alcanzar el nivel 3! Te faltan " + reputacionFaltante + " puntos de reputación.", "Aceptar");
            }
        }

        private void switchDestacado_Toggled(object sender, ToggledEventArgs e)
        {
            if (App.Usuario.nivel >= 3)
            {
                if (stackDestacado.BackgroundColor == Color.Gray)
                {
                    if (stackPrisa.BackgroundColor == Color.DodgerBlue)
                    {
                        switchPrisa.IsToggled = false;
                        stackPrisa.BackgroundColor = Color.Gray;
                    }
                    stackDestacado.BackgroundColor = Color.OrangeRed;
                }
                else
                {
                    stackDestacado.BackgroundColor = Color.Gray;
                }
            }
            else
            {
                switchDestacado.IsToggled = false;
                int reputacionNecesaria = App.ListaNiveles.Where(x => x.nivel == 3).FirstOrDefault().reputacion_necesaria;
                int reputacionFaltante = reputacionNecesaria - App.Usuario.reputacion;
                DisplayAlert("¡Reputación insuficiente!", "¡Para utilizar este beneficio, necesitas alcanzar el nivel 3! Te faltan " + reputacionFaltante + " puntos de reputación.", "Aceptar");
            }
        }

        private void cbDarRecompensa_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            if (App.Usuario.nivel >= 5)
            {
                if (cbDarRecompensa.IsChecked)
                {
                    pickerRecompensas.IsEnabled = true;
                }
                else
                {
                    pickerRecompensas.IsEnabled = false;
                }
            }
            else
            {
                cbDarRecompensa.IsChecked = false;
                int reputacionNecesaria = App.ListaNiveles.Where(x => x.nivel == 3).FirstOrDefault().reputacion_necesaria;
                int reputacionFaltante = reputacionNecesaria - App.Usuario.reputacion;
                DisplayAlert("¡Reputación insuficiente!", "¡Para utilizar este beneficio, necesitas alcanzar el nivel 5! Te faltan " + reputacionFaltante + " puntos de reputación.", "Aceptar");
            }
        }

        private async void btnSolicitarRecomendacion_Clicked(object sender, EventArgs e)
        {
            btnSolicitarRecomendacion.IsEnabled = false;
            CargandoCrearSolicitud.IsVisible = true;
            CargandoCrearSolicitud.IsBusy = true;

            try
            {
                SolicitudRecomendaciones nuevaSolRec = new SolicitudRecomendaciones
                {
                    id_usuario = App.Usuario.id,
                    fecha = DateTime.Now,
                    contenido = !String.IsNullOrEmpty(editorConsideraciones.Text) ? editorConsideraciones.Text.Trim() : null,
                    eliminado = false,
                    nivel_requerido = pickerNivelRequerido.IsEnabled ? Convert.ToInt32(pickerNivelRequerido.SelectedItem) : 1,
                    reputacion_otorgada = cbDarRecompensa.IsChecked ? Convert.ToInt32(pickerRecompensas.SelectedItem) : 0,
                    titulo = NecesidadBasica.Trim(),
                    tipo_producto = TipoProducto.Trim(),
                    visitas = 0,
                    sin_skill = !switchDestacado.IsToggled && !switchPrisa.IsToggled ? true : false,
                    skill_destacado = switchDestacado.IsToggled ? true : false,
                    skill_prisa = switchPrisa.IsToggled ? true : false,
                    leyenda = cbDarRecompensa.IsChecked ? Leyenda + "\r\n\r\n¡Este usuario otorgará " + pickerRecompensas.SelectedItem + " R! Points de recompensa al usuario que recomiende primero!" : Leyenda
                };
                int idNuevaSolRec = await GuardarSolicitudRecomendacion(nuevaSolRec);
                if (idNuevaSolRec > 0) //Se creo correctamente
                {
                    //Creamos la necesidad basica
                    List<NecesidadesRec> listaNecesidadesRec = new List<NecesidadesRec>();
                    listaNecesidadesRec.Add(new NecesidadesRec
                    {
                        id_solicitud_recomendacion = idNuevaSolRec,
                        nombre_necesidad = nuevaSolRec.titulo,
                        es_necesidad_primaria = true
                    });

                    //Creamos las necesidades adicionales
                    foreach (var nec in ListaNecesidadesAdicionales)
                    {
                        listaNecesidadesRec.Add(new NecesidadesRec
                        {
                            id_solicitud_recomendacion = idNuevaSolRec,
                            nombre_necesidad = nec,
                            es_necesidad_primaria = false
                        });
                    }

                    bool regCorrecto = true;
                    foreach (var necesidad in listaNecesidadesRec)
                    {
                        regCorrecto = await GuardarNecesidadRec(necesidad);
                        if (!regCorrecto)
                        {
                            break;
                        }
                    }
                    if (regCorrecto)
                    {
                        await DisplayAlert("Creación exitosa", "¡Solicitud de recomendación creada correctamente!", "Aceptar");
                        App.MasterD.Detail = new NavigationPage(new RecomendacionesProducto(idNuevaSolRec)) { BarTextColor = Color.FromHex("#FF8000") };
                    }
                }
                else
                {
                    await DisplayAlert("¡Lo sentimos!", "Hemos tenido un problema para crear tu solicitud. ¡Inténtelo nuevamente!", "Aceptar");
                    CargandoCrearSolicitud.IsVisible = false;
                    CargandoCrearSolicitud.IsBusy = false;
                    btnSolicitarRecomendacion.IsEnabled = true;
                }
            }
            catch (Exception)
            {
                await DisplayAlert("¡Lo sentimos!", "Hemos tenido un problema para crear tu solicitud. ¡Inténtelo nuevamente!", "Aceptar");
                CargandoCrearSolicitud.IsVisible = false;
                CargandoCrearSolicitud.IsBusy = false;
                btnSolicitarRecomendacion.IsEnabled = true;
            }
        }

        private async Task<int> GuardarSolicitudRecomendacion(SolicitudRecomendaciones solRec)
        {
            return await client.PostAndGetId<SolicitudRecomendaciones>("http://www.recommenditws.somee.com/api/SolicitudRecomendacionesApi", solRec);
        }

        private async Task<bool> GuardarNecesidadRec(NecesidadesRec necesidad)
        {
            return await client.Post<NecesidadesRec>("http://www.recommenditws.somee.com/api/NecesidadesRecApi", necesidad);
        }
    }
}