﻿using Answer_It.MasterDetail;
using Answer_It.Models;
using Negocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Answer_It.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Registrarse : ContentPage
    {
        RestClient client = new RestClient();
        public List<Usuarios> ListaUsuarios = new List<Usuarios>();

        public Registrarse()
        {
            InitializeComponent();

            inputEmail.HelperLabelStyle = new Syncfusion.XForms.TextInputLayout.LabelStyle { FontAttributes = FontAttributes.Bold };
            inputNombreUsuario.HelperLabelStyle = new Syncfusion.XForms.TextInputLayout.LabelStyle { FontAttributes = FontAttributes.Bold };
            inputContrasena.HelperLabelStyle = new Syncfusion.XForms.TextInputLayout.LabelStyle { FontAttributes = FontAttributes.Bold };
            inputRepitaContrasena.HelperLabelStyle = new Syncfusion.XForms.TextInputLayout.LabelStyle { FontAttributes = FontAttributes.Bold };
        }

        public Registrarse(string email, string nombre_usuario, string contrasena)
        {
            InitializeComponent();
            Cargando.IsVisible = false;
            editorEmail.Text = email;
            editorNombreUsuario.Text = nombre_usuario;
            editorContrasena.Text = contrasena;
            editorRepitaContrasena.Text = contrasena;

            inputEmail.HelperLabelStyle = new Syncfusion.XForms.TextInputLayout.LabelStyle { FontAttributes = FontAttributes.Bold };
            inputNombreUsuario.HelperLabelStyle = new Syncfusion.XForms.TextInputLayout.LabelStyle { FontAttributes = FontAttributes.Bold };
            inputContrasena.HelperLabelStyle = new Syncfusion.XForms.TextInputLayout.LabelStyle { FontAttributes = FontAttributes.Bold };
            inputRepitaContrasena.HelperLabelStyle = new Syncfusion.XForms.TextInputLayout.LabelStyle { FontAttributes = FontAttributes.Bold };
        }

        private async void BtnSiguiente_Clicked(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(editorEmail.Text) && !String.IsNullOrEmpty(editorNombreUsuario.Text) && !String.IsNullOrEmpty(editorContrasena.Text) && !String.IsNullOrEmpty(editorRepitaContrasena.Text))
            {
                if (inputEmail.HasError || inputNombreUsuario.HasError || inputContrasena.HasError || inputRepitaContrasena.HasError)
                {
                    await DisplayAlert("Campos con errores", "¡Corrija los errores antes de continuar!", "Aceptar");
                }
                else
                {
                    bool posible = await ComprobarUsuarioExistente(editorNombreUsuario.Text, editorEmail.Text);
                    if (posible)
                    {
                        App.MasterD.Detail = new NavigationPage(new Registrarse_2(editorEmail.Text.Trim(), editorNombreUsuario.Text.Trim(), editorContrasena.Text.Trim())) { BarTextColor = Color.FromHex("#FF8000") };
                    }
                    else
                    {
                        await DisplayAlert("¡Ups!", "Parece que ya existe un usuario registrado con ese nombre o ese email. Intenta con un nombre nuevo, o revisa que no hayas registrado tu correo antes.", "Aceptar");
                    }                    
                }               
            }
            else
            {
                await DisplayAlert("¡Campos incompletos!", "¡Complete todos los campos para continuar!", "Aceptar");
            }
        }

        private async Task<bool> ComprobarUsuarioExistente(string nombre_usuario, string email)
        {
            Cargando.IsBusy = true;
            Cargando.IsVisible = true;
            await CargarUsuarios();
            bool es_posible = false;
            bool existe = ListaUsuarios.Exists(x => x.nombre_usuario == nombre_usuario || x.email == email);
            if (existe)
            {
                es_posible = false;
            }
            else
            {
                es_posible = true;
            }
            Cargando.IsVisible = false;
            Cargando.IsBusy = false;

            return es_posible;
        }

        private void lblIngresa_Tapped(object sender, EventArgs e)
        {
            App.MasterD.Detail = new NavigationPage(new IniciarSesion()) { BarTextColor = Color.FromHex("#FF8000") };
        }

        protected override bool OnBackButtonPressed()
        {
            App.MasterD.Detail = new NavigationPage(new Inicio()) { BarTextColor = Color.FromHex("#FF8000") };
            return true;
        }

        //CONTROL DE ERRORES EN INPUTS

        //EMAIL
        private void editorEmail_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (editorEmail.Text.Length > 0)
            {
                if (!editorEmail.Text.Contains("@"))
                {
                    inputEmail.HasError = true;
                    inputEmail.ErrorText = "¡Email inválido!";
                }
                else
                {
                    if (editorEmail.Text.Length > 30)
                    {
                        inputEmail.HasError = true;
                        inputEmail.ErrorText = "¡Máximo 40 caracteres!";
                    }
                    else
                    {
                        inputEmail.HasError = false;
                    }
                }               
            }           
        }

        //NOMBRE USUARIO
        private void editorNombreUsuario_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (editorNombreUsuario.Text.Length > 20)
            {
                inputNombreUsuario.HasError = true;
                inputNombreUsuario.ErrorText = "¡Máximo 20 caracteres!";
            }
            else
            {
                inputNombreUsuario.HasError = false;
            }
        }

        //CONTRASEÑA
        private void editorContrasena_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (editorContrasena.Text.Length > 0)
            {
                if (editorContrasena.Text.Length > 30)
                {
                    inputContrasena.HasError = true;
                    inputContrasena.ErrorText = "¡Máximo 30 caracteres!";
                }
                else
                {
                    if (editorContrasena.Text.Length < 5)
                    {
                        inputContrasena.HasError = true;
                        inputContrasena.ErrorText = "¡Mínimo 5 caracteres!";
                    }
                    else
                    {
                        inputContrasena.HasError = false;
                    }
                }               
            }
        }

        //REPETIR CONTRASEÑA
        private void editorRepitaContrasena_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (editorRepitaContrasena.Text.Length > 0)
            {
                if (editorRepitaContrasena.Text != editorContrasena.Text)
                {
                    inputRepitaContrasena.HasError = true;
                    inputRepitaContrasena.ErrorText = "¡Las contraseñas no coinciden!";
                }
                else
                {
                    inputRepitaContrasena.HasError = false;
                }
            }
        }

        private async Task<bool> CargarUsuarios()
        {
            ListaUsuarios = await client.GetAll<Usuarios>("http://www.recommenditws.somee.com/api/UsuariosApi");
            return true;
        }
    }
}