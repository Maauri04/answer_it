﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Syncfusion.XForms.TextInputLayout;
using Answer_It.Models;

namespace Answer_It.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SolicitarRecomendacion : ContentPage
    {
        public SolicitarRecomendacion()
        {
            InitializeComponent();
        }

        private void cbNoSeElProducto_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            if (cbNoSeElProducto.IsChecked)
            {
                inputTipoDeProducto.FocusedColor = Color.DarkGray;
                inputTipoDeProducto.Hint = "Sin especificar.";
                editorTipoDeProducto.IsEnabled = false;
            }
            else
            {
                inputTipoDeProducto.FocusedColor = Color.FromHex("#FF8000");
                inputTipoDeProducto.Hint = "Ej: Lavadora, escoba.. ";
                editorTipoDeProducto.IsEnabled = true;
            }
        }

        protected override bool OnBackButtonPressed()
        {
            Navigation.PopAsync();
            return true;
        }

        private void btnSiguiente_Clicked(object sender, EventArgs e)
        {
            if (!cbNoSeElProducto.IsChecked)
            {
                if (String.IsNullOrEmpty(editorTipoDeProducto.Text))
                {
                    DisplayAlert("Tipo de producto", "Si no sabes el tipo de producto, ¡No olvides de marcar la casilla de 'No sé que producto necesito'!", "Aceptar");
                }
                else
                {
                    Navigation.PushAsync(new SolicitarRecomendacion_2(editorTipoDeProducto.Text));
                }
            }
            else
            {
                Navigation.PushAsync(new SolicitarRecomendacion_2(editorTipoDeProducto.Text));
            }
        }
    }
}