﻿using Answer_It.MasterDetail;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Answer_It.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SplashPage : ContentPage
    {
        //Control PRIMERA VEZ (Para el OnBoarding)
        public bool primera_vez { get; set; }
        string _fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "primera_vez.txt");
        //File.Delete(_fileName); para borrar el archivo

        public SplashPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);    
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            SplashImage.Opacity = 1;
            SplashBot.Opacity = 0;

            SplashBot.FadeTo(1, 1000);
            SplashImage.FadeTo(1, 1000);

            DelayIngreso();

            if (!App.TieneConexion())
            {
                await DisplayAlert("Error de conexión", "Parece que no tienes conexión a internet. Conéctate a internet para poder utilizar Recommend It!", "Aceptar");
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
        }

        public bool EsPrimeraVez()
        {
            if (File.Exists(_fileName))
            {
                primera_vez = false;
            }
            else
            {
                primera_vez = true;
                File.WriteAllText(_fileName, "primeraVez");
            }
            return primera_vez; 
        }

        private void DelayIngreso()
        {
            Device.StartTimer(TimeSpan.FromSeconds(8), () => //5 seg cambiar
            {
                bool prim_vez = false; //EsPrimeraVez(); 
                if (prim_vez)
                {
                    Application.Current.MainPage = new OnBoardingAnimationPage();
                }
                else
                {
                    Application.Current.MainPage = new HomePage();
                }
                return false; // return true to repeat counting, false to stop timer
            });
        }

        //private void btnComunidad_Clicked(object sender, EventArgs e)
        //{
        //    Application.Current.MainPage = new HomePage();
        //}
    }
}