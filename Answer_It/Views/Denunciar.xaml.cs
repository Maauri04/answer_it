﻿using Answer_It.MasterDetail;
using Answer_It.Models;
using Answer_It.ViewModels;
using Negocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Answer_It.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Denunciar : ContentPage
    {
        RestClient client = new RestClient();
        List<Valoraciones> ListaValoraciones = new List<Valoraciones>();
        List<Recomendaciones> ListaRecomendaciones = new List<Recomendaciones>();
        List<Usuarios> ListaUsuarios = new List<Usuarios>();
        public string _tipoComentario { get; set; }
        public int _idValoracionRecomendacion { get; set; }
        public Usuarios _usuarioDenunciado { get; set; }

        public Denunciar(int idValoracionRecomendacion, string tipoComentario) //Puedo recibir idValoracion, idRecomendacion
        {
            InitializeComponent();
            CargarPagina(idValoracionRecomendacion, tipoComentario);
        }

        private async Task<bool> CargarPagina(int idValoracionRecomendacion, string tipoComentario)
        {
            await CargarUsuarios();
            await CargarValoraciones();
            await CargarRecomendaciones();
            _tipoComentario = tipoComentario;
            _idValoracionRecomendacion = idValoracionRecomendacion;
            CargarTipoDenuncia(idValoracionRecomendacion, _tipoComentario);
            return true;
        }

        private void CargarTipoDenuncia(int idValRec, string tipoComentario)
        {
            if (tipoComentario == "valoracion") //Denuncian una valoracion
            {
                Title = "Denunciar valoración";
                cbProductoFalso.IsVisible = false;
                lblProductoFalso.IsVisible = false;
                Valoraciones val = ListaValoraciones.Where(x => x.id == idValRec).FirstOrDefault();
                _usuarioDenunciado = ListaUsuarios.Where(x => x.id == val.id_usuario).FirstOrDefault();
                lblTituloDenuncia.Text = "Denunciar valoración realizada por " + _usuarioDenunciado.nombre_usuario;
            }
            else //Denuncian una recomendacion
            {
                Title = "Denunciar recomendación";
                cbProductoFalso.IsVisible = true;
                lblProductoFalso.IsVisible = true;
                Recomendaciones rec = ListaRecomendaciones.Where(x => x.id == idValRec).FirstOrDefault();
                _usuarioDenunciado = ListaUsuarios.Where(x => x.id == rec.id_usuario).FirstOrDefault();
                lblTituloDenuncia.Text = "Denunciar recomendación realizada por " + _usuarioDenunciado.nombre_usuario;
            }
        }

        private void cbOtro_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            if (cbOtro.IsChecked)
            {
                editorMotivo.IsEnabled = true;
                cbComentarioAmbiguoContradictorio.IsChecked = false;
                cbContenidoInapropiado.IsChecked = false;
                cbProductoFalso.IsChecked = false;
            }
            else
            {
                editorMotivo.IsEnabled = false;
            }
        }

        private void cbComentarioAmbiguoContradictorio_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            if (cbComentarioAmbiguoContradictorio.IsChecked)
            {
                cbContenidoInapropiado.IsChecked = false;
                cbProductoFalso.IsChecked = false;
                cbOtro.IsChecked = false;
            }
        }

        private void cbContenidoInapropiado_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            if (cbContenidoInapropiado.IsChecked)
            {
                cbComentarioAmbiguoContradictorio.IsChecked = false;
                cbProductoFalso.IsChecked = false;
                cbOtro.IsChecked = false;
            }
        }

        private void cbProductoFalso_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            if (cbProductoFalso.IsChecked)
            {
                cbComentarioAmbiguoContradictorio.IsChecked = false;
                cbContenidoInapropiado.IsChecked = false;
                cbOtro.IsChecked = false;
            }
        }

        private async void btnDenunciar_Clicked(object sender, EventArgs e)
        {
            string motivo = "";
            bool denunciar = false;
            //bool checkProductoFalso_estaOculto = false;
            //if (_tipoComentario == "recomendacion")
            //{
            //    checkProductoFalso_estaOculto = false;
            //}
            //else
            //{
            //    checkProductoFalso_estaOculto = true;
            //}

            if (cbComentarioAmbiguoContradictorio.IsChecked || cbContenidoInapropiado.IsChecked || cbProductoFalso.IsChecked || cbOtro.IsChecked)
            {
                if (cbOtro.IsChecked)
                {
                    if (String.IsNullOrEmpty(editorMotivo.Text.Trim()))
                    {
                        DisplayAlert("¡Campos incompletos!", "Has seleccionado otro motivo de denuncia, pero no has escrito tu motivo.", "Aceptar");                 
                    }
                    else
                    {
                        denunciar = true;
                        motivo = editorMotivo.Text;
                    }
                }
                else
                {
                    if (cbComentarioAmbiguoContradictorio.IsChecked)
                    {
                        denunciar = true;
                        motivo = lblComentarioAmbiguoContradictorio.Text;
                    }
                    else
                    {
                        if (cbContenidoInapropiado.IsChecked)
                        {
                            denunciar = true;
                            motivo = lblContenidoInapropiado.Text;
                        }
                        else
                        {
                            denunciar = true;
                            motivo = lblProductoFalso.Text;
                        }
                    }
                }
                if (denunciar)
                {
                    //Genero la denuncia con el motivo.
                    switch (_tipoComentario)
                    {
                        case "valoracion":
                            DenunciasValoraciones denunciaVal = new DenunciasValoraciones
                            {
                                fecha_denuncia = DateTime.Now,
                                id_usuario_denunciado = _usuarioDenunciado.id,
                                id_usuario_denunciante = App.Usuario.id,
                                id_valoracion = _idValoracionRecomendacion,
                                motivo = motivo,
                                reputacion_descontada = 50
                            };
                            bool resultVal = await AgregarDenunciaValoraciones(denunciaVal);
                            if (resultVal)
                            {
                                await DisplayAlert("Valoración denunciada", "¡Gracias por tu aporte! Revisaremos tu denuncia a la brevedad.", "Aceptar");
                            }
                            else
                            {
                                DisplayAlert("Error", "Lo sentimos, no hemos podido crear la denuncia. Inténtelo más tarde nuevamente.", "Aceptar");
                            }
                            break;
                        case "recomendacion":
                            DenunciasRecomendaciones denunciaRec = new DenunciasRecomendaciones
                            {
                                fecha_denuncia = DateTime.Now,
                                id_usuario_denunciado = _usuarioDenunciado.id,
                                id_usuario_denunciante = App.Usuario.id,
                                id_recomendacion = _idValoracionRecomendacion,
                                motivo = motivo,
                                reputacion_descontada = 50
                            };
                            bool resultRec = await AgregarDenunciaRecomendaciones(denunciaRec);
                            if (resultRec)
                            {
                                await DisplayAlert("Recomendación denunciada", "¡Gracias por tu aporte! Revisaremos tu denuncia a la brevedad.", "Aceptar");
                            }
                            else
                            {
                                await DisplayAlert("Error", "Lo sentimos, no hemos podido crear la denuncia. Inténtelo más tarde nuevamente.", "Aceptar");
                            }
                            break;
                        default: //error
                            await DisplayAlert("Error", "Lo sentimos, no hemos podido crear la denuncia. Inténtelo más tarde nuevamente.", "Aceptar");
                            break;
                    }
                    App.MasterD.Detail = new NavigationPage(new Inicio()) { BarTextColor = Color.FromHex("#FF8000") };
                }
            }
            else
            {
                await DisplayAlert("¡Campos incompletos!", "Seleccione un motivo de denuncia para continuar.", "Aceptar");
            }
        }

        private async Task<bool> CargarUsuarios()
        {
            try
            {
                ListaUsuarios = await client.GetAll<Usuarios>("http://www.recommenditws.somee.com/api/UsuariosApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> AgregarDenunciaValoraciones(DenunciasValoraciones den)
        {
            bool correcto = await client.Post<DenunciasValoraciones>("http://www.recommenditws.somee.com/api/DenunciasValoracionesApi", den);
            return correcto;
        }

        private async Task<bool> AgregarDenunciaRecomendaciones(DenunciasRecomendaciones den)
        {
            bool correcto = await client.Post<DenunciasRecomendaciones>("http://www.recommenditws.somee.com/api/DenunciasRecomendacionesApi", den);
            return correcto;
        }

        private async Task<bool> CargarValoraciones()
        {
            try
            {
                ListaValoraciones = await client.GetAll<Valoraciones>("http://www.recommenditws.somee.com/api/ValoracionesApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarRecomendaciones()
        {
            try
            {
                ListaRecomendaciones = await client.GetAll<Recomendaciones>("http://www.recommenditws.somee.com/api/RecomendacionesApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}