﻿using Answer_It.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ImageCircle.Forms.Plugin.Abstractions;
using Negocio;
using System.IO;
using Answer_It.Models;

namespace Answer_It.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RecomendacionesProducto : ContentPage
    {
        RestClient client = new RestClient();
        public SolicitudRecomendacionesViewModel _solicitudRecomendacion { get; set; }
        public SolicitudRecomendaciones solicitudRecomendacion { get; set; }
        List<Recomendaciones> ListaRecomendaciones = new List<Recomendaciones>(); //Informacion que se muestra en la listview.
        List<Productos> ListaProductos = new List<Productos>();
        List<ImagenesProductos> ListaImagenes = new List<ImagenesProductos>();
        List<SolicitudRecomendaciones> ListaSolicitudRecomendaciones = new List<SolicitudRecomendaciones>();
        List<ImagenesUsuarios> ListaImagenesUsu = new List<ImagenesUsuarios>();
        List<Usuarios> ListaUsuarios = new List<Usuarios>();
        List<NecesidadesRec> ListaNecesidades = new List<NecesidadesRec>();
        List<ValoracionesMeGusta> ListaValoracionesMeGusta = new List<ValoracionesMeGusta>();
        public int id_solicitud_rec { get; set; }
        public bool puedeActualizarMg { get; set; } = true;


        public RecomendacionesProducto(int idSolicitudRecomendacion)
        {
            InitializeComponent();
            id_solicitud_rec = idSolicitudRecomendacion;
            CargarPagina();
        }

        private async Task<bool> CargarPagina()
        {
            await CargarUsuarios();
            await CargarProductos();
            await CargarRecomendaciones();
            await CargarSolicitudRecomendaciones();
            await CargarNecesidades();
            await CargarValoracionesMeGusta();

            solicitudRecomendacion = ListaSolicitudRecomendaciones.Where(x => x.id == id_solicitud_rec).First();
            List<NecesidadesRec> ListaNecesidadesVm = new List<NecesidadesRec>();
            ListaNecesidadesVm.Clear();
            foreach (var item in ListaNecesidades)
            {
                if (item.id_solicitud_recomendacion == solicitudRecomendacion.id)
                {
                    ListaNecesidadesVm.Add(new NecesidadesRec
                    {
                        id = item.id,
                        id_solicitud_recomendacion = item.id_solicitud_recomendacion,
                        nombre_necesidad = item.nombre_necesidad
                    });
                }
            }
            _solicitudRecomendacion = new SolicitudRecomendacionesViewModel
            {
                id = solicitudRecomendacion.id,
                nombre_usuario = ListaUsuarios.Where(x => x.id == solicitudRecomendacion.id_usuario).First().nombre_usuario,
                titulo = solicitudRecomendacion.titulo,
                contenido = solicitudRecomendacion.contenido,
                fecha_publicacion = solicitudRecomendacion.fecha.GetValueOrDefault(DateTime.Now),
                leyenda_publicacion = solicitudRecomendacion.leyenda,
                nivel_requerido = solicitudRecomendacion.nivel_requerido.GetValueOrDefault(1),
                reputacion_otorgada = solicitudRecomendacion.reputacion_otorgada,
                tipo_producto = solicitudRecomendacion.tipo_producto,
                visitas = solicitudRecomendacion.visitas.GetValueOrDefault(0),
                sin_skill = solicitudRecomendacion.sin_skill,
                skill_prisa = solicitudRecomendacion.skill_prisa,
                skill_destacado = solicitudRecomendacion.skill_destacado,
                lista_necesidades = ListaNecesidadesVm
            };
            ListaRecomendaciones = ListaRecomendaciones.Where(x => x.id_solicitud_recomendacion == solicitudRecomendacion.id).ToList();

            if (!App.HaySesion())
            {
                btnRecomendar.IsEnabled = false;
                btnRecomendar.Text = "¡Inicia sesión para recomendar!";
            }
            else
            {
                btnRecomendar.Text = "Recomendar un producto";
                if (App.Usuario.nivel < solicitudRecomendacion.nivel_requerido) //Nivel requerido?
                {
                    lblReputacionRequerida.Text = "Nivel de reputación " + solicitudRecomendacion.nivel_requerido + " requerido";
                    LeyendaSinReputacion.IsVisible = true;
                    btnRecomendar.IsEnabled = false;
                }
                else
                {
                    LeyendaSinReputacion.IsVisible = false;
                    bool usuYaRecomendo = ListaRecomendaciones.Exists(x => x.id_usuario == App.Usuario.id);
                    if (usuYaRecomendo)
                    {
                        LeyendaYaRecomendo.IsVisible = true;
                        btnRecomendar.IsEnabled = false;
                    }
                    else //Aun no recomendo y tiene el nivel requerido, puede recomendar.
                    {
                        LeyendaYaRecomendo.IsVisible = false;
                        btnRecomendar.IsEnabled = true;
                    }
                }
            }

            lblUsuSolicitante.Text = _solicitudRecomendacion.nombre_usuario;
            imgUsuarioCreador.Source = await CargarFotoUsuarioCreador(solicitudRecomendacion.id_usuario);
            lblVisitas.Text = _solicitudRecomendacion.visitas.ToString() + " visitas";
            lblFechaSolicitud.Text = "Publicado " + CalcularFechaPublicacion(_solicitudRecomendacion.fecha_publicacion);
            lblContenidoAdicional.Text = String.IsNullOrEmpty(_solicitudRecomendacion.contenido) ? "Sin consideraciones." : "\"" + _solicitudRecomendacion.contenido + "\"";
            lblTipoProducto.Text = String.IsNullOrEmpty(_solicitudRecomendacion.tipo_producto) ? "No especifica." : _solicitudRecomendacion.tipo_producto;

            //Cargo necesidades
            foreach (var item in _solicitudRecomendacion.lista_necesidades)
            {
                Label labelNecesidad = new Label { Text = item.nombre_necesidad, FontAttributes = FontAttributes.Bold, TextColor = Color.Black };
                StackLayout stackLabelNecesidad = new StackLayout { HorizontalOptions = LayoutOptions.Start };
                Frame frameNecesidad = new Frame { CornerRadius = 10, Margin = new Thickness(0, 4, 0, 4) };
                stackLabelNecesidad.Children.Add(labelNecesidad);
                frameNecesidad.Content = stackLabelNecesidad;
                stackNecesidades.Children.Add(frameNecesidad);
            }

            //Cargo consideraciones
            TituloNecesidad.Text = _solicitudRecomendacion.titulo;

            //Cargo recomendaciones de la solicitud          
            foreach (var item in ListaRecomendaciones)
            {
                Usuarios usuRecomendador = ListaUsuarios.Where(x => x.id == item.id_usuario).FirstOrDefault();

                //Stack usuario recomendador
                ImageSource imgUsuRec = await CargarFotoUsuarioRecomendador(usuRecomendador.id);
                CircleImage circleImage = new CircleImage { Source = imgUsuRec, HeightRequest = 28, Margin = new Thickness(4, 0, 0, 0) };
                App.BorrarImagenLocalmente(imgUsuRec);
                Label lblUsuario = new Label { Text = usuRecomendador.nombre_usuario, Margin = new Thickness(4, 0, 0, 0), VerticalTextAlignment = TextAlignment.Center, FontAttributes = FontAttributes.Bold, FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)), TextColor = Color.FromHex("#FF8000") };
                //Image imgReputacion = new Image { Source = "reputacion_logo.png", HeightRequest = 30 };
                StackLayout stackUser = new StackLayout { BackgroundColor = Color.FromHex("#383838"), Orientation = StackOrientation.Horizontal, Padding = 6 };
                stackUser.Children.Add(circleImage); stackUser.Children.Add(lblUsuario); //stackUser.Children.Add(imgReputacion);
                Frame frameUser = new Frame { CornerRadius = 10, Padding = 0, Margin = new Thickness(0, 0, 0, 4), HorizontalOptions = LayoutOptions.Start };
                frameUser.Content = stackUser;
                Image imgBorrar = new Image { Source = "ic_borrar.png", HorizontalOptions = LayoutOptions.EndAndExpand, Margin = new Thickness(0, 0, 10, 0) };
                Image imgMeGusta = new Image { Source = "ic_megusta_off.png", HorizontalOptions = LayoutOptions.EndAndExpand, Margin = new Thickness(0, 0, 3, 0), IsVisible = false, HeightRequest = 30 };
                Image imgNoMeGusta = new Image { Source = "ic_nomegusta_off.png", HorizontalOptions = LayoutOptions.End, Margin = new Thickness(0, 0, 10, 0), IsVisible = false, HeightRequest = 30 };
                StackLayout stackUserGeneral = new StackLayout { Orientation = StackOrientation.Horizontal, HorizontalOptions = LayoutOptions.FillAndExpand };
                stackUserGeneral.Children.Add(frameUser);
                if (usuRecomendador.id == App.Usuario.id)
                {
                    var tapGestureRecognizerBorrar = new TapGestureRecognizer();
                    tapGestureRecognizerBorrar.Tapped += async (sender, e) =>
                    {
                        var result = await DisplayAlert("Borrar recomendación", "¿Estás seguro que querés borrar esta recomendación?", "Sí", "No");
                        if (result)
                        {
                            await EliminarRecomendacion(item.id);
                            stackNecesidades.Children.Clear();
                            View recomendacionABorrar = StackRecomendacionesParcial.Children.Where(x => x.ClassId == item.id.ToString()).FirstOrDefault();
                            StackRecomendacionesParcial.Children.Remove(recomendacionABorrar);
                            await CargarPagina();
                        }
                    };
                    imgBorrar.GestureRecognizers.Add(tapGestureRecognizerBorrar);
                    stackUserGeneral.Children.Add(imgBorrar);
                }
                else
                {
                    if (App.Usuario.nivel >= 2)
                    {
                        imgMeGusta.IsVisible = true;
                        imgNoMeGusta.IsVisible = true;
                        ValoracionesMeGusta valMeGusta = new ValoracionesMeGusta();
                        valMeGusta = ListaValoracionesMeGusta.Where(x => x.id_valoracion == item.id && x.id_usuario_like == App.Usuario.id).FirstOrDefault();
                        if (valMeGusta != null)
                        {
                            if (valMeGusta.me_gusta)
                            {
                                imgMeGusta.Source = "ic_megusta_on.png";
                                imgNoMeGusta.Source = "ic_nomegusta_off.png";
                            }
                            else
                            {
                                imgMeGusta.Source = "ic_megusta_off.png";
                                imgNoMeGusta.Source = "ic_nomegusta_on.png";
                            }
                        }
                    }

                    //OnTouch "me gusta"
                    var tapGestureRecognizerMeGusta = new TapGestureRecognizer();
                    tapGestureRecognizerMeGusta.Tapped += async (sender, e) =>
                    {
                        bool MeGusta = false;
                        bool NoMeGusta = false;
                        ValoracionesMeGusta valMeGusta = new ValoracionesMeGusta();
                        valMeGusta = ListaValoracionesMeGusta.Where(x => x.id_valoracion == item.id && x.id_usuario_like == App.Usuario.id).FirstOrDefault();
                        if (valMeGusta != null) //Ya valoró este comentario 
                        {
                            if (valMeGusta.me_gusta) //Si ya tenia me gusta..
                            {
                                imgMeGusta.Source = "ic_megusta_off.png";
                                imgNoMeGusta.Source = "ic_nomegusta_off.png";
                                if (puedeActualizarMg) //Evito que se pisen actualizaciones en los likes simultaneas
                                {
                                    puedeActualizarMg = false;
                                    item.me_gusta -= 1;
                                    bool actualizacionCorrecta = await ActualizarMeGustaEnRecomendaciones(item); //Actualizo contador de likes en Valoraciones
                                    if (actualizacionCorrecta)
                                    {
                                        bool eliminadoCorrecto = await EliminarMeGusta(valMeGusta.id);
                                        if (eliminadoCorrecto)
                                        {
                                            puedeActualizarMg = true;
                                        }
                                        else
                                        {
                                            DisplayAlert("Error", "Error del servidor, intentelo más tarde.", "Aceptar");
                                        }
                                    }
                                    else
                                    {
                                        DisplayAlert("Error", "Error del servidor, intentelo más tarde.", "Aceptar");
                                    }
                                }
                            }
                            else
                            {
                                if (valMeGusta.no_me_gusta) //Si ya tenia un no me gusta..
                                {
                                    imgNoMeGusta.Source = "ic_nomegusta_off.png";
                                    imgMeGusta.Source = "ic_megusta_on.png";
                                    MeGusta = true;
                                    NoMeGusta = false;
                                    if (puedeActualizarMg)
                                    {
                                        puedeActualizarMg = false;
                                        item.me_gusta += 1;
                                        bool actualizacionCorrecta = await ActualizarMeGustaEnRecomendaciones(item);
                                        if (actualizacionCorrecta)
                                        {
                                            valMeGusta.me_gusta = MeGusta;
                                            valMeGusta.no_me_gusta = NoMeGusta;
                                            bool actCorrecta = await ActualizarMeGusta(valMeGusta);
                                            if (actCorrecta)
                                            {
                                                puedeActualizarMg = true;
                                            }
                                            else
                                            {
                                                DisplayAlert("Error", "Error del servidor, intentelo más tarde.", "Aceptar");
                                            }
                                        }
                                        else
                                        {
                                            DisplayAlert("Error", "Error del servidor, intentelo más tarde.", "Aceptar");
                                        }
                                    }
                                }
                            }
                        }
                        else //No valoró este comentario
                        {
                            imgMeGusta.Source = "ic_megusta_on.png";
                            imgNoMeGusta.Source = "ic_nomegusta_off.png";
                            MeGusta = true;
                            NoMeGusta = false;
                            if (puedeActualizarMg)
                            {
                                puedeActualizarMg = false;
                                item.me_gusta += 1;
                                bool actualizacionCorrecta = await ActualizarMeGustaEnRecomendaciones(item);
                                if (actualizacionCorrecta)
                                {
                                    ValoracionesMeGusta nuevaValMg = new ValoracionesMeGusta
                                    {
                                        id_usuario_like = App.Usuario.id,
                                        id_valoracion = item.id,
                                        me_gusta = MeGusta,
                                        no_me_gusta = NoMeGusta
                                    };
                                    bool actCorrecta = await AgregarMeGusta(nuevaValMg);
                                    if (actCorrecta)
                                    {
                                        puedeActualizarMg = true;
                                    }
                                    else
                                    {
                                        DisplayAlert("Error", "Error del servidor, intentelo más tarde.", "Aceptar");
                                    }
                                }
                                else
                                {
                                    DisplayAlert("Error", "Error del servidor, intentelo más tarde.", "Aceptar");
                                }
                            }
                        }
                    };
                    imgMeGusta.GestureRecognizers.Add(tapGestureRecognizerMeGusta);

                    //OnTouch "no me gusta"
                    var tapGestureRecognizerNoMeGusta = new TapGestureRecognizer();
                    tapGestureRecognizerNoMeGusta.Tapped += async (sender, e) =>
                    {
                        bool MeGusta = false;
                        bool NoMeGusta = false;
                        ValoracionesMeGusta valMeGusta = new ValoracionesMeGusta();
                        valMeGusta = ListaValoracionesMeGusta.Where(x => x.id_valoracion == item.id && x.id_usuario_like == App.Usuario.id).FirstOrDefault();
                        if (valMeGusta != null)
                        {
                            if (valMeGusta.no_me_gusta) //Si ya tenia no me gusta..
                            {
                                imgNoMeGusta.Source = "ic_nomegusta_off.png";
                                imgMeGusta.Source = "ic_megusta_off.png";
                                if (puedeActualizarMg) //Evito que se pisen actualizaciones en los likes simultaneas
                                {
                                    puedeActualizarMg = false;
                                    item.me_gusta += 1;
                                    bool actualizacionCorrecta = await ActualizarMeGustaEnRecomendaciones(item); //Actualizo contador de likes en Valoraciones
                                    if (actualizacionCorrecta)
                                    {
                                        bool eliminarcionCorrecta = await EliminarMeGusta(valMeGusta.id);
                                        if (eliminarcionCorrecta)
                                        {
                                            puedeActualizarMg = true;
                                        }
                                        else
                                        {
                                            DisplayAlert("Error", "Error del servidor, intentelo más tarde.", "Aceptar");
                                        }
                                    }
                                    else
                                    {
                                        DisplayAlert("Error", "Error del servidor, intentelo más tarde.", "Aceptar");
                                    }
                                }
                            }
                            else
                            {
                                if (valMeGusta.me_gusta) //Si ya tenia un me gusta..
                                {
                                    imgMeGusta.Source = "ic_megusta_off.png";
                                    imgNoMeGusta.Source = "ic_nomegusta_on.png";
                                    MeGusta = false;
                                    NoMeGusta = true;
                                    if (puedeActualizarMg) //Evito que se pisen actualizaciones en los likes simultaneas
                                    {
                                        puedeActualizarMg = false;
                                        item.me_gusta -= 1;
                                        bool actualizacionCorrecta = await ActualizarMeGustaEnRecomendaciones(item); //Actualizo contador de likes en Valoraciones
                                        if (actualizacionCorrecta)
                                        {
                                            valMeGusta.me_gusta = MeGusta;
                                            valMeGusta.no_me_gusta = NoMeGusta;
                                            bool actCorrecta = await ActualizarMeGusta(valMeGusta);
                                            if (actCorrecta)
                                            {
                                                puedeActualizarMg = true;
                                            }
                                            else
                                            {
                                                DisplayAlert("Error", "Error del servidor, intentelo más tarde.", "Aceptar");
                                            }
                                        }
                                        else
                                        {
                                            DisplayAlert("Error", "Error del servidor, intentelo más tarde.", "Aceptar");
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            imgNoMeGusta.Source = "ic_nomegusta_on.png";
                            imgMeGusta.Source = "ic_megusta_off.png";
                            NoMeGusta = true;
                            MeGusta = false;
                            if (puedeActualizarMg) //Evito que se pisen actualizaciones en los likes simultaneas
                            {
                                puedeActualizarMg = false;
                                item.me_gusta -= 1;
                                bool actualizacionCorrecta = await ActualizarMeGustaEnRecomendaciones(item); //Actualizo contador de likes en Valoraciones
                                if (actualizacionCorrecta)
                                {
                                    ValoracionesMeGusta nuevaValMg = new ValoracionesMeGusta
                                    {
                                        id_usuario_like = App.Usuario.id,
                                        id_valoracion = item.id,
                                        me_gusta = MeGusta,
                                        no_me_gusta = NoMeGusta
                                    };
                                    bool agregadoCorrecto = await AgregarMeGusta(valMeGusta);
                                    if (agregadoCorrecto)
                                    {
                                        puedeActualizarMg = true;
                                    }
                                    else
                                    {
                                        DisplayAlert("Error", "Error del servidor, intentelo más tarde.", "Aceptar");
                                    }
                                }
                                else
                                {
                                    DisplayAlert("Error", "Error del servidor, intentelo más tarde.", "Aceptar");
                                }
                            }
                        }
                    };
                    imgNoMeGusta.GestureRecognizers.Add(tapGestureRecognizerNoMeGusta);

                    stackUserGeneral.Children.Add(imgMeGusta);
                    stackUserGeneral.Children.Add(imgNoMeGusta);
                }

                Productos productoRec = ListaProductos.Where(x => x.id == item.id_producto_recomendado).First();

                //Stack producto recomendado
                ImageSource imgProd = await CargarFotoProductos(productoRec.id);
                Image imgProducto = new Image { Source = imgProd, HeightRequest = 28, Margin = new Thickness(4, 0, 0, 0) };
                App.BorrarImagenLocalmente(imgProd);
                Label lblProducto = new Label { Text = productoRec.nombre, Margin = new Thickness(4, 0, 0, 0), VerticalTextAlignment = TextAlignment.Center, FontAttributes = FontAttributes.Bold, FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)), TextColor = Color.FromHex("#FF8000") };
                StackLayout stackProducto = new StackLayout { BackgroundColor = Color.FromHex("#383838"), Orientation = StackOrientation.Horizontal, Padding = 6 };
                stackProducto.Children.Add(imgProducto); stackProducto.Children.Add(lblProducto);
                Frame frameProducto = new Frame { CornerRadius = 10, Padding = 0, Margin = new Thickness(0, 0, 0, 8) };
                //Gesture recognizer para el frame
                var tapGestureRecognizerProducto = new TapGestureRecognizer();
                tapGestureRecognizerProducto.Tapped += (sender, e) =>
                {
                    Navigation.PushAsync(new ValoracionesProducto(productoRec.id));
                };
                frameProducto.GestureRecognizers.Add(tapGestureRecognizerProducto);
                frameProducto.Content = stackProducto;
                Label lblHaRecomendado = new Label { Text = "Ha recomendado:", TextColor = Color.Black, FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)), FontAttributes = FontAttributes.Bold };
                Label lblReportar = new Label { Text = "¡Reportar!", IsVisible = false, FontAttributes = FontAttributes.Italic, HorizontalOptions = LayoutOptions.EndAndExpand, HorizontalTextAlignment = TextAlignment.End, TextDecorations = TextDecorations.Underline, FontSize = 12, TextColor = Color.Red };
                if (usuRecomendador.id != App.Usuario.id)
                {
                    lblReportar.IsVisible = true;
                    var tapGestureRecognizerReportar = new TapGestureRecognizer();
                    tapGestureRecognizerReportar.Tapped += (sender, e) =>
                    {
                        Navigation.PushAsync(new Denunciar(item.id, "recomendacion"));
                    };
                    lblReportar.GestureRecognizers.Add(tapGestureRecognizerReportar);
                }
                StackLayout stackHaRecomendadoYDenuncia = new StackLayout { Padding = 0, Spacing = 0, Orientation = StackOrientation.Horizontal };
                stackHaRecomendadoYDenuncia.Children.Add(lblHaRecomendado);
                stackHaRecomendadoYDenuncia.Children.Add(lblReportar);

                //Stack footer producto recomendado
                Image imgEstadoCara = new Image { Source = App.MeGustaEnIcono(item.me_gusta) };
                Label lblMeGusta = new Label { Text = item.me_gusta.ToString(), TextColor = Color.Black, FontAttributes = FontAttributes.Bold };
                StackLayout stackMeGusta = new StackLayout { Orientation = StackOrientation.Horizontal, HorizontalOptions = LayoutOptions.Start, VerticalOptions = LayoutOptions.EndAndExpand };
                stackMeGusta.Children.Add(imgEstadoCara); stackMeGusta.Children.Add(lblMeGusta);
                Label lblFechaPublicacion = new Label { Text = CalcularFechaPublicacion(item.fecha), FontSize = Device.GetNamedSize(NamedSize.Caption, typeof(Label)), HorizontalOptions = LayoutOptions.EndAndExpand };
                StackLayout stackFooter = new StackLayout { Orientation = StackOrientation.Horizontal };
                stackFooter.Children.Add(stackMeGusta); stackFooter.Children.Add(lblFechaPublicacion);
                Label lblComentario = new Label { Margin = new Thickness(0, 0, 0, 6), Text = "\"" + item.contenido + "\"", FontAttributes = FontAttributes.Italic };

                StackLayout stackMarcoParcial = new StackLayout { Padding = new Thickness(14, 0, 14, 14), HorizontalOptions = LayoutOptions.FillAndExpand };
                stackMarcoParcial.Children.Add(stackHaRecomendadoYDenuncia); stackMarcoParcial.Children.Add(frameProducto);
                stackMarcoParcial.Children.Add(lblComentario); stackMarcoParcial.Children.Add(stackFooter);

                StackLayout stackMarcoGlobal = new StackLayout();
                stackMarcoGlobal.Children.Add(stackUserGeneral);
                stackMarcoGlobal.Children.Add(stackMarcoParcial);

                Frame frameRecomendacion = new Frame { CornerRadius = 10, BackgroundColor = Color.FromHex("#E0DCD7"), Margin = new Thickness(0, 8, 0, 4), Padding = 0 };
                frameRecomendacion.Content = stackMarcoGlobal;
                frameRecomendacion.ClassId = item.id.ToString();

                StackRecomendacionesParcial.Children.Add(frameRecomendacion);
            }
            return true;
        }

        private async Task<bool> CargarProductos()
        {
            try
            {
                ListaProductos = await client.GetAll<Productos>("http://www.recommenditws.somee.com/api/ProductosApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarUsuarios()
        {
            try
            {
                ListaUsuarios = await client.GetAll<Usuarios>("http://www.recommenditws.somee.com/api/UsuariosApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarRecomendaciones()
        {
            try
            {
                ListaRecomendaciones = await client.GetAll<Recomendaciones>("http://www.recommenditws.somee.com/api/RecomendacionesApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarSolicitudRecomendaciones()
        {
            try
            {
                ListaSolicitudRecomendaciones = await client.GetAll<SolicitudRecomendaciones>("http://www.recommenditws.somee.com/api/SolicitudRecomendacionesApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarNecesidades()
        {
            try
            {
                ListaNecesidades = await client.GetAll<NecesidadesRec>("http://www.recommenditws.somee.com/api/NecesidadesRecApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> ActualizarMeGustaEnRecomendaciones(Recomendaciones val)
        {
            bool correcto = await client.Put<Recomendaciones>("http://www.recommenditws.somee.com/api/RecomendacionesApi", val);
            return correcto;
        }

        private async Task<bool> EliminarMeGusta(int IdvalMg)
        {
            bool correcto = await client.Delete<ValoracionesMeGusta>("http://www.recommenditws.somee.com/api/ValoracionesMeGustaApi/" + IdvalMg);
            return correcto;
        }

        private async Task<bool> AgregarMeGusta(ValoracionesMeGusta valMg)
        {
            bool correcto = await client.Post<ValoracionesMeGusta>("http://www.recommenditws.somee.com/api/ValoracionesMeGustaApi", valMg);
            return correcto;
        }

        private async Task<bool> ActualizarMeGusta(ValoracionesMeGusta valMg)
        {
            bool correcto = await client.Put<ValoracionesMeGusta>("http://www.recommenditws.somee.com/api/ValoracionesMeGustaApi", valMg);
            return correcto;
        }

        private async Task<bool> EliminarRecomendacion(int id)
        {
            try
            {
                bool result = await client.Delete<Recomendaciones>("http://www.recommenditws.somee.com/api/RecomendacionesApi/" + id);
                return result;
            }
            catch (Exception)
            {
                await DisplayAlert("Error", "No hemos podido borrar tu recomendación, intentelo más tarde.", "Aceptar");
                return false;
            }
        }

        private async Task<string> CargarFotoProductos(int id_prod)
        {
            try
            {
                ListaImagenes = await client.GetAll<ImagenesProductos>("http://www.recommenditws.somee.com/api/ImagenesProductosApi");
                ImagenesProductos imgProductoDb = ListaImagenes.Where(x => x.id_producto == id_prod).FirstOrDefault();
                var pathImgDescargada = imgProductoDb == null ? null : await App.DescargarImagenCloudinary(imgProductoDb.nombre_imagen);
                return imgProductoDb == null ? "product_default.png" : pathImgDescargada;
            }
            catch (Exception)
            {
                return "product_default.png";
            }
        }

        private async Task<string> CargarFotoUsuarioRecomendador(int id_usu)
        {
            try
            {
                ListaImagenesUsu = await client.GetAll<ImagenesUsuarios>("http://www.recommenditws.somee.com/api/ImagenesUsuariosApi");
                ImagenesUsuarios imgUsuarioDb = ListaImagenesUsu.Where(x => x.id_usuario == id_usu).FirstOrDefault();
                var pathImgDescargada = imgUsuarioDb == null ? null : await App.DescargarImagenCloudinary(imgUsuarioDb.nombre_imagen);
                return imgUsuarioDb == null ? "usuario_default.png" : pathImgDescargada;
            }
            catch (Exception)
            {
                return "usuario_default.png";
            }
        }

        private async Task<string> CargarFotoUsuarioCreador(int id_usu)
        {
            try
            {
                ListaImagenesUsu = await client.GetAll<ImagenesUsuarios>("http://www.recommenditws.somee.com/api/ImagenesUsuariosApi");
                ImagenesUsuarios imgUsuarioDb = ListaImagenesUsu.Where(x => x.id_usuario == id_usu).FirstOrDefault();
                var pathImgDescargada = imgUsuarioDb == null ? null : await App.DescargarImagenCloudinary(imgUsuarioDb.nombre_imagen);
                return imgUsuarioDb == null ? "usuario_default.png" : pathImgDescargada;
            }
            catch (Exception)
            {
                return "usuario_default.png";
            }
        }

        private async Task<bool> CargarValoracionesMeGusta()
        {
            try
            {
                ListaValoracionesMeGusta = await client.GetAll<ValoracionesMeGusta>("http://www.recommenditws.somee.com/api/ValoracionesMeGustaApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private string CalcularFechaPublicacion(DateTime? fecha_creacion)
        {
            string fecha_mostrada = "";
            DateTime fecha = DateTime.Now;
            if (fecha_creacion != null)
            {
                TimeSpan ts = (DateTime.Now - fecha_creacion).Value;
                fecha_mostrada = "Hace " + ts.Days.ToString() + " días";
                if (ts.Days < 2)
                {
                    if (ts.Days == 0)
                    {
                        fecha_mostrada = "Hoy";
                    }
                    else
                    {
                        fecha_mostrada = "Hace 1 día";
                    }
                }
            }
            return fecha_mostrada;
        }

        private void btnRecomendar_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new RecomendarProducto(_solicitudRecomendacion.id, _solicitudRecomendacion.titulo));
        }
    }
}