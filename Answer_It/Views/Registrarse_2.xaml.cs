﻿using Negocio;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.Permissions.Abstractions;
using Answer_It.Models;

namespace Answer_It.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Registrarse_2 : ContentPage
    {
        public string _email { get; set; }
        public string _nombre_usuario { get; set; }
        public string _contrasena { get; set; }
        RestClient client = new RestClient();

        public string NombreImagenSubida { get; set; }
        public MediaFile ImagenSubida { get; set; }

        public Registrarse_2(string email, string nombre_usuario, string contrasena)
        {
            InitializeComponent();
            _email = email;
            _nombre_usuario = nombre_usuario;
            _contrasena = contrasena;
        }

        private void lblIngresa_Tapped(object sender, EventArgs e)
        {
            App.MasterD.Detail = new NavigationPage(new IniciarSesion()) { BarTextColor = Color.FromHex("#FF8000") };
        }       

        private async void btnRegistrarse_Clicked(object sender, EventArgs e)
        {
            Cargando.IsVisible = true;
            if (!String.IsNullOrEmpty(editorNombre.Text) && !String.IsNullOrEmpty(editorApellido.Text)) //Si es posible el registro..
            {
                if (!inputNombre.HasError && !inputApellido.HasError)
                {
                    if (cbPoliticaPrivacidad.IsChecked)
                    {
                        if (cbTerminosCondiciones.IsChecked)
                        {
                            Usuarios usu = new Usuarios
                            {
                                id_rol = 1,
                                nombre_usuario = _nombre_usuario,
                                email = _email,
                                contrasena = _contrasena,
                                nombre = editorNombre.Text,
                                apellido = editorApellido.Text,
                                edad = null,
                                pais = null,
                                localidad = null,
                                fecha_registro = DateTime.Now,
                                eliminado = false,
                                es_verificado = false,
                                nivel = 1,
                                reputacion = 100,
                                r_points = 0,
                                visitas_al_perfil = 0,
                                votos_negativos = 0,
                                votos_positivos = 0
                            };
                            RestClient client = new RestClient();
                            int nuevoId = await client.PostAndGetId<Usuarios>("http://www.recommenditws.somee.com/api/UsuariosApi", usu);
                            if (nuevoId > 0)
                            {
                                if (NombreImagenSubida == String.Empty && ImagenSubida != null) //Si el user cargo una imagen
                                {
                                    string idImagen = await App.SubirImagenCloudinary(NombreImagenSubida, ImagenSubida.GetStream());
                                    bool result = await AgregarImagenUsuario(new ImagenesUsuarios { id_usuario = nuevoId, nombre_imagen = idImagen });
                                    if (!result)
                                    {
                                        await DisplayAlert("Ups..", "Se produjo un error al subir la foto. No te preocupes, ¡Podrás subirla luego desde tu perfil!", "¡De acuerdo!");
                                    }
                                }                              
                                await DisplayAlert("Registro correcto", "¡Bienvenido a Recommend It, " + usu.nombre + "! Ya puedes iniciar sesión para interactuar en la comunidad.", "Aceptar");
                                NombreImagenSubida = "";
                                ImagenSubida = null;
                                App.MasterD.Detail = new NavigationPage(new IniciarSesion()) { BarTextColor = Color.FromHex("#FF8000") };
                            }
                            else
                            {
                                await DisplayAlert("Error", "Se produjo un error al conectarse con el servidor. Inténtelo nuevamente más tarde", "Aceptar");
                            }
                        }
                        else
                        {
                            await DisplayAlert("Terminos y condiciones", "Es necesario aceptar los términos y condiciones para registrarte en Recommend It!", "Aceptar");
                        }
                    }
                    else
                    {
                        await DisplayAlert("Política de privacidad", "Es necesario aceptar la política de privacidad para registrarte en Recommend It!", "Aceptar");
                    }                  
                }
                else
                {
                    await DisplayAlert("Campos con errores", "¡Corrija los errores antes de continuar!", "Aceptar");
                }
            }
            else
            {
                await DisplayAlert("¡Campos incompletos!", "¡Complete todos los campos para continuar!", "Aceptar");
            }
            Cargando.IsVisible = false;
        }

        private async void btnSubirFoto_Clicked(object sender, EventArgs e)
        {
            try
            {
                await CrossMedia.Current.Initialize();
                if (!CrossMedia.Current.IsPickPhotoSupported)
                {
                    await DisplayAlert("No disponible", "Tu dispositivo no dispone de esta funcionalidad actualmente.", "Aceptar");
                    return;
                }
                Cargando.IsVisible = true;
                Cargando.IsBusy = true;
                var mediaOptions = new PickMediaOptions()
                {
                    //Para aplicar resize, maxQuality, etc.. sobre la img cargada.
                };

                var selectedImageFile = await CrossMedia.Current.PickPhotoAsync(mediaOptions);

                imgPerfil.Source = ImageSource.FromStream(() => selectedImageFile.GetStream());
                NombreImagenSubida = Path.GetFileName(selectedImageFile.Path);
                ImagenSubida = selectedImageFile;

                Cargando.IsVisible = false;
                Cargando.IsBusy = false;
            }
            catch (Exception)
            {
                Cargando.IsVisible = false;
                Cargando.IsBusy = false;
                await DisplayAlert("¡Permisos necesarios!", "Debes otorgarnos los permisos al almacenamiento de tu teléfono para que puedas subir una foto.", "Aceptar");
            }
        }

        private async Task<bool> AgregarImagenUsuario(ImagenesUsuarios img)
        {
            bool result = await client.Post<ImagenesUsuarios>("http://www.recommenditws.somee.com/api/ImagenesUsuariosApi", img);
            return result;
        }

        protected override bool OnBackButtonPressed()
        {
            App.MasterD.Detail = new NavigationPage(new Registrarse(_email,_nombre_usuario,_contrasena)) { BarTextColor = Color.FromHex("#FF8000") };
            return true;
        }

        private void editorNombre_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (editorNombre.Text.Length == 1)
            {
                inputNombre.HasError = true;
                inputNombre.ErrorText = "¡Muy pocos caracteres!";
            }
            else
            {
                if (editorNombre.Text.Length > 20)
                {
                    inputNombre.HasError = true;
                    inputNombre.ErrorText = "¡Muchos caracteres!";
                }
                else
                {
                    inputNombre.HasError = false;
                }
            }
        }

        private void editorApellido_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (editorApellido.Text.Length == 1)
            {
                inputApellido.HasError = true;
                inputApellido.ErrorText = "¡Muy pocos caracteres!";
            }
            else
            {
                if (editorApellido.Text.Length > 40)
                {
                    inputApellido.HasError = true;
                    inputApellido.ErrorText = "¡Muchos caracteres!";
                }
                else
                {
                    inputApellido.HasError = false;
                }
            }
        }

        private void cbPoliticaPrivacidad_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {

        }

        private void cbTerminosCondiciones_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {

        }
    }
}