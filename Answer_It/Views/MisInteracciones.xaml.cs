﻿using Answer_It.Models;
using Answer_It.ViewModels;
using Negocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Answer_It.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MisInteracciones : ContentPage
    {
        RestClient client = new RestClient();
        List<Usuarios> ListaUsuarios = new List<Usuarios>();
        List<Valoraciones> ListaValoraciones = new List<Valoraciones>();
        List<Productos> ListaProductos = new List<Productos>();
        List<Recomendaciones> ListaRecomendaciones = new List<Recomendaciones>();
        List<SolicitudRecomendaciones> ListaSolicitudRecomendaciones = new List<SolicitudRecomendaciones>();
        List<ValoracionesViewModel> ListaValoracionesVm = new List<ValoracionesViewModel>();
        List<RecomendacionesViewModel> ListaRecomendacionesVm = new List<RecomendacionesViewModel>();
        List<MisInteraccionesViewModel> ListaInteraccionesFinal = new List<MisInteraccionesViewModel>();

        public MisInteracciones()
        {
            InitializeComponent();
            CargandoInteracciones.IsBusy = true;
            CargandoInteracciones.IsVisible = true;
            CargarUsuarios();
            CargarListaValoraciones();
            CargarListaRecomendaciones();
            CargarProductos();
            CargarMisInteracciones();
            if (ListaInteraccionesFinal.Count == 0)
            {
                lblSinDatos.IsVisible = true;
            }
            CargandoInteracciones.IsBusy = false;
            CargandoInteracciones.IsVisible = false;
        }

        public void CargarMisInteracciones()
        {
            foreach (var val in ListaValoraciones)
            {
                ListaValoracionesVm.Add(new ValoracionesViewModel
                {
                    id = val.id,
                    id_producto = val.id_producto,
                    id_usuario = val.id_usuario,
                    nombre_usuario = ListaUsuarios.Where(x => x.id == val.id_usuario).First().nombre_usuario,
                    titulo = val.titulo,
                    contenido = val.contenido,
                    eliminado = val.eliminado,
                    fecha = val.fecha,
                    me_gusta = val.me_gusta,
                    puntaje = val.puntaje,
                    estado_cara = App.MeGustaEnIcono(val.me_gusta)
                });

                foreach (var item in ListaValoracionesVm)
                {
                    if (!val.eliminado)
                    {
                        item.estado_cara = App.MeGustaEnIcono(item.me_gusta);
                        ListaInteraccionesFinal.Add(new MisInteraccionesViewModel
                        {
                            id_val_rec = item.id_producto,
                            titulo_val_rec = "Valoración",
                            titulo_publicacion = ListaProductos.Where(x => x.id == item.id_producto).FirstOrDefault().nombre,
                            me_gusta = item.me_gusta,
                            estado_cara = item.estado_cara,
                            fecha_interaccion = item.fecha
                        });
                    }
                }                
            }

            foreach (var rec in ListaRecomendaciones)
            {
                ListaRecomendacionesVm.Add(new RecomendacionesViewModel
                {
                    id = rec.id,
                    id_solicitud_recomendacion = rec.id_solicitud_recomendacion,
                    id_usuario = rec.id_usuario,
                    contenido = rec.contenido,
                    eliminado = rec.eliminado,
                    fecha = rec.fecha,
                    me_gusta = rec.me_gusta,
                    estado_cara = App.MeGustaEnIcono(rec.me_gusta),
                    producto_recomendado = ListaProductos.Where(x => x.id == rec.id_producto_recomendado).First()
                });
                foreach (var item in ListaRecomendacionesVm)
                {
                    if (!item.eliminado)
                    {
                        item.estado_cara = App.MeGustaEnIcono(item.me_gusta);
                        ListaInteraccionesFinal.Add(new MisInteraccionesViewModel
                        {
                            id_val_rec = item.id_solicitud_recomendacion,
                            titulo_val_rec = "Recomendación",
                            titulo_publicacion = ListaSolicitudRecomendaciones.Where(x => x.id == item.id_solicitud_recomendacion).FirstOrDefault().titulo,
                            me_gusta = item.me_gusta,
                            estado_cara = item.estado_cara,
                            fecha_interaccion = item.fecha
                        });
                    }
                }              
            }

            ListaInteracciones.ItemsSource = null;
            ListaInteracciones.ItemsSource = ListaInteraccionesFinal;
        }

        private void ListaInteracciones_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var inter = e.Item as MisInteraccionesViewModel;
            if (inter != null)
            {
                if (inter.titulo_val_rec == "Valoración")
                {
                    Navigation.PushAsync(new ValoracionesProducto(inter.id_val_rec));
                }
                else
                {
                    if (inter.titulo_val_rec == "Recomendación")
                    {
                        Navigation.PushAsync(new RecomendacionesProducto(inter.id_val_rec));
                    }
                    else
                    {
                        DisplayAlert("Ups..", "Ha ocurrido un error al intentar ingresar a la publicación. Inténtelo más tarde", "Aceptar");
                    }
                }
            }
        }

        private async void CargarListaValoraciones()
        {
            ListaValoraciones = await client.GetAll<Valoraciones>("http://www.recommenditws.somee.com/api/ValoracionesApi");
            ListaValoraciones = ListaValoraciones.Where(x => x.id_usuario == App.Usuario.id).ToList();
        }

        private async void CargarListaRecomendaciones()
        {
            ListaRecomendaciones = await client.GetAll<Recomendaciones>("http://www.recommenditws.somee.com/api/RecomendacionesApi");
            ListaRecomendaciones = ListaRecomendaciones.Where(x => x.id_usuario == App.Usuario.id).ToList();
        }

        private async void CargarUsuarios()
        {
            ListaUsuarios = await client.GetAll<Usuarios>("http://www.recommenditws.somee.com/api/UsuariosApi");
        }

        private async void CargarProductos()
        {
            ListaProductos = await client.GetAll<Productos>("http://www.recommenditws.somee.com/api/ProductosApi");
        }
    }
}