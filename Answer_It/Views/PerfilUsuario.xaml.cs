﻿using Answer_It.MasterDetail;
using Answer_It.Models;
using Negocio;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Answer_It.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PerfilUsuario : ContentPage
    {
        RestClient client = new RestClient();
        List<NivelesBeneficios> ListaNiveles = new List<NivelesBeneficios>();
        List<Usuarios> ListaUsuarios = new List<Usuarios>();
        List<Valoraciones> ListaValoraciones = new List<Valoraciones>();
        List<Recomendaciones> ListaRecomendaciones = new List<Recomendaciones>();

        public PerfilUsuario(string nombre_usu) //CARGAR METODOS ASINCRONICAMENTE!!! Sino NO CARGA BIEN
        {
            InitializeComponent();
            CargarPerfil(nombre_usu);
        }

        private async Task<bool> CargarFoto(int id_usuario)
        {
            try
            {
                ImagenesUsuarios imgUsuario = await client.GetById<ImagenesUsuarios>("http://www.recommenditws.somee.com/api/ImagenesUsuariosApi/" + id_usuario);
                var pathImgDescargada = imgUsuario == null ? null : await App.DescargarImagenCloudinary(imgUsuario.nombre_imagen);
                imgPerfil.Source = imgUsuario == null ? "usuario_default.png" : pathImgDescargada;
                App.BorrarImagenLocalmente(pathImgDescargada);
                return true;
            }
            catch (Exception)
            {
                await DisplayAlert("Lo sentimos", "Hemos tenido un problema al cargar la foto de perfil.", "OK");
                return false;
            }
            
        }

        private async void CargarPerfil(string nombre_usuario)
        {
            Cargando.IsBusy = true;
            Cargando.IsVisible = true;
            await CargarNiveles();
            await CargarUsuario();
            await CargarUsuarios();
            await CargarListaValoraciones();
            await CargarListaRecomendaciones();
            if (App.Usuario.nombre_usuario == nombre_usuario)
            {
                this.Title = "Mi perfil";
                await CargarFoto(App.Usuario.id);
                frameVisitasPerfil.IsVisible = App.Usuario.nivel >= 8 ? true : false;
                stackEditarPerfil.IsVisible = true;
                lblNombre.Text = App.Usuario.nombre;
                lblApellido.Text = App.Usuario.apellido;
                lblNombreUsuario.Text = App.Usuario.nombre_usuario;
                lblNivel.Text = App.Usuario.nivel.ToString();
                lblRPoints.Text = App.Usuario.r_points.GetValueOrDefault(0).ToString();
                lblRanking.Text = App.ObtenerRankingUsuario(App.Usuario.id, ListaUsuarios).ToString();
                stackRPoints.IsVisible = true;
                ListaNiveles = await client.GetAll<NivelesBeneficios>("http://www.recommenditws.somee.com/api/NivelesBeneficiosApi");
                lblValoraciones.Text = ListaValoraciones.Where(x => x.id_usuario == App.Usuario.id).Count().ToString();
                lblRecomendaciones.Text = ListaRecomendaciones.Where(x => x.id_usuario == App.Usuario.id).Count().ToString();
                int reputacion_min = ListaNiveles.Find(x => x.nivel == App.Usuario.nivel).reputacion_necesaria;
                int reputacion_max = ListaNiveles.Find(x => x.nivel == App.Usuario.nivel + 1).reputacion_necesaria; //La rep. minima del nivel suguiente es la maxima del nivel actual del usuario.
                int reputacion_nivel = reputacion_max - reputacion_min;
                int reputacion_actual = App.Usuario.reputacion - reputacion_min;
                double porcentaje_nivel = reputacion_actual * 100 / reputacion_nivel;
                lblReputacion.Text = App.Usuario.reputacion.ToString() + " pts.  ";
                lblPorcentajeNivel.Text = porcentaje_nivel.ToString() + "%";
                pbReputacion.Progress = porcentaje_nivel;
                lblNivelActual.Text = App.Usuario.nivel.ToString();
                lblNivelProximo.Text = App.Usuario.nivel == 10 ? "MAX" : (App.Usuario.nivel + 1).ToString();
                lblVisitasAlPerfil.Text = App.Usuario.visitas_al_perfil.ToString();
                lblVotosPositivos.Text = App.Usuario.votos_positivos.ToString();
                lblVotosNegativos.Text = App.Usuario.votos_negativos.ToString();
                if (App.Usuario.es_verificado != null)
                {
                    if (App.Usuario.es_verificado == true)
                    {
                        lblUsuarioVerificado.IsVisible = true;
                    }
                    else
                    {
                        lblUsuarioVerificado.IsVisible = false;
                    }
                }
                else
                {
                    lblUsuarioVerificado.IsVisible = false;
                }
            }
            else
            {
                this.Title = "Perfil de " + nombre_usuario;
                stackEditarPerfil.IsVisible = false;
                Usuarios usuActual = ListaUsuarios.Where(x => x.nombre_usuario == nombre_usuario).FirstOrDefault();
                if (usuActual != null)
                {
                    await CargarFoto(usuActual.id);
                    frameVisitasPerfil.IsVisible = usuActual.nivel >= 8 ? true : false;
                    lblNombre.Text = usuActual.nombre;
                    lblApellido.Text = usuActual.apellido;
                    lblNombreUsuario.Text = usuActual.nombre_usuario;
                    lblNivel.Text = usuActual.nivel.ToString();
                    lblRPoints.Text = usuActual.r_points.ToString();
                    lblRanking.Text = App.ObtenerRankingUsuario(usuActual.id, ListaUsuarios).ToString();
                    stackRPoints.IsVisible = false;
                    ListaNiveles = await client.GetAll<NivelesBeneficios>("http://www.recommenditws.somee.com/api/NivelesBeneficiosApi");
                    lblValoraciones.Text = ListaValoraciones.Where(x => x.id_usuario == usuActual.id).Count().ToString();
                    lblRecomendaciones.Text = ListaRecomendaciones.Where(x => x.id_usuario == usuActual.id).Count().ToString();
                    int reputacion_min = ListaNiveles.Find(x => x.nivel == usuActual.nivel).reputacion_necesaria;
                    int reputacion_max = ListaNiveles.Find(x => x.nivel == usuActual.nivel + 1).reputacion_necesaria; //La rep. minima del nivel suguiente es la maxima del nivel actual del usuario.
                    int reputacion_nivel = reputacion_max - reputacion_min;
                    int reputacion_actual = usuActual.reputacion - reputacion_min;
                    double porcentaje_nivel = reputacion_actual * 100 / reputacion_nivel;
                    lblReputacion.Text = usuActual.reputacion.ToString() + " pts.  ";
                    lblPorcentajeNivel.Text = porcentaje_nivel.ToString() + "%";
                    pbReputacion.Progress = porcentaje_nivel;
                    lblNivelActual.Text = usuActual.nivel.ToString();
                    lblNivelProximo.Text = usuActual.nivel == 10 ? "MAX" : (usuActual.nivel + 1).ToString();
                    lblVisitasAlPerfil.Text = usuActual.visitas_al_perfil.ToString();
                    lblVotosPositivos.Text = usuActual.votos_positivos.ToString();
                    lblVotosNegativos.Text = usuActual.votos_negativos.ToString();
                    if (usuActual.es_verificado != null)
                    {
                        if (usuActual.es_verificado == true)
                        {
                            lblUsuarioVerificado.IsVisible = true;
                        }
                        else
                        {
                            lblUsuarioVerificado.IsVisible = false;
                        }
                    }
                    else
                    {
                        lblUsuarioVerificado.IsVisible = false;
                    }
                }
                else
                {
                    await DisplayAlert("Error", "No pudimos encontrar el usuario actual. Inténtelo más tarde.", "Aceptar");
                }
            }
            Cargando.IsVisible = false;
            Cargando.IsBusy = false;
        }

        protected override bool OnBackButtonPressed()
        {
            Navigation.PopAsync();
            return true;
        }

        private void FramePerfilCompleto_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new PerfilUsuarioCompleto());
        }

        private void TapGestureRecognizer_DetalleNivel(object sender, EventArgs e)
        {
            Navigation.PushAsync(new NivelesYBeneficios());
        }

        private void TapGestureRecognizer_MisInteracciones(object sender, EventArgs e)
        {
            Navigation.PushAsync(new MisInteracciones());
        }

        private async Task<bool> CargarUsuario()
        {
            try
            {
                Usuarios usu = await client.GetById<Usuarios>("http://www.recommenditws.somee.com/api/UsuariosApi" + App.Usuario.id);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarUsuarios()
        {
            try
            {
                ListaUsuarios = await client.GetAll<Usuarios>("http://www.recommenditws.somee.com/api/UsuariosApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarListaValoraciones()
        {
            try
            {
                ListaValoraciones = await client.GetAll<Valoraciones>("http://www.recommenditws.somee.com/api/ValoracionesApi");
                ListaValoraciones = ListaValoraciones.Where(x => x.id_usuario == App.Usuario.id).ToList();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarListaRecomendaciones()
        {
            try
            {
                ListaRecomendaciones = await client.GetAll<Recomendaciones>("http://www.recommenditws.somee.com/api/RecomendacionesApi");
                ListaRecomendaciones = ListaRecomendaciones.Where(x => x.id_usuario == App.Usuario.id).ToList();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarNiveles()
        {
            try
            {
                ListaNiveles = await client.GetAll<NivelesBeneficios>("http://www.recommenditws.somee.com/api/NivelesBeneficiosApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}