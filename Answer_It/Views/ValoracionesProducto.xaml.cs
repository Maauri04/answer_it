﻿using Answer_It.ViewModels;
using ImageCircle.Forms.Plugin.Abstractions;
using Negocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Syncfusion.SfRating.XForms;
using System.IO;
using Answer_It.Models;
using Answer_It.MasterDetail;

namespace Answer_It.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ValoracionesProducto : ContentPage
    {
        RestClient client = new RestClient();
        List<Valoraciones> ListaValoracionesProducto = new List<Valoraciones>(); //Informacion que se muestra en la listview.
        List<ValoracionesMeGusta> ListaValoracionesMeGusta = new List<ValoracionesMeGusta>(); //Informacion que se muestra en la listview.
        List<ImagenesUsuarios> ListaImagenesUsu = new List<ImagenesUsuarios>();
        List<ImagenesProductos> ListaImagenesProd = new List<ImagenesProductos>();
        List<Productos> ListaProductos = new List<Productos>();
        List<Usuarios> ListaUsuarios = new List<Usuarios>();
        Productos productoTarget = new Productos();
        public int id_producto { get; set; }

        public int _idSolVal { get; set; }
        public bool puedeActualizarMg { get; set; } = true;

        public ValoracionesProducto(int idProd)
        {
            InitializeComponent();
            id_producto = idProd;
            CargarPagina();
        }

        private async Task<bool> CargarPagina()
        {
            await CargarValoraciones(id_producto);
            await CargarValoracionesMeGusta();
            await CargarUsuarios();
            await CargarProducto(id_producto);
            await CargarFotoProducto(id_producto);
            lblProductoSinValorar.IsVisible = ListaValoracionesProducto.Count == 0 ? true : false;
            TituloProducto.Text = productoTarget.nombre;
            IdProducto.Text = productoTarget.id.ToString();
            double sumaValoraciones = ListaValoracionesProducto.Sum(x => x.puntaje);
            double ratingPromedio = (double)sumaValoraciones / ListaValoracionesProducto.Count();
            RatingProducto.Value = ratingPromedio; //Rating promedio entre todas sus valoraciones
            lblCantidadValoraciones.Text = ListaValoracionesProducto.Count() == 0 ? "(Aún sin valoraciones)" : "(Basado en " + ListaValoracionesProducto.Count() + " valoraciones)";
            lblPuntajePromedio.IsVisible = ListaValoracionesProducto.Count() == 0 ? false : true;
            lblPuntajePromedio.Text = (Math.Truncate(100 * ratingPromedio) / 100).ToString();

            if (!App.HaySesion())
            {
                btnValorar.IsEnabled = false;
                btnValorar.Text = "¡Inicia sesión para valorar!";
            }
            else
            {
                btnValorar.Text = "Valorar este producto";
                bool usuYaValoro = ListaValoracionesProducto.Exists(x => x.id_usuario == App.Usuario.id);
                if (usuYaValoro)
                {
                    LeyendaYaRecomendo.IsVisible = true;
                    btnValorar.IsEnabled = false;
                }
                else //Todo en orden! puede recomendar.
                {
                    LeyendaYaRecomendo.IsVisible = false;
                    btnValorar.IsEnabled = true;
                }
            }

            foreach (var item in ListaValoracionesProducto)
            {
                //Stack user
                Usuarios usuValorador = ListaUsuarios.Where(x => x.id == item.id_usuario).FirstOrDefault();
                CircleImage circleImage = new CircleImage { Source = ImageSource.FromFile(await CargarFotoUsuarioValorador(usuValorador.id)), HeightRequest = 28, Margin = new Thickness(4, 0, 0, 0) };
                Label lblUsuario = new Label { Text = usuValorador.nombre_usuario, Margin = new Thickness(4, 0, 0, 0), VerticalTextAlignment = TextAlignment.Center, FontAttributes = FontAttributes.Bold, FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)), TextColor = Color.FromHex("#FF8000") };
                //Image imgReputacion = new Image { Source = "reputacion_logo.png", HeightRequest = 30 };
                StackLayout stackUser = new StackLayout { BackgroundColor = Color.FromHex("#383838"), Orientation = StackOrientation.Horizontal, Padding = 6 };
                stackUser.Children.Add(circleImage); stackUser.Children.Add(lblUsuario); //stackUser.Children.Add(imgReputacion);
                Frame frameUser = new Frame { CornerRadius = 10, Padding = 0, Margin = new Thickness(0, 0, 0, 4), HorizontalOptions = LayoutOptions.Start };
                frameUser.Content = stackUser;
                Image imgBorrar = new Image { Source = "ic_borrar.png", HorizontalOptions = LayoutOptions.EndAndExpand, Margin = new Thickness(0, 0, 10, 0) };
                Image imgMeGusta = new Image { Source = "ic_megusta_off.png", HorizontalOptions = LayoutOptions.EndAndExpand, Margin = new Thickness(0, 0, 3, 0), IsVisible = false, HeightRequest = 30 };
                Image imgNoMeGusta = new Image { Source = "ic_nomegusta_off.png", HorizontalOptions = LayoutOptions.End, Margin = new Thickness(0, 0, 10, 0), IsVisible = false, HeightRequest = 30 };
                StackLayout stackUser2 = new StackLayout { Orientation = StackOrientation.Horizontal };
                stackUser2.Children.Add(frameUser);
                if (usuValorador.id == App.Usuario.id)
                {
                    var tapGestureRecognizerBorrar = new TapGestureRecognizer();
                    tapGestureRecognizerBorrar.Tapped += async (sender, e) =>
                    {
                        var result = await DisplayAlert("Borrar valoración", "¿Estás seguro que querés borrar esta valoración?", "Sí", "No");
                        if (result)
                        {
                            await EliminarValoracion(item.id);
                            View frameABorrar = StackValoracionesParcial.Children.Where(x => x.ClassId == item.id.ToString()).FirstOrDefault();
                            StackValoracionesParcial.Children.Remove(frameABorrar);
                            await CargarPagina();
                        }
                    };
                    imgBorrar.GestureRecognizers.Add(tapGestureRecognizerBorrar);
                    stackUser2.Children.Add(imgBorrar);
                }
                else //Esta valoracion NO fue hecha por el usuario actual.
                {
                    if (App.Usuario.nivel >= 2)
                    {
                        imgMeGusta.IsVisible = true;
                        imgNoMeGusta.IsVisible = true;
                        ValoracionesMeGusta valMeGusta = new ValoracionesMeGusta();
                        valMeGusta = ListaValoracionesMeGusta.Where(x => x.id_valoracion == item.id && x.id_usuario_like == App.Usuario.id).FirstOrDefault();
                        if (valMeGusta != null)
                        {
                            if (valMeGusta.me_gusta)
                            {
                                imgMeGusta.Source = "ic_megusta_on.png";
                                imgNoMeGusta.Source = "ic_nomegusta_off.png";
                            }
                            else
                            {
                                imgMeGusta.Source = "ic_megusta_off.png";
                                imgNoMeGusta.Source = "ic_nomegusta_on.png";
                            }
                        }
                    }

                    //OnTouch "me gusta"
                    var tapGestureRecognizerMeGusta = new TapGestureRecognizer();
                    tapGestureRecognizerMeGusta.Tapped += async (sender, e) =>
                    {
                        bool MeGusta = false;
                        bool NoMeGusta = false;
                        ValoracionesMeGusta valMeGusta = new ValoracionesMeGusta();
                        valMeGusta = ListaValoracionesMeGusta.Where(x => x.id_valoracion == item.id && x.id_usuario_like == App.Usuario.id).FirstOrDefault();
                        if (valMeGusta != null) //Ya valoró este comentario 
                        {
                            if (valMeGusta.me_gusta) //Si ya tenia me gusta..
                            {
                                imgMeGusta.Source = "ic_megusta_off.png";
                                imgNoMeGusta.Source = "ic_nomegusta_off.png";
                                if (puedeActualizarMg) //Evito que se pisen actualizaciones en los likes simultaneas
                                {
                                    puedeActualizarMg = false;
                                    item.me_gusta -= 1;
                                    bool actualizacionCorrecta = await ActualizarMeGustaEnValoraciones(item); //Actualizo contador de likes en Valoraciones
                                    if (actualizacionCorrecta)
                                    {
                                        bool eliminadoCorrecto = await EliminarMeGusta(valMeGusta.id);
                                        if (eliminadoCorrecto)
                                        {
                                            await CargarValoraciones(id_producto);
                                            puedeActualizarMg = true;
                                        }
                                        else
                                        {
                                            DisplayAlert("Error", "Error del servidor, intentelo más tarde.", "Aceptar");
                                        }
                                    }
                                    else
                                    {
                                        DisplayAlert("Error", "Error del servidor, intentelo más tarde.", "Aceptar");
                                    }
                                }
                            }
                            else
                            {
                                if (valMeGusta.no_me_gusta) //Si ya tenia un no me gusta..
                                {
                                    imgNoMeGusta.Source = "ic_nomegusta_off.png";
                                    imgMeGusta.Source = "ic_megusta_on.png";
                                    MeGusta = true;
                                    NoMeGusta = false;
                                    if (puedeActualizarMg)
                                    {
                                        puedeActualizarMg = false;
                                        item.me_gusta += 1;
                                        bool actualizacionCorrecta = await ActualizarMeGustaEnValoraciones(item);
                                        if (actualizacionCorrecta)
                                        {
                                            valMeGusta.me_gusta = MeGusta;
                                            valMeGusta.no_me_gusta = NoMeGusta;
                                            bool actCorrecta = await ActualizarMeGusta(valMeGusta);
                                            if (actCorrecta)
                                            {
                                                await CargarValoraciones(id_producto);
                                                puedeActualizarMg = true;
                                            }
                                            else
                                            {
                                                DisplayAlert("Error", "Error del servidor, intentelo más tarde.", "Aceptar");
                                            }
                                        }
                                        else
                                        {
                                            DisplayAlert("Error", "Error del servidor, intentelo más tarde.", "Aceptar");
                                        }
                                    }
                                }
                            }
                        }
                        else //No valoró este comentario
                        {
                            imgMeGusta.Source = "ic_megusta_on.png";
                            imgNoMeGusta.Source = "ic_nomegusta_off.png";
                            MeGusta = true;
                            NoMeGusta = false;
                            if (puedeActualizarMg)
                            {
                                puedeActualizarMg = false;
                                item.me_gusta += 1;
                                bool actualizacionCorrecta = await ActualizarMeGustaEnValoraciones(item);
                                if (actualizacionCorrecta)
                                {
                                    ValoracionesMeGusta nuevaValMg = new ValoracionesMeGusta
                                    {
                                        id_usuario_like = App.Usuario.id,
                                        id_valoracion = item.id,
                                        me_gusta = MeGusta,
                                        no_me_gusta = NoMeGusta
                                    };
                                    bool actCorrecta = await AgregarMeGusta(nuevaValMg);
                                    if (actCorrecta)
                                    {
                                        await CargarValoraciones(id_producto);
                                        puedeActualizarMg = true;
                                    }
                                    else
                                    {
                                        DisplayAlert("Error", "Error del servidor, intentelo más tarde.", "Aceptar");
                                    }
                                }
                                else
                                {
                                    DisplayAlert("Error", "Error del servidor, intentelo más tarde.", "Aceptar");
                                }
                            }
                        }
                    };
                    imgMeGusta.GestureRecognizers.Add(tapGestureRecognizerMeGusta);

                    //OnTouch "no me gusta"
                    var tapGestureRecognizerNoMeGusta = new TapGestureRecognizer();
                    tapGestureRecognizerNoMeGusta.Tapped += async (sender, e) =>
                    {
                        bool MeGusta = false;
                        bool NoMeGusta = false;
                        ValoracionesMeGusta valMeGusta = new ValoracionesMeGusta();
                        valMeGusta = ListaValoracionesMeGusta.Where(x => x.id_valoracion == item.id && x.id_usuario_like == App.Usuario.id).FirstOrDefault();
                        if (valMeGusta != null)
                        {
                            if (valMeGusta.no_me_gusta) //Si ya tenia no me gusta..
                            {
                                imgNoMeGusta.Source = "ic_nomegusta_off.png";
                                imgMeGusta.Source = "ic_megusta_off.png";
                                if (puedeActualizarMg) //Evito que se pisen actualizaciones en los likes simultaneas
                                {
                                    puedeActualizarMg = false;
                                    item.me_gusta += 1;
                                    bool actualizacionCorrecta = await ActualizarMeGustaEnValoraciones(item); //Actualizo contador de likes en Valoraciones
                                    if (actualizacionCorrecta)
                                    {
                                        bool eliminarcionCorrecta = await EliminarMeGusta(valMeGusta.id);
                                        if (eliminarcionCorrecta)
                                        {
                                            await CargarValoraciones(id_producto);
                                            puedeActualizarMg = true;
                                        }
                                        else
                                        {
                                            DisplayAlert("Error", "Error del servidor, intentelo más tarde.", "Aceptar");
                                        }
                                    }
                                    else
                                    {
                                        DisplayAlert("Error", "Error del servidor, intentelo más tarde.", "Aceptar");
                                    }
                                }
                            }
                            else
                            {
                                if (valMeGusta.me_gusta) //Si ya tenia un me gusta..
                                {
                                    imgMeGusta.Source = "ic_megusta_off.png";
                                    imgNoMeGusta.Source = "ic_nomegusta_on.png";
                                    MeGusta = false;
                                    NoMeGusta = true;
                                    if (puedeActualizarMg) //Evito que se pisen actualizaciones en los likes simultaneas
                                    {
                                        puedeActualizarMg = false;
                                        item.me_gusta -= 1;
                                        bool actualizacionCorrecta = await ActualizarMeGustaEnValoraciones(item); //Actualizo contador de likes en Valoraciones
                                        if (actualizacionCorrecta)
                                        {
                                            valMeGusta.me_gusta = MeGusta;
                                            valMeGusta.no_me_gusta = NoMeGusta;
                                            bool actCorrecta = await ActualizarMeGusta(valMeGusta);
                                            if (actCorrecta)
                                            {
                                                await CargarValoraciones(id_producto);
                                                puedeActualizarMg = true;
                                            }
                                            else
                                            {
                                                DisplayAlert("Error", "Error del servidor, intentelo más tarde.", "Aceptar");
                                            }
                                        }
                                        else
                                        {
                                            DisplayAlert("Error", "Error del servidor, intentelo más tarde.", "Aceptar");
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            imgNoMeGusta.Source = "ic_nomegusta_on.png";
                            imgMeGusta.Source = "ic_megusta_off.png";
                            NoMeGusta = true;
                            MeGusta = false;
                            if (puedeActualizarMg) //Evito que se pisen actualizaciones en los likes simultaneas
                            {
                                puedeActualizarMg = false;
                                item.me_gusta -= 1;
                                bool actualizacionCorrecta = await ActualizarMeGustaEnValoraciones(item); //Actualizo contador de likes en Valoraciones
                                if (actualizacionCorrecta)
                                {
                                    ValoracionesMeGusta nuevaValMg = new ValoracionesMeGusta
                                    {
                                        id_usuario_like = App.Usuario.id,
                                        id_valoracion = item.id,
                                        me_gusta = MeGusta,
                                        no_me_gusta = NoMeGusta
                                    };
                                    bool agregadoCorrecto = await AgregarMeGusta(nuevaValMg);
                                    if (agregadoCorrecto)
                                    {
                                        await CargarValoraciones(id_producto);
                                        puedeActualizarMg = true;
                                    }
                                    else
                                    {
                                        DisplayAlert("Error", "Error del servidor, intentelo más tarde.", "Aceptar");
                                    }
                                }
                                else
                                {
                                    DisplayAlert("Error", "Error del servidor, intentelo más tarde.", "Aceptar");
                                }
                            }
                        }
                    };
                    imgNoMeGusta.GestureRecognizers.Add(tapGestureRecognizerNoMeGusta);

                    stackUser2.Children.Add(imgMeGusta);
                    stackUser2.Children.Add(imgNoMeGusta);
                }

                SfRating ratingUsuario = new SfRating { Value = item.puntaje, ReadOnly = true, ItemSize = 15, HorizontalOptions = LayoutOptions.Start, RatingSettings = new SfRatingSettings { RatedFill = Color.FromHex("#FF8000"), RatedStrokeWidth = 0 } };
                Label lblReportar = new Label { Text = "¡Reportar!", IsVisible = false, FontAttributes = FontAttributes.Italic, HorizontalOptions = LayoutOptions.EndAndExpand, HorizontalTextAlignment = TextAlignment.End, TextDecorations = TextDecorations.Underline, FontSize = 12, TextColor = Color.Red };
                Label lblTitulo = new Label { Text = item.titulo, FontAttributes = FontAttributes.Bold, TextColor = Color.Black };
                if (usuValorador.id != App.Usuario.id)
                {
                    lblReportar.IsVisible = true;
                    var tapGestureRecognizerReportar = new TapGestureRecognizer();
                    tapGestureRecognizerReportar.Tapped += (sender, e) =>
                    {
                        Navigation.PushAsync(new Denunciar(item.id, "valoracion"));
                    };
                    lblReportar.GestureRecognizers.Add(tapGestureRecognizerReportar);
                }
                StackLayout stackValoracionYDenuncia = new StackLayout { Padding = 0, Spacing = 0, Orientation = StackOrientation.Horizontal };
                stackValoracionYDenuncia.Children.Add(ratingUsuario);
                stackValoracionYDenuncia.Children.Add(lblReportar);
                Label lblContenido = new Label { Text = item.contenido, Margin = new Thickness(0, 0, 0, 4) };

                //Stack footer valoracion
                Image imgEstadoCara = new Image { Source = App.MeGustaEnIcono(item.me_gusta) };
                Label lblMeGusta = new Label { Text = item.me_gusta.ToString(), TextColor = Color.Black, FontAttributes = FontAttributes.Bold };
                StackLayout stackMeGusta = new StackLayout { Orientation = StackOrientation.Horizontal, HorizontalOptions = LayoutOptions.Start };
                stackMeGusta.Children.Add(imgEstadoCara); stackMeGusta.Children.Add(lblMeGusta);
                Label lblFechaPublicacion = new Label { Text = CalcularFechaPublicacion(item.fecha), FontSize = Device.GetNamedSize(NamedSize.Caption, typeof(Label)), HorizontalOptions = LayoutOptions.EndAndExpand };
                StackLayout stackFooter = new StackLayout { Orientation = StackOrientation.Horizontal, Padding = new Thickness(14, 0, 14, 14) };
                stackFooter.Children.Add(stackMeGusta); stackFooter.Children.Add(lblFechaPublicacion);

                StackLayout stackComentarioParcial = new StackLayout { Padding = new Thickness(14, 0, 14, 14) };
                stackComentarioParcial.Children.Add(stackValoracionYDenuncia); stackComentarioParcial.Children.Add(lblTitulo); stackComentarioParcial.Children.Add(lblContenido);
                stackComentarioParcial.Children.Add(stackFooter);

                StackLayout stackGeneral = new StackLayout();
                stackGeneral.Children.Add(stackUser2); stackGeneral.Children.Add(stackComentarioParcial); stackGeneral.Children.Add(stackFooter);

                Frame frameGeneral = new Frame { BackgroundColor = Color.FromHex("#E0DCD7"), CornerRadius = 10, Margin = new Thickness(0, 8, 0, 4), Padding = 0 };
                frameGeneral.Content = stackGeneral;
                frameGeneral.ClassId = item.id.ToString(); //Le asigno un id para eliminar el frame luego.

                StackValoracionesParcial.Children.Add(frameGeneral);
            }
            return true;
        }

        private string CalcularFechaPublicacion(DateTime? fecha_creacion)
        {
            string fecha_mostrada = "";
            DateTime fecha = DateTime.Now;
            if (fecha_creacion != null)
            {
                TimeSpan ts = (DateTime.Now - fecha_creacion).Value;
                fecha_mostrada = "Hace " + ts.Days.ToString() + " días";
                if (ts.Days < 2)
                {
                    if (ts.Days == 0)
                    {
                        fecha_mostrada = "Hoy";
                    }
                    else
                    {
                        fecha_mostrada = "Hace 1 día";
                    }
                }
            }
            return fecha_mostrada;
        }

        private async Task<bool> CargarFotoProducto(int id_producto)
        {
            try
            {
                ListaImagenesProd = await client.GetAll<ImagenesProductos>("http://www.recommenditws.somee.com/api/ImagenesProductosApi");
                ImagenesProductos imgProd = ListaImagenesProd.Where(x => x.id_producto == id_producto).FirstOrDefault();
                var pathImgDescargada = imgProd == null ? null : await App.DescargarImagenCloudinary(imgProd.nombre_imagen);
                imgProducto.Source = imgProd == null ? "product_default.png" : pathImgDescargada;
                App.BorrarImagenLocalmente(pathImgDescargada);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        private async Task<bool> CargarValoraciones(int id_producto)
        {
            try
            {
                ListaValoracionesProducto = await client.GetAll<Valoraciones>("http://www.recommenditws.somee.com/api/ValoracionesApi");
                ListaValoracionesProducto = ListaValoracionesProducto.Where(x => x.id_producto == id_producto).ToList();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarValoracionesMeGusta()
        {
            try
            {
                ListaValoracionesMeGusta = await client.GetAll<ValoracionesMeGusta>("http://www.recommenditws.somee.com/api/ValoracionesMeGustaApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarProducto(int id_prod)
        {
            try
            {
                ListaProductos = await client.GetAll<Productos>("http://www.recommenditws.somee.com/api/ProductosApi");
                productoTarget = ListaProductos.Where(x => x.id == id_prod).FirstOrDefault();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarUsuarios()
        {
            try
            {
                ListaUsuarios = await client.GetAll<Usuarios>("http://www.recommenditws.somee.com/api/UsuariosApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> EliminarValoracion(int id)
        {
            try
            {
                bool result = await client.Delete<Valoraciones>("http://www.recommenditws.somee.com/api/ValoracionesApi/" + id);
                return result;
            }
            catch (Exception)
            {
                await DisplayAlert("Error", "No hemos podido borrar tu valoración, intentelo más tarde.", "Aceptar");
                return false;
            }
        }


        private async Task<string> CargarFotoUsuarioValorador(int id_usu)
        {
            ListaImagenesUsu = await client.GetAll<ImagenesUsuarios>("http://www.recommenditws.somee.com/api/ImagenesUsuariosApi");
            ImagenesUsuarios imgUsuarioDb = ListaImagenesUsu.Where(x => x.id_usuario == id_usu).FirstOrDefault();
            var pathImgDescargada = imgUsuarioDb == null ? null : await App.DescargarImagenCloudinary(imgUsuarioDb.nombre_imagen);
            return imgUsuarioDb == null ? "usuario_default.png" : pathImgDescargada;
        }

        private async Task<bool> ActualizarMeGustaEnValoraciones(Valoraciones val)
        {
            bool correcto = await client.Put<Valoraciones>("http://www.recommenditws.somee.com/api/ValoracionesApi", val);
            return correcto;
        }

        private async Task<bool> EliminarMeGusta(int IdvalMg)
        {
            bool correcto = await client.Delete<ValoracionesMeGusta>("http://www.recommenditws.somee.com/api/ValoracionesMeGustaApi/" + IdvalMg);
            return correcto;
        }

        private async Task<bool> AgregarMeGusta(ValoracionesMeGusta valMg)
        {
            bool correcto = await client.Post<ValoracionesMeGusta>("http://www.recommenditws.somee.com/api/ValoracionesMeGustaApi", valMg);
            return correcto;
        }

        private async Task<bool> ActualizarMeGusta(ValoracionesMeGusta valMg)
        {
            bool correcto = await client.Put<ValoracionesMeGusta>("http://www.recommenditws.somee.com/api/ValoracionesMeGustaApi", valMg);
            return correcto;
        }

        protected override bool OnBackButtonPressed()
        {
            App.MasterD.Detail = new NavigationPage(new Inicio()) { BarTextColor = Color.FromHex("#FF8000") };
            return true;
        }

        private void btnValorar_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ValorarProducto(id_producto));
        }
    }
}