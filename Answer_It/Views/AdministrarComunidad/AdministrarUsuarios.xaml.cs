﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Answer_It.Models;
using Answer_It.ViewModels;
using Negocio;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Answer_It.Views.AdministrarComunidad
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AdministrarUsuarios : ContentPage
    {
        public List<AdministrarUsuariosViewModel> ListaUsuFinal = new List<AdministrarUsuariosViewModel>();
        public List<Usuarios> ListaUsuarios = new List<Usuarios>();
        public RestClient client = new RestClient();

        public AdministrarUsuarios()
        {
            InitializeComponent();
            CargarPagina();
        }

        private async Task<bool> CargarPagina()
        {
            CargandoUsu.IsBusy = true;
            CargandoUsu.IsVisible = true;
            ListaUsuFinal.Clear();
            ListaUsu.ItemsSource = null;
            await CargarUsuarios();
            await CargarListadoUsuAsync();
            if (ListaUsuFinal.Count == 0)
            {
                lblUsuPendientes.IsVisible = true;
            }
            CargandoUsu.IsBusy = false;
            CargandoUsu.IsVisible = false;
            return true;
        }

        public async Task<bool> CargarListadoUsuAsync()
        {
            foreach (var item in ListaUsuarios)
            {
                AdministrarUsuariosViewModel adminUsu = new AdministrarUsuariosViewModel
                {
                    id_usu = item.id,
                    es_verificado = item.es_verificado.GetValueOrDefault(false),
                    nombre_usuario = item.nombre_usuario,
                    ranking_usuario = App.ObtenerRankingUsuario(item.id, ListaUsuarios),
                    votos_totales = item.votos_positivos.GetValueOrDefault(0) - item.votos_negativos.GetValueOrDefault(0),
                    permite_borrar = await ObtenerRol(item.id_rol) == "Administrador" ? false : true
                };
                ListaUsuFinal.Add(adminUsu);
            }
            ListaUsuFinal = ListaUsuFinal.OrderBy(x => x.ranking_usuario).ToList();
            ListaUsu.ItemsSource = null;
            ListaUsu.ItemsSource = ListaUsuFinal;
            return true;
        }

        private async void OnBorrarUsuario_Tapped(object sender, EventArgs e)
        {
            var result = await DisplayAlert("Borrar usuario", "¿Estás seguro que querés borrar este usuario?", "Sí", "No");
            if (result)
            {
                var id_usu = ((TappedEventArgs)e).Parameter;
                bool resultEliminar = await EliminarUsuario(Convert.ToInt32(id_usu));
                if (resultEliminar)
                {
                    await DisplayAlert("Usuario borrado", "El usuario fue borrado exitosamente.", "Aceptar");
                    await CargarPagina();
                }
                else
                {
                    await DisplayAlert("Error", "No hemos podido borrar el usuario, intentelo más tarde.", "Aceptar");
                }
            }
        }

        private async Task<bool> CargarUsuarios()
        {
            try
            {
                ListaUsuarios = await client.GetAll<Usuarios>("http://www.recommenditws.somee.com/api/UsuariosApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<string> ObtenerRol(int id_rol)
        {
            try
            {
                Roles rol = await client.GetById<Roles>("http://www.recommenditws.somee.com/api/RolesApi/" + id_rol);
                return rol.nombre;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private async Task<bool> EliminarUsuario(int id)
        {
            try
            {
                bool result = await client.Delete<Usuarios>("http://www.recommenditws.somee.com/api/UsuariosApi/" + id);
                return result;
            }
            catch (Exception)
            {
                await DisplayAlert("Error", "No hemos podido borrar el usuario especificado, inténtelo más tarde.", "Aceptar");
                return false;
            }
        }
    }
}