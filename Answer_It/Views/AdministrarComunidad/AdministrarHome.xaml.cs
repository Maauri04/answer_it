﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Answer_It.Views.AdministrarComunidad
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AdministrarHome : ContentPage
    {
        public AdministrarHome()
        {
            InitializeComponent();
        }

        private void btnDenuncias_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AdministrarDenuncias());
        }

        private void btnSolicitudesValoracion_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AdministrarSolVal());
        }

        private void btnSolicitudesRecomendacion_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AdministrarSolRec());
        }

        private void btnValoraciones_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AdministrarValoraciones());
        }

        private void btnUsuarios_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AdministrarUsuarios());
        }
    }
}