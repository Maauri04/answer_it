﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Answer_It.Models;
using Negocio;
using Answer_It.ViewModels;

namespace Answer_It.Views.AdministrarComunidad
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AdministrarDenuncias : ContentPage
    {
        public RestClient client = new RestClient();
        public List<DenunciasValoraciones> ListaDenunciasVal = new List<DenunciasValoraciones>();
        public List<DenunciasRecomendaciones> ListaDenunciasRec = new List<DenunciasRecomendaciones>();
        public List<Usuarios> ListaUsuarios = new List<Usuarios>();
        public List<AdministrarDenunciasViewModel> ListaDenunciasFinal = new List<AdministrarDenunciasViewModel>();

        public AdministrarDenuncias()
        {
            InitializeComponent();
            CargarPagina();
        }

        private async Task<bool> CargarPagina()
        {
            CargandoDenuncias.IsBusy = true;
            CargandoDenuncias.IsVisible = true;
            ListaDenunciasFinal.Clear();
            ListaDenunciasRec.Clear();
            ListaDenunciasVal.Clear();
            ListaDenuncias.ItemsSource = null;
            await CargarDenunciasValoraciones();
            await CargarDenunciasRecomendaciones();
            await CargarUsuarios();
            await CargarListadoDenunciasAsync();
            if (ListaDenunciasVal.Count == 0 && ListaDenunciasRec.Count == 0)
            {
                lblDenunciasPendientes.IsVisible = true;
            }
            CargandoDenuncias.IsBusy = false;
            CargandoDenuncias.IsVisible = false;
            return true;
        }

        private async Task CargarListadoDenunciasAsync()
        {
            foreach (var item in ListaDenunciasVal)
            {
                AdministrarDenunciasViewModel adminDen = new AdministrarDenunciasViewModel
                {
                    id_val_rec = item.id_valoracion,
                    id_denuncia = item.id,
                    comentario_denunciado = "\"" + await ObtenerComentarioValoracion(item.id_valoracion) + "\"",
                    motivo_denuncia = item.motivo,
                    texto_valoracion_recomendacion = "Valoración",
                    nombre_usuario_denunciado = await ObtenerNombreUsuario(item.id_usuario_denunciado),
                    nombre_usuario_denunciante = await ObtenerNombreUsuario(item.id_usuario_denunciante),
                    fecha_denuncia = item.fecha_denuncia.GetValueOrDefault()
                };
                ListaDenunciasFinal.Add(adminDen);
            }

            foreach (var item in ListaDenunciasRec)
            {
                AdministrarDenunciasViewModel adminDen = new AdministrarDenunciasViewModel
                {
                    id_val_rec = item.id_recomendacion,
                    id_denuncia = item.id,
                    comentario_denunciado = "\"" + await ObtenerComentarioRecomendacion(item.id_recomendacion) + "\"",
                    motivo_denuncia = item.motivo,
                    texto_valoracion_recomendacion = "Recomendación",
                    nombre_usuario_denunciado = await ObtenerNombreUsuario(item.id_usuario_denunciado),
                    nombre_usuario_denunciante = await ObtenerNombreUsuario(item.id_usuario_denunciante),
                    fecha_denuncia = item.fecha_denuncia.GetValueOrDefault()
                };
                ListaDenunciasFinal.Add(adminDen);
            }

            ListaDenunciasFinal = ListaDenunciasFinal.OrderByDescending(x => x.fecha_denuncia).ToList();
            ListaDenuncias.ItemsSource = null;
            ListaDenuncias.ItemsSource = ListaDenunciasFinal;
        }

        private async Task<bool> CargarDenunciasValoraciones()
        {
            try
            {
                ListaDenunciasVal = await client.GetAll<DenunciasValoraciones>("http://www.recommenditws.somee.com/api/DenunciasValoracionesApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarDenunciasRecomendaciones()
        {
            try
            {
                ListaDenunciasRec = await client.GetAll<DenunciasRecomendaciones>("http://www.recommenditws.somee.com/api/DenunciasRecomendacionesApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarUsuarios()
        {
            try
            {
                ListaUsuarios = await client.GetAll<Usuarios>("http://www.recommenditws.somee.com/api/UsuariosApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<string> ObtenerComentarioValoracion(int id_valoracion)
        {
            try
            {
                Valoraciones val = await client.GetById<Valoraciones>("http://www.recommenditws.somee.com/api/ValoracionesApi/" + id_valoracion);
                return val.contenido;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private async Task<string> ObtenerComentarioRecomendacion(int id_recomendacion)
        {
            try
            {
                Recomendaciones rec = await client.GetById<Recomendaciones>("http://www.recommenditws.somee.com/api/RecomendacionesApi/" + id_recomendacion);
                return rec.contenido;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private async Task<string> ObtenerNombreUsuario(int id_usuario)
        {
            try
            {
                Usuarios usu = await client.GetById<Usuarios>("http://www.recommenditws.somee.com/api/UsuariosApi/" + id_usuario);
                return usu.nombre_usuario;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private async Task<bool> EliminarValoracion(int id)
        {
            try
            {
                bool result = await client.Delete<Valoraciones>("http://www.recommenditws.somee.com/api/ValoracionesApi/" + id);
                return result;
            }
            catch (Exception)
            {
                await DisplayAlert("Error", "No hemos podido borrar la valoración, intentelo más tarde.", "Aceptar");
                return false;
            }
        }

        private async Task<bool> EliminarRecomendacion(int id)
        {
            try
            {
                bool result = await client.Delete<Recomendaciones>("http://www.recommenditws.somee.com/api/RecomendacionesApi/" + id);
                return result;
            }
            catch (Exception)
            {
                await DisplayAlert("Error", "No hemos podido borrar la recomendación, intentelo más tarde.", "Aceptar");
                return false;
            }
        }

        private async Task<bool> EliminarDenunciaValoracion(int id)
        {
            try
            {
                bool result = await client.Delete<DenunciasValoraciones>("http://www.recommenditws.somee.com/api/DenunciasValoracionesApi/" + id);
                return result;
            }
            catch (Exception)
            {
                await DisplayAlert("Error", "No hemos podido borrar tu denuncia, intentelo más tarde.", "Aceptar");
                return false;
            }
        }

        private async Task<bool> EliminarDenunciaRecomendacion(int id)
        {
            try
            {
                bool result = await client.Delete<DenunciasRecomendaciones>("http://www.recommenditws.somee.com/api/DenunciasRecomendacionesApi/" + id);
                return result;
            }
            catch (Exception)
            {
                await DisplayAlert("Error", "No hemos podido borrar tu denuncia, intentelo más tarde.", "Aceptar");
                return false;
            }
        }

        private async void OnBorrarDenuncia_Tapped(object sender, EventArgs e)
        {
            string valORec = "";
            StackLayout StackBorrarComentario = sender as StackLayout;
            Frame FrameDenuncia = StackBorrarComentario.Parent.Parent.Parent as Frame;
            StackLayout StackWrapperDenuncia = FrameDenuncia.Children.Where(x => x.ClassId == "StackWrapperDenuncia").FirstOrDefault() as StackLayout;
            StackLayout StackTituloDenuncia = StackWrapperDenuncia.Children.Where(x => x.ClassId == "StackTituloDenuncia").FirstOrDefault() as StackLayout;
            Label lblTituloDenuncia = StackTituloDenuncia.Children.Where(x => x.ClassId == "lblTituloDenuncia").FirstOrDefault() as Label;
            switch (lblTituloDenuncia.Text)
            {
                case "Valoración":
                    valORec = "valoración";
                    break;
                case "Recomendación":
                    valORec = "recomendación";
                    break;
                default:
                    break;
            }

            string tituloAlert = "Borrar denuncia de " + valORec;
            var result = await DisplayAlert(tituloAlert, "¿Estás seguro que querés borrar esta denuncia de " + valORec + "?", "Sí", "No");
            if (result)
            {
                switch (valORec)
                {
                    case "valoración":
                        var id_den_val = ((TappedEventArgs)e).Parameter;
                        bool resultEliminarVal = await EliminarDenunciaValoracion(Convert.ToInt32(id_den_val));
                        if (resultEliminarVal)
                        {
                            StackPrincipal.Children.Remove(FrameDenuncia);
                            await DisplayAlert("Denuncia borrada", "La denuncia de valoración fue borrada exitosamente.", "Aceptar");
                            await CargarPagina();
                        }
                        else
                        {
                            await DisplayAlert("Error", "No hemos podido borrar la denuncia, intentelo más tarde.", "Aceptar");
                        }
                        break;
                    case "recomendación":
                        var id_den_rec = ((TappedEventArgs)e).Parameter;
                        bool resultEliminarRec = await EliminarDenunciaRecomendacion(Convert.ToInt32(id_den_rec));
                        if (resultEliminarRec)
                        {
                            StackPrincipal.Children.Remove(FrameDenuncia);
                            await DisplayAlert("Denuncia borrada", "La denuncia de recomendación fue borrada exitosamente.", "Aceptar");
                            await CargarPagina();
                        }
                        else
                        {
                            await DisplayAlert("Error", "No hemos podido borrar la denuncia, intentelo más tarde.", "Aceptar");
                        }
                        break;
                    default:
                        await DisplayAlert("Error", "No hemos podido borrar tu denuncia, intentelo más tarde.", "Aceptar");
                        break;
                }
            }
        }

        private async void OnBorrarComentario_Tapped(object sender, EventArgs e)
        {
            string valORec = "";
            StackLayout StackBorrarComentario = sender as StackLayout;
            Frame FrameDenuncia = StackBorrarComentario.Parent.Parent.Parent as Frame;
            StackLayout StackWrapperDenuncia = FrameDenuncia.Children.Where(x => x.ClassId == "StackWrapperDenuncia").FirstOrDefault() as StackLayout;
            StackLayout StackTituloDenuncia = StackWrapperDenuncia.Children.Where(x => x.ClassId == "StackTituloDenuncia").FirstOrDefault() as StackLayout;
            Label lblTituloDenuncia = StackTituloDenuncia.Children.Where(x => x.ClassId == "lblTituloDenuncia").FirstOrDefault() as Label;
            switch (lblTituloDenuncia.Text)
            {
                case "Valoración":
                    valORec = "valoración";
                    break;
                case "Recomendación":
                    valORec = "recomendación";
                    break;
                default:
                    break;
            }

            string tituloAlert = "Borrar " + valORec;
            var result = await DisplayAlert(tituloAlert, "¿Estás seguro que querés borrar esta " + valORec + "?", "Sí", "No");
            if (result)
            {
                switch (valORec)
                {
                    case "valoración":
                        var id_val = ((TappedEventArgs)e).Parameter;
                        bool resultEliminarVal = await EliminarValoracion(Convert.ToInt32(id_val));
                        if (resultEliminarVal)
                        {
                            StackPrincipal.Children.Remove(FrameDenuncia);
                            await DisplayAlert("Valoración borrada", "La valoración fue borrada exitosamente.", "Aceptar");
                            await CargarPagina();
                        }
                        else
                        {
                            await DisplayAlert("Error", "No hemos podido borrar la valoración, intentelo más tarde.", "Aceptar");
                        }
                        break;
                    case "recomendación":
                        var id_rec = ((TappedEventArgs)e).Parameter;
                        bool resultEliminarRec = await EliminarRecomendacion(Convert.ToInt32(id_rec));
                        if (resultEliminarRec)
                        {
                            StackPrincipal.Children.Remove(FrameDenuncia);
                            await DisplayAlert("Valoración borrada", "La recomendación fue borrada exitosamente.", "Aceptar");
                            await CargarPagina();
                        }
                        else
                        {
                            await DisplayAlert("Error", "No hemos podido borrar la recomendación, intentelo más tarde.", "Aceptar");
                        }
                        break;
                    default:
                        await DisplayAlert("Error", "No hemos podido borrar tu comentario, intentelo más tarde.", "Aceptar");
                        break;
                }
            }
        }
    }
}