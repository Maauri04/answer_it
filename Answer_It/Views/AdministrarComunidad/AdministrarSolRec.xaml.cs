﻿using Answer_It.Models;
using Answer_It.ViewModels;
using Negocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Answer_It.Views.AdministrarComunidad
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AdministrarSolRec : ContentPage
    {
        public List<AdministrarSolicitudesViewModel> ListaSolRecFinal = new List<AdministrarSolicitudesViewModel>();
        public List<SolicitudRecomendaciones> ListaSolicitudRecomendaciones = new List<SolicitudRecomendaciones>();
        public List<Usuarios> ListaUsuarios = new List<Usuarios>();
        public RestClient client = new RestClient();

        public AdministrarSolRec()
        {
            InitializeComponent();
            CargarPagina();
        }

        private async Task<bool> CargarPagina()
        {
            CargandoSolRec.IsBusy = true;
            CargandoSolRec.IsVisible = true;
            ListaSolRecFinal.Clear();
            ListaSolicitudesRec.ItemsSource = null;
            await CargarSolicitudRecomendaciones();
            await CargarListadoSolRecAsync();
            if (ListaSolRecFinal.Count == 0)
            {
                lblSolRecPendientes.IsVisible = true;
            }
            CargandoSolRec.IsBusy = false;
            CargandoSolRec.IsVisible = false;
            return true;
        }

        public async Task<bool> CargarListadoSolRecAsync()
        {
            foreach (var item in ListaSolicitudRecomendaciones)
            {
                AdministrarSolicitudesViewModel adminSolRec = new AdministrarSolicitudesViewModel
                {
                    id_sol = item.id,
                    titulo = item.titulo,
                    nombre_usuario = await ObtenerUsuario(item.id_usuario),
                    fecha_creacion = item.fecha.GetValueOrDefault()
                };
                ListaSolRecFinal.Add(adminSolRec);
            }
            ListaSolRecFinal = ListaSolRecFinal.OrderByDescending(x => x.fecha_creacion).ToList();
            ListaSolicitudesRec.ItemsSource = null;
            ListaSolicitudesRec.ItemsSource = ListaSolRecFinal;
            return true;
        }

        private async void OnBorrarSolicitud_Tapped(object sender, EventArgs e)
        {
            var result = await DisplayAlert("Borrar solicitud de recomendación", "¿Estás seguro que querés borrar esta solicitud?", "Sí", "No");
            if (result)
            {
                var id_sol_rec = ((TappedEventArgs)e).Parameter;
                bool resultEliminar = await EliminarSolicitudRecomendacion(Convert.ToInt32(id_sol_rec));
                if (resultEliminar)
                {
                    await DisplayAlert("Solicitud borrada", "La solicitud de recomendación fue borrada exitosamente.", "Aceptar");
                    await CargarPagina();
                }
                else
                {
                    await DisplayAlert("Error", "No hemos podido borrar la solicitud, intentelo más tarde.", "Aceptar");
                }
            }
        }

        private async Task<bool> CargarSolicitudRecomendaciones()
        {
            try
            {
                ListaSolicitudRecomendaciones = await client.GetAll<SolicitudRecomendaciones>("http://www.recommenditws.somee.com/api/SolicitudRecomendacionesApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<string> ObtenerUsuario(int id_usuario)
        {
            try
            {
                Usuarios usuario = await client.GetById<Usuarios>("http://www.recommenditws.somee.com/api/UsuariosApi/" + id_usuario);
                return usuario.nombre_usuario;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private async Task<bool> EliminarSolicitudRecomendacion(int id)
        {
            try
            {
                bool result = await client.Delete<SolicitudRecomendaciones>("http://www.recommenditws.somee.com/api/SolicitudRecomendacionesApi/" + id);
                return result;
            }
            catch (Exception)
            {
                await DisplayAlert("Error", "No hemos podido borrar tu solicitud de recomendación, intentelo más tarde.", "Aceptar");
                return false;
            }
        }
    }
}