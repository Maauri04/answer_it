﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Answer_It.Models;
using Answer_It.ViewModels;
using Negocio;

namespace Answer_It.Views.AdministrarComunidad
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AdministrarValoraciones : ContentPage
    {
        public List<AdministrarValoracionesViewModel> ListaValFinal = new List<AdministrarValoracionesViewModel>();
        public List<Valoraciones> ListaValoraciones = new List<Valoraciones>();
        public List<DenunciasValoraciones> ListaDenunciasValoraciones = new List<DenunciasValoraciones>();
        public List<Usuarios> ListaUsuarios = new List<Usuarios>();
        public RestClient client = new RestClient();


        public AdministrarValoraciones()
        {
            InitializeComponent();
            CargarPagina();
        }

        private async Task<bool> CargarPagina()
        {
            CargandoVal.IsBusy = true;
            CargandoVal.IsVisible = true;
            ListaValFinal.Clear();
            ListaVal.ItemsSource = null;
            await CargarValoraciones();
            await CargarDenunciasValoraciones();
            await CargarListadoValAsync();
            if (ListaValFinal.Count == 0)
            {
                lblValPendientes.IsVisible = true;
            }
            CargandoVal.IsBusy = false;
            CargandoVal.IsVisible = false;
            return true;
        }

        public async Task<bool> CargarListadoValAsync()
        {
            foreach (var item in ListaValoraciones)
            {
                AdministrarValoracionesViewModel adminVal = new AdministrarValoracionesViewModel
                {
                    id_val = item.id,
                    titulo_prod = await ObtenerProducto(item.id_producto),
                    nombre_usuario_creador = await ObtenerUsuario(item.id_usuario),
                    fecha = item.fecha.GetValueOrDefault(),
                    descripcion = item.contenido,
                    titulo_val = item.titulo,
                    me_gusta = item.me_gusta,
                    esta_denunciada = ListaDenunciasValoraciones.Exists(x => x.id_valoracion == item.id) ? "Sí" : "No"
                };
                ListaValFinal.Add(adminVal);
            }
            ListaValFinal = ListaValFinal.OrderBy(x => x.me_gusta).ToList();
            ListaVal.ItemsSource = null;
            ListaVal.ItemsSource = ListaValFinal;
            return true;
        }

        private async void OnBorrarValoracion_Tapped(object sender, EventArgs e)
        {
            var result = await DisplayAlert("Borrar valoración", "¿Estás seguro que querés borrar esta valoración?", "Sí", "No");
            if (result)
            {
                var id_val = ((TappedEventArgs)e).Parameter;
                bool resultEliminar = await EliminarValoracion(Convert.ToInt32(id_val));
                if (resultEliminar)
                {
                    await DisplayAlert("Valoración borrada", "La valoración fue borrada exitosamente.", "Aceptar");
                    await CargarPagina();
                }
                else
                {
                    await DisplayAlert("Error", "No hemos podido borrar la valoración, intentelo más tarde.", "Aceptar");
                }
            }
        }

        private async Task<bool> CargarValoraciones()
        {
            try
            {
                ListaValoraciones = await client.GetAll<Valoraciones>("http://www.recommenditws.somee.com/api/ValoracionesApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarDenunciasValoraciones()
        {
            try
            {
                ListaDenunciasValoraciones = await client.GetAll<DenunciasValoraciones>("http://www.recommenditws.somee.com/api/DenunciasValoracionesApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<string> ObtenerUsuario(int id_usuario)
        {
            try
            {
                Usuarios usuario = await client.GetById<Usuarios>("http://www.recommenditws.somee.com/api/UsuariosApi/" + id_usuario);
                return usuario.nombre_usuario;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private async Task<bool> EliminarValoracion(int id)
        {
            try
            {
                bool result = await client.Delete<Valoraciones>("http://www.recommenditws.somee.com/api/ValoracionesApi/" + id);
                return result;
            }
            catch (Exception)
            {
                await DisplayAlert("Error", "No hemos podido borrar tu valoración, intentelo más tarde.", "Aceptar");
                return false;
            }
        }

        private async Task<string> ObtenerProducto(int id_prod)
        {
            try
            {
                Productos prod = await client.GetById<Productos>("http://www.recommenditws.somee.com/api/ProductosApi/" + id_prod);
                return prod.nombre;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}