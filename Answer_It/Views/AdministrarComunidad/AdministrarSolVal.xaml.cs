﻿using Answer_It.Models;
using Answer_It.ViewModels;
using Negocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Answer_It.Views.AdministrarComunidad
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AdministrarSolVal : ContentPage
    {
        public List<AdministrarSolicitudesViewModel> ListaSolValFinal = new List<AdministrarSolicitudesViewModel>();
        public List<SolicitudValoraciones> ListaSolicitudValoraciones = new List<SolicitudValoraciones>();
        public List<Usuarios> ListaUsuarios = new List<Usuarios>();
        public RestClient client = new RestClient();

        public AdministrarSolVal()
        {
            InitializeComponent();
            CargarPagina();
        }

        private async Task<bool> CargarPagina()
        {
            CargandoSolVal.IsBusy = true;
            CargandoSolVal.IsVisible = true;
            ListaSolValFinal.Clear();
            ListaSolicitudesVal.ItemsSource = null;
            await CargarSolicitudValoraciones();
            await CargarListadoSolValAsync();
            if (ListaSolValFinal.Count == 0)
            {
                lblSolValPendientes.IsVisible = true;
            }
            CargandoSolVal.IsBusy = false;
            CargandoSolVal.IsVisible = false;
            return true;
        }

        public async Task<bool> CargarListadoSolValAsync()
        {
            foreach (var item in ListaSolicitudValoraciones)
            {
                AdministrarSolicitudesViewModel adminSolVal = new AdministrarSolicitudesViewModel
                {
                    id_sol = item.id,
                    titulo = await ObtenerProducto(item.id_producto),
                    nombre_usuario = await ObtenerUsuario(item.id_usuario),
                    fecha_creacion = item.fecha.GetValueOrDefault()
                };
                ListaSolValFinal.Add(adminSolVal);
            }
            ListaSolValFinal = ListaSolValFinal.OrderByDescending(x => x.fecha_creacion).ToList();
            ListaSolicitudesVal.ItemsSource = null;
            ListaSolicitudesVal.ItemsSource = ListaSolValFinal;
            return true;
        }

        private async void OnBorrarSolicitud_Tapped(object sender, EventArgs e)
        {
            var result = await DisplayAlert("Borrar solicitud de valoración", "¿Estás seguro que querés borrar esta solicitud?", "Sí", "No");
            if (result)
            {
                var id_sol_val = ((TappedEventArgs)e).Parameter;
                bool resultEliminar = await EliminarSolicitudValoracion(Convert.ToInt32(id_sol_val));
                if (resultEliminar)
                {
                    await DisplayAlert("Solicitud borrada", "La solicitud de valoración fue borrada exitosamente.", "Aceptar");
                    await CargarPagina();
                }
                else
                {
                    await DisplayAlert("Error", "No hemos podido borrar la solicitud, intentelo más tarde.", "Aceptar");
                }
            }
        }

        private async Task<bool> CargarSolicitudValoraciones()
        {
            try
            {
                ListaSolicitudValoraciones = await client.GetAll<SolicitudValoraciones>("http://www.recommenditws.somee.com/api/SolicitudValoracionesApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<string> ObtenerUsuario(int id_usuario)
        {
            try
            {
                Usuarios usuario = await client.GetById<Usuarios>("http://www.recommenditws.somee.com/api/UsuariosApi/" + id_usuario);
                return usuario.nombre_usuario;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private async Task<bool> EliminarSolicitudValoracion(int id)
        {
            try
            {
                bool result = await client.Delete<SolicitudValoraciones>("http://www.recommenditws.somee.com/api/SolicitudValoracionesApi/" + id);
                return result;
            }
            catch (Exception)
            {
                await DisplayAlert("Error", "No hemos podido borrar tu solicitud de valoración, intentelo más tarde.", "Aceptar");
                return false;
            }
        }

        private async Task<string> ObtenerProducto(int id_prod)
        {
            try
            {
                Productos prod = await client.GetById<Productos>("http://www.recommenditws.somee.com/api/ProductosApi/" + id_prod);
                return prod.nombre;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}