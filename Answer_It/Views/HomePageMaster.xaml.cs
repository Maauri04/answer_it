﻿using Answer_It.Models;
using Answer_It.Views;
using Answer_It.Views.AdministrarComunidad;
using Negocio;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XFWatsonDemo;

namespace Answer_It.MasterDetail
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePageMaster : ContentPage
    {
        RestClient client = new RestClient();
        public HomePageMaster()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            StackUsuario.IsVisible = false;
            StackSesion.IsVisible = true;
            StackCerrarSesion.IsVisible = false;
            PersonalizarMenuParaUsuario();
        }

        private void PersonalizarMenuParaUsuario()
        {
            if (App.HaySesion())
            {
                CargarFoto(App.Usuario.id);
                lblNombre.Text = App.Usuario.nombre;
                lblApellido.Text = App.Usuario.apellido;
                lblNombreUsuario.Text = App.Usuario.nombre_usuario;
                lblNivel.Text = App.Usuario.nivel.ToString();
                lblRPoints.Text = App.Usuario.r_points.ToString();
                imgPerfilUsuario.Source = App.Usuario.foto == null ? "Logo_App.png" : App.Usuario.foto; /*ImageSource.FromStream(() => new MemoryStream(App.Usuario.foto));*/
                StackUsuario.IsVisible = true;
                StackSesion.IsVisible = false;
                StackCerrarSesion.IsVisible = true;
                StackAdministrarComunidad.IsVisible = App.Usuario.id_rol == 1 ? true : false;
            }
            else
            {
                StackUsuario.IsVisible = false;
                StackSesion.IsVisible = true;
                StackCerrarSesion.IsVisible = false;
                StackAdministrarComunidad.IsVisible = false;
            }
        }

        private async void CargarFoto(int id_usuario)
        {
            ImagenesUsuarios imgUsuario = await client.GetById<ImagenesUsuarios>("http://www.recommenditws.somee.com/api/ImagenesUsuariosApi/" + id_usuario);
            var pathImgDescargada = imgUsuario == null ? null : await App.DescargarImagenCloudinary(imgUsuario.nombre_imagen);
            imgPerfilUsuario.Source = imgUsuario == null ? "usuario_default.png" : pathImgDescargada;
            App.BorrarImagenLocalmente(pathImgDescargada);
        }

        private void TapGestureRecognizer_IniciarSesion(object sender, EventArgs e)
        {
            App.MasterD.Detail = new NavigationPage(new IniciarSesion()) { BarTextColor = Color.FromHex("#FF8000") };
            App.MasterD.IsPresented = false;
        }

        private void TapGestureRecognizer_Registrarse(object sender, EventArgs e)
        {
            App.MasterD.Detail = new NavigationPage(new Registrarse()) { BarTextColor = Color.FromHex("#FF8000") };
            App.MasterD.IsPresented = false;
        }

        private void TapGestureRecognizer_ValorarProducto(object sender, EventArgs e)
        {
            if (!App.HaySesion())
            {
                App.MasterD.Detail = new NavigationPage(new IniciarSesion()) { BarTextColor = Color.FromHex("#FF8000") };
                App.MasterD.IsPresented = false;
            }
            else
            {
                App.MasterD.Detail = new NavigationPage(new ValorarProducto()) { BarTextColor = Color.FromHex("#FF8000"), Title = "Valorar producto" };
                App.MasterD.IsPresented = false;
            }
        }

        private void TapGestureRecognizer_BuscarEnLaComunidad(object sender, EventArgs e)
        {
            App.MasterD.Detail = new NavigationPage(new Buscar()) { BarTextColor = Color.FromHex("#FF8000"), Title = "Buscar en Recommend It!" };
            App.MasterD.IsPresented = false;
        }

        private void TapGestureRecognizer_ValoracionesRecientes(object sender, EventArgs e)
        {
            App.MasterD.Detail = new NavigationPage(new Inicio()) { BarTextColor = Color.FromHex("#FF8000"), Title = "Valoraciones recientes" };
            App.MasterD.IsPresented = false;
        }

        private void TapGestureRecognizer_ProductosSinValorar(object sender, EventArgs e)
        {
            App.MasterD.Detail = new NavigationPage(new Inicio()) { BarTextColor = Color.FromHex("#FF8000"), Title = "Productos aún sin valorar" };
            App.MasterD.IsPresented = false;
        }

        private void TapGestureRecognizer_TendenciaProductos(object sender, EventArgs e)
        {
            App.MasterD.Detail = new NavigationPage(new TendenciaProductos()) { BarTextColor = Color.FromHex("#FF8000") };
            App.MasterD.IsPresented = false;
        }

        private void TapGestureRecognizer_RankingUsuarios(object sender, EventArgs e)
        {
            App.MasterD.Detail = new NavigationPage(new RankingUsuarios()) { BarTextColor = Color.FromHex("#FF8000") };
            App.MasterD.IsPresented = false;
        }       

        private void TapGestureRecognizer_AcercaDe(object sender, EventArgs e)
        {
            App.MasterD.Detail = new NavigationPage(new AcercaDe()) { BarTextColor = Color.FromHex("#FF8000") };
            App.MasterD.IsPresented = false;
        }

        private async void TapGestureRecognizer_CerrarSesion(object sender, EventArgs e)
        {
            if (App.HaySesion())
            {
                var result = await DisplayAlert("Cerrar sesión", App.Usuario.nombre + ", ¿Estás seguro de cerrar la sesión?", "SÍ", "NO");
                if (result)
                {
                    App.Usuario = null;
                    Application.Current.MainPage = new HomePage();
                }
            }
        }

        private void TapGestureRecognizer_Perfil(object sender, EventArgs e)
        {
            App.MasterD.Detail = new NavigationPage(new PerfilUsuario(App.Usuario.nombre_usuario)) { BarTextColor = Color.FromHex("#FF8000") };
            App.MasterD.IsPresented = false;
        }

        private void TapGestureRecognizer_Logo(object sender, EventArgs e)
        {
            App.MasterD.Detail = new NavigationPage(new Inicio()) { BarTextColor = Color.FromHex("#FF8000"), Title = "Preguntas recientes" };
            App.MasterD.IsPresented = false;
        }

        private void TapGestureRecognizer_Chatbot(object sender, EventArgs e) 
        {
            if (!App.HaySesion())
            {
                DisplayAlert("Asistente Watson", "¡Debes identificarte para interactuar con Watson!", "Aceptar");
                App.MasterD.Detail = new NavigationPage(new IniciarSesion()) { BarTextColor = Color.FromHex("#FF8000") };
                App.MasterD.IsPresented = false;
            }
            else
            {
                App.MasterD.Detail = new NavigationPage(new ChatPage()) { BarTextColor = Color.FromHex("#FF8000"), Title = "Asistente Watson" };
                App.MasterD.IsPresented = false;
            }
        }

        private void TapGestureRecognizer_AdministrarComunidad(object sender, EventArgs e)
        {
            App.MasterD.Detail = new NavigationPage(new AdministrarHome()) { BarTextColor = Color.FromHex("#FF8000") };
            App.MasterD.IsPresented = false;
        }
    }
}