﻿using Answer_It.Models;
using Answer_It.ViewModels;
using Negocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Answer_It.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NotificacionesPage : ContentPage
    {
        RestClient client = new RestClient();

        List<NotificacionesViewModel> ListaNotificacionesUsuario = new List<NotificacionesViewModel>();
        List<Models.Notificaciones> ListaNotificacionesService = new List<Models.Notificaciones>();
        List<NotificacionesTipo> ListaTipoNotificaciones = new List<NotificacionesTipo>();
        List<Usuarios> ListaUsuarios = new List<Usuarios>();

        public NotificacionesPage()
        {
            InitializeComponent();
            Cargando.IsBusy = true;
            Cargando.IsVisible = true;
            CargarUsuarios();
            CargarNotificaciones();
            Cargando.IsBusy = false;
            Cargando.IsVisible = false;
        }

        private async void CargarNotificaciones()
        {
            ListaTipoNotificaciones = await client.GetAll<NotificacionesTipo>("http://www.recommenditws.somee.com/api/NotificacionesTipoApi");
            ListaNotificacionesService = await client.GetAll<Notificaciones>("http://www.recommenditws.somee.com/api/NotificacionesApi");
            ListaNotificacionesService = ListaNotificacionesService.Where(x => x.id_usuario_destino == App.Usuario.id).ToList();
            foreach (var item in ListaNotificacionesService)
            {
                ListaNotificacionesUsuario.Add(new NotificacionesViewModel
                {
                    id = item.id,
                    id_publicacion = item.id_publicacion.GetValueOrDefault(0),
                    id_tipo_notificacion = item.id_tipo,
                    id_usuario_origen = item.id_usuario_origen,
                    id_usuario_destino = item.id_usuario_destino,
                    nombre_usuario_origen = ListaUsuarios.Where(x => x.id == item.id_usuario_origen).First().nombre_usuario,
                    nombre_tipo_notificacion = ListaTipoNotificaciones.Where(x => x.Id == item.id_tipo).FirstOrDefault().Tipo,
                    leida = item.leida,
                    fecha = item.fecha.GetValueOrDefault(DateTime.Now),
                    descripcion = item.descripcion,
                    imagen_notificacion = ObtenerImagenNotificacion(item.id_tipo),
                    color_notificacion = item.leida ? "#636363" : "#167371"
                });
            }
            ListaNotificacionesUsuario = ListaNotificacionesUsuario.OrderByDescending(x => x.leida == false).ThenByDescending(x => x.fecha).ToList();
            ListaNotificaciones.ItemsSource = null;
            ListaNotificaciones.ItemsSource = ListaNotificacionesUsuario;
        }

        private string ObtenerImagenNotificacion(int id_tipo_notificacion)
        {
            string ruta_imagen = "";
            switch (id_tipo_notificacion)
            {
                case 1:
                    ruta_imagen = "Logo_App.png";
                    break;
                case 2:
                    ruta_imagen = "ic_valoraciones_recientes.png";
                    break;
                case 3:
                    ruta_imagen = "ic_recomendar_productos.png";
                    break;
                default:
                    break;
            }
            return ruta_imagen;
        }

        private async void ListaNotificaciones_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var notificacion = e.Item as NotificacionesViewModel;
            if (notificacion != null)
            {
                Notificaciones notificacion_a_actualizar = new Notificaciones { 
                    id = notificacion.id,
                    id_publicacion = notificacion.id_publicacion,
                    id_tipo = notificacion.id_tipo_notificacion,
                    id_usuario_origen = notificacion.id_usuario_origen,
                    id_usuario_destino = notificacion.id_usuario_destino,
                    descripcion = notificacion.descripcion,
                    fecha = notificacion.fecha,
                    leida = true
                };
                await ActualizarNotificacion(notificacion_a_actualizar);
                switch (notificacion.nombre_tipo_notificacion)
                {
                    case "Valoración":
                        await Navigation.PushAsync(new ValoracionesProducto(notificacion.id_publicacion)); //el id del producto
                        break;
                    case "Recomendación":
                        await Navigation.PushAsync(new RecomendacionesProducto(notificacion.id_publicacion)); //el id de la Sol. de Recomendacion
                        break;
                    default:
                        break;
                }
            }          
        }

        private async void CargarUsuarios()
        {
            ListaUsuarios = await client.GetAll<Usuarios>("http://www.recommenditws.somee.com/api/UsuariosApi");
        }

        private async Task<bool> ActualizarNotificacion(Notificaciones notif)
        {
            try
            {
                bool resultado = await client.Put<Notificaciones>("http://www.recommenditws.somee.com/api/NotificacionesApi", notif);
                return resultado;
            }
            catch (Exception e)
            {
                await DisplayAlert("Tuvimos un problema", "Lo sentimos, tuvimos un problema con la notificación. Lo estamos resolviendo!", "Acecptar");
                return false;
            }
        }
    }
}