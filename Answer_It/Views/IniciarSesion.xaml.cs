﻿using Answer_It.MasterDetail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Negocio;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Answer_It.Models;

namespace Answer_It.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IniciarSesion : ContentPage
    {
        RestClient client = new RestClient();
        List<Usuarios> ListaUsuarios = new List<Usuarios>();

        public IniciarSesion()
        {
            InitializeComponent();
            Cargando.IsVisible = true;
            CargarUsuarios();
            Cargando.IsVisible = false;
        }

        private async void BtnIniciarSesion_Clicked(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(editorEmail.Text) && !String.IsNullOrEmpty(editorContrasena.Text))
            {
                //Verifico logeo con base de datos
                btnIniciarSesion.IsVisible = false;
                stackRegistrarse.IsVisible = false;
                Cargando.IsVisible = true;
                Usuarios usu = ListaUsuarios.Where(x => x.nombre_usuario == editorEmail.Text.Trim() && x.contrasena == editorContrasena.Text || x.email == editorEmail.Text.Trim() && x.contrasena == editorContrasena.Text).FirstOrDefault();
                if (usu != null)
                {
                    App.Usuario = usu;
                    await DisplayAlert("¡Bienvenido a RecommendIt!", "Nos alegra verte de nuevo, " + usu.nombre + ".", "Ingresar");
                    Application.Current.MainPage = new HomePage();
                }
                else
                {
                    await DisplayAlert("Datos incorrectos", "Nombre de usuario o contraseña incorrecta.", "Aceptar");
                }
                Cargando.IsVisible = false;
                btnIniciarSesion.IsVisible = true;
                stackRegistrarse.IsVisible = true;
            }
            else
            {
                await DisplayAlert("¡Campos incompletos!", "¡Complete todos los campos para continuar!", "Aceptar");
            }
        }

        private void BtnRegistrarse_Clicked(object sender, EventArgs e)
        {
            App.MasterD.Detail = new NavigationPage(new Registrarse()) { BarTextColor = Color.FromHex("#FF8000") };
        }

        private void lblRegistrarse_Tapped(object sender, EventArgs e)
        {
            App.MasterD.Detail = new NavigationPage(new Registrarse()) { BarTextColor = Color.FromHex("#FF8000") };
        }

        protected override bool OnBackButtonPressed()
        {
            Navigation.PopAsync();
            return true;
        }

        private async void CargarUsuarios()
        {
            ListaUsuarios = await client.GetAll<Usuarios>("http://www.recommenditws.somee.com/api/UsuariosApi");
        }
    }
}