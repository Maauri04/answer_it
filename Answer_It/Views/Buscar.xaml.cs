﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Answer_It.ViewModels;
using System.Collections.ObjectModel;
using Answer_It.MasterDetail;
using Answer_It.Models;
using Negocio;

namespace Answer_It.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Buscar : ContentPage
    {
        RestClient client = new RestClient();

        //Valoraciones
        ObservableCollection<BuscarProductoViewModel> ListaBuscarValoracionesFinal = new ObservableCollection<BuscarProductoViewModel>();
        List<Productos> ListaProductos = new List<Productos>();
        List<Valoraciones> ListaValoraciones = new List<Valoraciones>();
        List<Recomendaciones> ListaRecomendaciones = new List<Recomendaciones>();

        //Usuarios
        ObservableCollection<BuscarUsuarioViewModel> ListaBuscarUsuariosFinal = new ObservableCollection<BuscarUsuarioViewModel>();
        List<Usuarios> ListaUsuarios = new List<Usuarios>();

        public Buscar()
        {
            InitializeComponent();
            CargandoValoraciones.IsBusy = false;
            CargandoValoraciones.IsVisible = false;
            CargandoUsuarios.IsBusy = false;
            CargandoUsuarios.IsVisible = false;
        }      

        private async void btnBuscarProducto_Clicked(object sender, EventArgs e)
        {
            if (!App.TieneConexion())
            {
                await DisplayAlert("Error de conexión", "Parece que no tienes conexión a internet. Conéctate a internet para poder utilizar Recommend It!", "Aceptar");
            }
            else
            {
                if (!String.IsNullOrEmpty(txtBuscarValoracion.Text))
                {
                    OcultarTeclado();
                    CargandoValoraciones.IsBusy = true;
                    CargandoValoraciones.IsVisible = true;
                    OcultarCamposValoracion();
                    await CargarProductos();
                    await CargarValoraciones();
                    await CargarRecomendaciones();
                    string userQuery = txtBuscarValoracion.Text.ToLower().Trim();
                    ListaBuscarValoracionesFinal.Clear();
                    ListViewValoraciones.ItemsSource = null;
                    List<string> queryWordList = new List<string>();
                    queryWordList.AddRange(userQuery.Split(' '));

                    foreach (var producto in ListaProductos)
                    {
                        int cantCoincidencias = 0;
                        foreach (var wordQuery in queryWordList)
                        {
                            if (producto.nombre.ToLower().Contains(wordQuery))
                            {
                                cantCoincidencias += 1; //Ordenar luego de mayor a menor coincidencias
                            }
                        }
                        if (cantCoincidencias > 0)
                        {
                            double valoracionPromedio = CalcularRatingPromedio(producto.id);
                            ImageSource imgProd = ImageSource.FromFile(await CargarFotoProducto(producto.id));
                            ListaBuscarValoracionesFinal.Add(new BuscarProductoViewModel
                            {
                                id_producto = producto.id,
                                titulo = producto.nombre,
                                valoracion = Math.Truncate(100 * valoracionPromedio) / 100,
                                cant_valoraciones = ListaValoraciones.Count(x => x.id_producto == producto.id),
                                cant_recomendaciones = ListaRecomendaciones.Count(x => x.id_producto_recomendado == producto.id),
                                coincidencias = cantCoincidencias,
                                es_tendencia = ListaProductos.OrderByDescending(x => x.valoracion).Take(10).Where(x => x.id == producto.id) != null ? true : false,
                                imagen = imgProd,
                                sin_valoraciones_visible = valoracionPromedio == 0 ? true : false,
                                rating_visible = valoracionPromedio == 0 ? false : true
                            });
                            App.BorrarImagenLocalmente(imgProd);
                        }
                        cantCoincidencias = 0;
                    }
                    ListViewValoraciones.ItemsSource = null;
                    ListViewValoraciones.ItemsSource = ListaBuscarValoracionesFinal.OrderByDescending(x => x.coincidencias).ThenByDescending(x => x.cant_valoraciones);
                }
                else
                {
                    DisplayAlert("Error", "¡Escriba un producto para buscar!", "Cancelar");
                }
                CargandoValoraciones.IsBusy = false;
                CargandoValoraciones.IsVisible = false;
            }     
        }

        private async void btnBuscarUsuario_Clicked(object sender, EventArgs e)
        {
            //COMPROBAR CONEXION A INTERNET
            if (!String.IsNullOrEmpty(txtBuscarUsuario.Text))
            {
                OcultarTeclado();
                CargandoUsuarios.IsBusy = true;
                CargandoUsuarios.IsVisible = true;
                OcultarCamposUsuario();
                await CargarUsuarios();
                string userQuery = txtBuscarUsuario.Text.ToLower();
                ListaBuscarUsuariosFinal.Clear();
                ListViewUsuarios.ItemsSource = null;
                List<string> queryWordList = new List<string>();
                queryWordList.AddRange(userQuery.Split(' '));

                foreach (var usuario in ListaUsuarios)
                {
                    int cantCoincidencias = 0;
                    foreach (var wordQuery in queryWordList)
                    {
                        if (usuario.nombre_usuario.ToLower().Contains(wordQuery))
                        {
                            cantCoincidencias += 1;
                        }
                    }
                    if (cantCoincidencias > 0)
                    {
                        ImageSource imgUsu = ImageSource.FromFile(await CargarFotoUsuario(usuario.id));
                        ListaBuscarUsuariosFinal.Add(new BuscarUsuarioViewModel
                        {
                            nombre_usuario = usuario.nombre_usuario,
                            nivel_usuario = usuario.nivel,
                            ranking_usuario = App.ObtenerRankingUsuario(usuario.id, ListaUsuarios),
                            votos_totales = usuario.votos_positivos.GetValueOrDefault(0) - usuario.votos_negativos.GetValueOrDefault(0),
                            es_verificado = usuario.es_verificado.GetValueOrDefault(false),
                            coincidencias = cantCoincidencias,
                            imagen = imgUsu
                        });
                        App.BorrarImagenLocalmente(imgUsu);
                    }
                    cantCoincidencias = 0;
                }

                ListViewUsuarios.ItemsSource = null;
                ListViewUsuarios.ItemsSource = ListaBuscarUsuariosFinal.OrderByDescending(x => x.coincidencias);
            }
            else
            {
                DisplayAlert("Error", "¡Escriba un producto para buscar!", "Cancelar");
            }
            CargandoUsuarios.IsBusy = false;
            CargandoUsuarios.IsVisible = false;
        }

        private void btnNuevaBusquedaValoracion_Clicked(object sender, EventArgs e)
        {
            MostrarCamposValoracion();
            ListaBuscarValoracionesFinal.Clear();
            ListViewValoraciones.ItemsSource = null;
            ListViewValoraciones.ItemsSource = ListaBuscarValoracionesFinal;
        }

        private void OcultarCamposValoracion()
        {
            lblTituloBuscarValoracion.IsVisible = false;
            frameBuscarValoracion.IsVisible = false;
            btnBuscarValoracion.IsVisible = false;
            btnNuevaBusquedaValoracion.IsVisible = true;
        }

        private void OcultarCamposUsuario()
        {
            lblTituloBuscarUsuario.IsVisible = false;
            frameBuscarUsuario.IsVisible = false;
            btnBuscarUsuario.IsVisible = false;
            btnNuevaBusquedaUsuario.IsVisible = true;
        }

        private void MostrarCamposValoracion()
        {
            lblTituloBuscarValoracion.IsVisible = true;
            frameBuscarValoracion.IsVisible = true;
            btnBuscarValoracion.IsVisible = true;
            btnNuevaBusquedaValoracion.IsVisible = false;
        }

        private void MostrarCamposUsuario()
        {
            lblTituloBuscarUsuario.IsVisible = true;
            frameBuscarUsuario.IsVisible = true;
            btnBuscarUsuario.IsVisible = true;
            btnNuevaBusquedaUsuario.IsVisible = false;
        }

        private void OcultarTeclado()
        {
            txtBuscarValoracion.IsEnabled = false;
            txtBuscarValoracion.IsEnabled = true;
        }

        private void btnNuevaBusquedaUsuario_Clicked(object sender, EventArgs e)
        {
            MostrarCamposUsuario();
        }

        private void ListaValoraciones_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (!App.HaySesion())
            {
                App.MasterD.Detail = new NavigationPage(new IniciarSesion()) { BarTextColor = Color.FromHex("#FF8000") };
            }
            else
            {
                var producto = e.Item as BuscarProductoViewModel;
                if (producto != null)
                {
                    Navigation.PushAsync(new ValoracionesProducto(producto.id_producto));
                }
            }
        }

        private void ListaUsuarios_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (!App.HaySesion())
            {
                App.MasterD.Detail = new NavigationPage(new IniciarSesion()) { BarTextColor = Color.FromHex("#FF8000") };
            }
            else
            {
                var usuario = e.Item as BuscarUsuarioViewModel;
                if (usuario != null)
                {
                    Navigation.PushAsync(new PerfilUsuario(usuario.nombre_usuario));
                }
            }         
        }

        private double CalcularRatingPromedio(int idProducto)
        {
            List<Valoraciones> ListaValProd = ListaValoraciones.Where(x => x.id_producto == idProducto).ToList();
            if (ListaValProd.Count > 0)
            {
                double sumaValoracionesProd = ListaValProd.Sum(x => x.puntaje);
                double ratingPromedioProd = sumaValoracionesProd / ListaValProd.Count();
                return ratingPromedioProd;
            }
            else
            {
                return 0;
            }
        }

        protected override bool OnBackButtonPressed()
        {
            App.MasterD.Detail = new NavigationPage(new Inicio()) { BarTextColor = Color.FromHex("#FF8000") };
            return true;
        }

        // CARGA DE DATOS
        private async Task<bool> CargarProductos()
        {
            try
            {
                ListaProductos = await client.GetAll<Productos>("http://www.recommenditws.somee.com/api/ProductosApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarRecomendaciones()
        {
            try
            {
                ListaRecomendaciones = await client.GetAll<Recomendaciones>("http://www.recommenditws.somee.com/api/RecomendacionesApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarValoraciones()
        {
            try
            {
                ListaValoraciones = await client.GetAll<Valoraciones>("http://www.recommenditws.somee.com/api/ValoracionesApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<string> CargarFotoProducto(int id_producto)
        {
            string pathFinal;
            ImagenesProductos imgDB = await client.GetById<ImagenesProductos>("http://www.recommenditws.somee.com/api/ImagenesProductosApi/" + id_producto);
            var pathImgDescargada = imgDB == null ? null : await App.DescargarImagenCloudinary(imgDB.nombre_imagen);
            if (imgDB != null)
            {
                pathFinal = pathImgDescargada;
            }
            else
            {
                pathFinal = "product_default.png";
            }
            return pathFinal;
        }

        private async Task<string> CargarFotoUsuario(int id_usuario)
        {
            string pathFinal;
            ImagenesUsuarios imgDB = await client.GetById<ImagenesUsuarios>("http://www.recommenditws.somee.com/api/ImagenesUsuariosApi/" + id_usuario);
            var pathImgDescargada = imgDB == null ? null : await App.DescargarImagenCloudinary(imgDB.nombre_imagen);
            if (imgDB != null)
            {
                pathFinal = pathImgDescargada;
            }
            else
            {
                pathFinal = "usuario_default.png";
            }
            return pathFinal;
        }

        private async Task<bool> CargarUsuarios()
        {
            try
            {
                ListaUsuarios = await client.GetAll<Usuarios>("http://www.recommenditws.somee.com/api/UsuariosApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}