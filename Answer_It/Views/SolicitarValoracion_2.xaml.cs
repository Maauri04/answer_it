﻿using Answer_It.Models;
using Negocio;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Answer_It.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SolicitarValoracion_2 : ContentPage
    {
        RestClient client = new RestClient();

        public string _nombreProducto { get; set; }
        public string _aspectoEspecial { get; set; }
        public int _rPointsRecompensa { get; set; }
        public bool _skillPrisa { get; set; }
        public bool _skillDestacado { get; set; }
        public bool _sinSkill { get; set; }
        public MediaFile ImagenSubida { get; set; }
        public string NombreImagenSubida { get; set; }

        public SolicitarValoracion_2(string nombreProducto, string aspectoEspecial)
        {
            InitializeComponent();
            CargandoFoto.IsVisible = false;
            CargandoFoto.IsBusy = false;
            _nombreProducto = nombreProducto;
            _aspectoEspecial = aspectoEspecial;
            InicializarComboRecompensas();
        }

        private void InicializarComboRecompensas()
        {
            List<int> listaRecompensasPosibles = new List<int> { 10, 50, 100 };
            pickerRecompensas.ItemsSource = listaRecompensasPosibles;
            pickerRecompensas.SelectedIndex = 0;
        }

        private void InfoRecompensas_Tapped(object sender, EventArgs e)
        {
            DisplayAlert("Otorgar R! Points como recompensa", "¿Buscas una opinión rápida? ¡Esta función es para tí!\r\n \r\nEsta función permite dar como recompensa parte de tus R! Points acumulados al usuario que responda primero, por ser el primero en colaborar contigo.", "Aceptar");
        }

        private void InfoPrisa_Tapped(object sender, EventArgs e)
        {
            DisplayAlert("¡Tengo prisa!", "Si solicitas una valoración con prisa, tu solicitud se verá por encima del resto de las publicaciones, ¡incluso de aquellas destacadas! \r\n \r\n Se necesitan 500 R! Points", "Aceptar");
        }

        private void InfoDestacado_Tapped(object sender, EventArgs e)
        {
            DisplayAlert("Destacar solicitud", "Si destacas tu solicitud, la misma se verá por encima del resto de las publicaciones, pero debajo de aquellas que tengan prisa. \r\n \r\n Se necesitan 370 R! Points", "Aceptar");
        }

        private void switchPrisa_Toggled(object sender, ToggledEventArgs e)
        {
            if (App.Usuario.nivel >= 4)
            {
                if (stackPrisa.BackgroundColor == Color.Gray)
                {
                    if (stackDestacado.BackgroundColor == Color.OrangeRed)
                    {
                        switchDestacado.IsToggled = false;
                        stackDestacado.BackgroundColor = Color.Gray;
                    }
                    stackPrisa.BackgroundColor = Color.DodgerBlue;
                }
                else
                {
                    stackPrisa.BackgroundColor = Color.Gray;
                }
            }
            else
            {
                switchPrisa.IsToggled = false;
                int reputacionNecesaria = App.ListaNiveles.Where(x => x.nivel == 4).FirstOrDefault().reputacion_necesaria;
                int reputacionFaltante = reputacionNecesaria - App.Usuario.reputacion;
                DisplayAlert("¡Reputación insuficiente!", "¡Para utilizar este beneficio, necesitas alcanzar el nivel 3! Te faltan " + reputacionFaltante + " puntos de reputación.", "Aceptar");
            }
        }

        private void switchDestacado_Toggled(object sender, ToggledEventArgs e)
        {
            if (App.Usuario.nivel >= 3)
            {
                if (stackDestacado.BackgroundColor == Color.Gray)
                {
                    if (stackPrisa.BackgroundColor == Color.DodgerBlue)
                    {
                        switchPrisa.IsToggled = false;
                        stackPrisa.BackgroundColor = Color.Gray;
                    }
                    stackDestacado.BackgroundColor = Color.OrangeRed;
                }
                else
                {
                    stackDestacado.BackgroundColor = Color.Gray;
                }
            }
            else
            {
                switchDestacado.IsToggled = false;
                int reputacionNecesaria = App.ListaNiveles.Where(x => x.nivel == 3).FirstOrDefault().reputacion_necesaria;
                int reputacionFaltante = reputacionNecesaria - App.Usuario.reputacion;
                DisplayAlert("¡Reputación insuficiente!", "¡Para utilizar este beneficio, necesitas alcanzar el nivel 3! Te faltan " + reputacionFaltante + " puntos de reputación.", "Aceptar");
            }
        }

        private void cbDarRecompensa_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            if (App.Usuario.nivel >= 5)
            {
                if (cbDarRecompensa.IsChecked)
                {
                    pickerRecompensas.IsEnabled = true;
                }
                else
                {
                    pickerRecompensas.IsEnabled = false;
                }
            }
            else
            {
                cbDarRecompensa.IsChecked = false;
                int reputacionNecesaria = App.ListaNiveles.Where(x => x.nivel == 3).FirstOrDefault().reputacion_necesaria;
                int reputacionFaltante = reputacionNecesaria - App.Usuario.reputacion;
                DisplayAlert("¡Reputación insuficiente!", "¡Para utilizar este beneficio, necesitas alcanzar el nivel 5! Te faltan " + reputacionFaltante + " puntos de reputación.", "Aceptar");
            }
        }

        private async void btnSubirFoto_Clicked(object sender, EventArgs e)
        {
            try
            {
                await CrossMedia.Current.Initialize();
                if (!CrossMedia.Current.IsPickPhotoSupported)
                {
                    await DisplayAlert("No disponible", "Tu dispositivo no dispone de esta funcionalidad actualmente.", "Aceptar");
                    return;
                }

                CargandoFoto.IsVisible = true;
                CargandoFoto.IsBusy = true;

                var mediaOptions = new PickMediaOptions()
                {
                    //Para aplicar resize, maxQuality, etc.. sobre la img cargada.
                };

                var selectedImageFile = await CrossMedia.Current.PickPhotoAsync(mediaOptions);

                if (imgProducto == null)
                {
                    await DisplayAlert("Error", "No hemos podido cargar la imagen. Inténtelo nuevamente.", "Aceptar");
                }
                else
                {
                    imgProducto.Source = ImageSource.FromStream(() => selectedImageFile.GetStream());
                    NombreImagenSubida = Path.GetFileName(selectedImageFile.Path);
                    ImagenSubida = selectedImageFile;
                }

                CargandoFoto.IsVisible = false;
                CargandoFoto.IsBusy = false;
            }
            catch (Exception)
            {
                await DisplayAlert("¡Permisos necesarios!", "Debes otorgarnos los permisos al almacenamiento de tu teléfono para que puedas subir una foto.", "Aceptar");
                CargandoFoto.IsVisible = false;
                CargandoFoto.IsBusy = false;
            }
        }

        private async void btnSolicitarValoracion_Clicked(object sender, EventArgs e)
        {
            CargandoCrearSolicitud.IsBusy = true;
            CargandoCrearSolicitud.IsVisible = true;
            try
            {
                Productos nuevoProducto = new Productos
                {
                    nombre = _nombreProducto,
                    cant_votos = 0,
                    valoracion = 0,
                    fecha_creacion = DateTime.Now
                };
                int idGenerado = await GuardarProducto(nuevoProducto);
                ObtenerDatosFaltantes();
                SolicitudValoraciones solVal = new SolicitudValoraciones
                {
                    id_producto = idGenerado,
                    id_usuario = App.Usuario.id,
                    aspecto_especial = _aspectoEspecial,
                    cant_recomendaciones = 0,
                    eliminado = false,
                    fecha = DateTime.Now,
                    sin_skill = _sinSkill,
                    skill_prisa = _skillPrisa,
                    skill_destacado = _skillDestacado,
                    visitas = 0,
                    leyenda = "Este producto aún no tiene valoraciones.Sé el primero en valorarlo y obtendrás reputación extra.",
                    reputacion_otorgada = 125
                };
                bool creacionCorrecta = await GuardarSolicitudValoracion(solVal);
                if (creacionCorrecta)
                {
                    string idImagen = "";
                    if (ImagenSubida != null)
                    {
                        idImagen = await App.SubirImagenCloudinary(NombreImagenSubida, ImagenSubida.GetStream());
                    }

                    bool result = true;
                    if (idImagen != "") //Si subio alguna imagen del producto..
                    {
                        result = await AgregarImagenProducto(new ImagenesProductos { id_producto = idGenerado, nombre_imagen = idImagen });
                    }
                    if (!result)
                    {
                        await DisplayAlert("Ups..", "Se produjo un error al subir la foto. No te preocupes, ¡Podrás subirla luego!", "¡De acuerdo!");
                    }
                    await DisplayAlert("Creación exitosa", "¡Solicitud de valoración creada correctamente!", "Aceptar");
                    App.MasterD.Detail = new NavigationPage(new ValoracionesProducto(solVal.id_producto)) { BarTextColor = Color.FromHex("#FF8000") };
                }
                else
                {
                    await DisplayAlert("¡Lo sentimos!", "Hemos tenido un problema para crear tu solicitud. ¡Inténtelo nuevamente!", "Aceptar");
                }
                CargandoCrearSolicitud.IsBusy = false;
                CargandoCrearSolicitud.IsVisible = false;
            }
            catch (Exception)
            {
                await DisplayAlert("¡Lo sentimos!", "Hemos tenido un problema para crear tu solicitud. ¡Inténtelo nuevamente!", "Aceptar");
                CargandoCrearSolicitud.IsBusy = false;
                CargandoCrearSolicitud.IsVisible = false;
            }
        }

        private void ObtenerDatosFaltantes()
        {
            if (cbDarRecompensa.IsChecked)
            {
                _rPointsRecompensa = Convert.ToInt32(pickerRecompensas.SelectedItem);
            }
            _skillPrisa = switchPrisa.IsToggled ? true : false;
            _skillDestacado = switchDestacado.IsToggled ? true : false;
            _sinSkill = !switchPrisa.IsToggled && !switchDestacado.IsToggled ? true : false;
        }

        private async Task<int> GuardarProducto(Productos prod)
        {
            return await client.PostAndGetId<Productos>("http://www.recommenditws.somee.com/api/ProductosApi", prod);
        }

        private async Task<bool> GuardarSolicitudValoracion(SolicitudValoraciones sol)
        {
            return await client.Post<SolicitudValoraciones>("http://www.recommenditws.somee.com/api/SolicitudValoracionesApi", sol);
        }

        private async Task<bool> AgregarImagenProducto(ImagenesProductos img)
        {
            try
            {
                await client.Post<ImagenesProductos>("http://www.recommenditws.somee.com/api/ImagenesProductosApi", img);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }       
    }
}