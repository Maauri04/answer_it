﻿using Answer_It.Models;
using Answer_It.ViewModels;
using Negocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Answer_It.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ValorarProducto : ContentPage
    {
        RestClient client = new RestClient();
        List<Productos> ListaProductos = new List<Productos>();

        public ValorarProducto(string nombreProductoAnt = "", string tituloDescripcionAnt = "", string descripcionAnt = "", bool precargar_producto = false)
        {
            InitializeComponent();
            if (!string.IsNullOrEmpty(nombreProductoAnt) && !string.IsNullOrEmpty(tituloDescripcionAnt) && !string.IsNullOrEmpty(descripcionAnt))
            {
                ProductosAutoComplete.Text = nombreProductoAnt;
                txtTituloValoracion.Text = tituloDescripcionAnt;
                txtDescValoracion.Text = descripcionAnt;
                if (precargar_producto)
                {
                    ProductosAutoComplete.IsEnabled = false;
                }
            }           
            CargarProductosSugerencias();
        }

        public ValorarProducto(int idProducto)
        {
            InitializeComponent();
            CargarProductosSinSugerencias(idProducto);
        }

        private async void CargarProductosSinSugerencias(int idProducto)
        {
            await CargarProductos();
            ProductosAutoComplete.IsEnabled = false;
            ProductosAutoComplete.Text = ListaProductos.Where(x => x.id == idProducto).First().nombre;
        }

        private async void CargarProductosSugerencias() //OPTIMIZAR
        {
            bool cargaCorrecta = await CargarProductos();
            if (cargaCorrecta)
            {
                List<string> ListaProductosAutoComplete = new List<string>();
                foreach (var item in ListaProductos)
                {
                    ListaProductosAutoComplete.Add(item.nombre);
                }
                ProductosAutoComplete.AutoCompleteSource = ListaProductosAutoComplete;
            }
            else
            {
                await DisplayAlert("¡Lo sentimos!", "Tenemos un problema y no es posible crear nuevas valoraciones. ¡Inténtelo nuevamente en unos minutos!", "Aceptar");
                await Navigation.PopAsync();
            }
        }

        private async void btnSiguiente_Clicked(object sender, EventArgs e)
        {
            bool puede_avanzar = true;
            if (!String.IsNullOrEmpty(ProductosAutoComplete.Text) && !String.IsNullOrEmpty(txtTituloValoracion.Text) && !String.IsNullOrEmpty(txtDescValoracion.Text))
            {
                Productos producto = ListaProductos.Where(x => x.nombre == ProductosAutoComplete.Text.Trim()).FirstOrDefault();
                bool precargar_producto = true; //Le aviso al ValorarProducto_2 que cuando vuelva, debe bloquear el campo de producto.
                if (ProductosAutoComplete.IsEnabled) //Viene a crear una valoracion sin ser redireccionado, el form esta totalmente vacio.
                {
                    precargar_producto = false;
                    if (producto == null) //El producto a valorar NO existe en la comunidad, lo creo.
                    {
                        var result = await DisplayAlert("¡Precaución!", "Estás agregando un nuevo producto a la comunidad.\r\n¿Estás seguro que tu producto no está en la lista de sugerencias? ¡Recuerda que serás penalizado si agregas un producto ya existente!", "Aceptar", "Cancelar");
                        if (result)
                        {
                            Productos nuevoProducto = new Productos
                            {
                                nombre = ProductosAutoComplete.Text,
                                cant_votos = 0,
                                valoracion = 0,
                                fecha_creacion = DateTime.Now
                            };
                            int idGenerado = await GuardarProducto(nuevoProducto);
                            nuevoProducto.id = idGenerado;
                            producto = nuevoProducto;
                        }
                        else
                        {
                            puede_avanzar = false;
                        }
                    }
                }
                if (puede_avanzar)
                {
                    await Navigation.PushAsync(new ValorarProducto_2(ProductosAutoComplete.Text.Trim(), producto, txtTituloValoracion.Text.Trim(), txtDescValoracion.Text.Trim(), precargar_producto));
                }
            }
            else
            {
                await DisplayAlert("¡Campos incompletos!", "Complete todos los campos para continuar.", "Aceptar");
            }
        }

        private async Task<int> GuardarProducto(Productos prod)
        {
            return await client.PostAndGetId<Productos>("http://www.recommenditws.somee.com/api/ProductosApi", prod);
        }

        private async Task<bool> CargarProductos()
        {
            try
            {
                ListaProductos = await client.GetAll<Productos>("http://www.recommenditws.somee.com/api/ProductosApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void ProductosAutoComplete_Unfocused(object sender, FocusEventArgs e)
        {
            if (!String.IsNullOrEmpty(ProductosAutoComplete.Text.Trim()))
            {
                bool producto_existe = ListaProductos.Exists(x => x.nombre == ProductosAutoComplete.Text);
                if (producto_existe)
                {
                    stackFooterAutoComplete.BackgroundColor = Color.DarkGreen;
                    lblLeyendaFooterAutoComplete.Text = "Producto existente en la comunidad";
                }
                else
                {
                    stackFooterAutoComplete.BackgroundColor = Color.FromHex("#FF8000");
                    lblLeyendaFooterAutoComplete.Text = "Producto inexistente. ¡Lo crearemos!";
                }
                stackFooterAutoComplete.IsVisible = true;
            }           
        }       
    }
}