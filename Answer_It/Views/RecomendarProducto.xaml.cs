﻿using Answer_It.Models;
using Negocio;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Answer_It.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RecomendarProducto : ContentPage
    {
        RestClient client = new RestClient();
        public int _idSolicitudRec { get; set; }
        List<Productos> ListaProductos = new List<Productos>();
        List<SolicitudRecomendaciones> ListaSolicitudRecomendaciones = new List<SolicitudRecomendaciones>();
        List<ImagenesProductos> ListaImagenesProd = new List<ImagenesProductos>();

        public string NombreImagenSubida { get; set; }
        public MediaFile ImagenSubida { get; set; }

        public int id_solicitud_rec { get; set; }
        public string titulo_solicitud_rec { get; set; }

        public RecomendarProducto(int idSolicitudRec, string tituloSolicitudRec)
        {
            InitializeComponent();
            id_solicitud_rec = idSolicitudRec;
            titulo_solicitud_rec = tituloSolicitudRec;
            CargarPagina();
        }

        private async void CargarPagina()
        {
            CargandoSubirFoto.IsVisible = true;
            CargandoSubirFoto.IsBusy = true;
            _idSolicitudRec = id_solicitud_rec;
            lblTituloNecesidad.Text = titulo_solicitud_rec;
            await CargarProductos(); //TO DO: UNA VEZ QUE CARGUE EL PRODUCTO, SI YA EXISTIA CARGAR SU FOTO PREEXISTENTE, SINO PERMITIRLE AGREGAR SI EL PROD NO EXISTE.
            CargarProductosSugerencias();
            CargandoSubirFoto.IsVisible = false;
            CargandoSubirFoto.IsBusy = false;
        }

        private void CargarProductosSugerencias() //OPTIMIZAR
        {
            List<string> ListaProductosAutoComplete = new List<string>();
            foreach (var item in ListaProductos)
            {
                ListaProductosAutoComplete.Add(item.nombre);
            }
            ProductosAutoComplete.AutoCompleteSource = ListaProductosAutoComplete;
        }

        private async void btnSubirFoto_Clicked(object sender, EventArgs e)
        {
            try
            {
                await CrossMedia.Current.Initialize();
                if (!CrossMedia.Current.IsPickPhotoSupported)
                {
                    await DisplayAlert("No disponible", "Tu dispositivo no dispone de esta funcionalidad actualmente.", "Aceptar");
                    return;
                }

                CargandoSubirFoto.IsVisible = true;
                CargandoSubirFoto.IsBusy = true;

                var mediaOptions = new PickMediaOptions()
                {
                    //Para aplicar resize, maxQuality, etc.. sobre la img cargada.
                };

                var selectedImageFile = await CrossMedia.Current.PickPhotoAsync(mediaOptions);

                imgProducto.Source = ImageSource.FromStream(() => selectedImageFile.GetStream());
                NombreImagenSubida = Path.GetFileName(selectedImageFile.Path);
                ImagenSubida = selectedImageFile;

                CargandoSubirFoto.IsVisible = false;
                CargandoSubirFoto.IsBusy = false;
            }
            catch (Exception)
            {
                CargandoSubirFoto.IsVisible = false;
                CargandoSubirFoto.IsBusy = false;
                await DisplayAlert("¡Permisos necesarios!", "Debes otorgarnos los permisos al almacenamiento de tu teléfono para que puedas subir una foto.", "Aceptar");
            }
        }

        private async Task<bool> CargarProductos()
        {
            try
            {
                ListaProductos = await client.GetAll<Productos>("http://www.recommenditws.somee.com/api/ProductosApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async void btnRecomendar_Clicked(object sender, EventArgs e)
        {
            btnRecomendar.IsEnabled = false;
            int idProdRecomendado = 0;
            if (!String.IsNullOrEmpty(ProductosAutoComplete.Text) && !String.IsNullOrEmpty(txtComentario.Text))
            {
                CargandoCrearRec.IsBusy = true;
                CargandoCrearRec.IsVisible = true;

                bool resultSolRec = await CargarSolicitudRecomendaciones();
                if (resultSolRec)
                {
                    Productos producto = ListaProductos.Where(x => x.nombre == ProductosAutoComplete.Text).FirstOrDefault();
                    if (producto == null) //El producto a recomendar NO existe, lo creo.
                    {
                        Productos nuevoProducto = new Productos
                        {
                            nombre = ProductosAutoComplete.Text,
                            cant_votos = 0,
                            valoracion = 0,
                            fecha_creacion = DateTime.Now
                        };
                        int idGenerado = await GuardarProducto(nuevoProducto);
                        idProdRecomendado = idGenerado;
                    }
                    else
                    {
                        idProdRecomendado = producto.id;
                    }
                    Recomendaciones nuevaRec = new Recomendaciones
                    {
                        id_producto_recomendado = idProdRecomendado,
                        id_solicitud_recomendacion = _idSolicitudRec,
                        eliminado = false,
                        fecha = DateTime.Now,
                        id_usuario = App.Usuario.id,
                        me_gusta = 0,
                        contenido = txtComentario.Text
                    };
                    bool regCorrecto = await client.Post<Recomendaciones>("http://www.recommenditws.somee.com/api/RecomendacionesApi", nuevaRec);
                    if (regCorrecto)
                    {
                        if (ImagenSubida != null)
                        {
                            string idImagen = await App.SubirImagenCloudinary(NombreImagenSubida, ImagenSubida.GetStream());
                            bool resultAgregar = await AgregarImagenProducto(new ImagenesProductos { id_producto = nuevaRec.id_producto_recomendado, nombre_imagen = idImagen });
                            if (!resultAgregar)
                            {
                                await DisplayAlert("Ups..", "No hemos podido subir la foto de tu producto, crearemos tu recomendación sin la foto.", "Aceptar");
                            }
                        }

                        SolicitudRecomendaciones solRec = ListaSolicitudRecomendaciones.Where(x => x.id == id_solicitud_rec).FirstOrDefault();
                        if (solRec != null)
                        {
                            //El producto valorado fue creado por una Solicitud de valoracion. Creamos la notificacion.
                            Notificaciones nuevaNotificacion = new Notificaciones
                            {
                                id_publicacion = id_solicitud_rec,
                                id_tipo = 3, //Recomendacion
                                id_usuario_origen = App.Usuario.id,
                                id_usuario_destino = solRec.id_usuario,
                                fecha = DateTime.Now,
                                descripcion = "El usuario " + App.Usuario.nombre_usuario + " te ha recomendado un producto!",
                                leida = false
                            };
                            bool resultNotif = await EnviarNotificacion(nuevaNotificacion);
                            if (!resultNotif)
                            {
                                //Crear log con errores.
                            }

                            //Actualizo leyenda de la solicitud de recomendacion y le quito los 25 extras a esa solrec (125pts -> 100pts).
                            solRec.leyenda = "Obtendrás " + solRec.reputacion_otorgada + " puntos de reputación si recomiendas un producto.";
                            solRec.reputacion_otorgada = 100; //Al ser ya valorada una vez, baja la recompensa.
                            await ActualizarSolicitudRecomendaciones(solRec);

                            //Actualizo reputacion del usuario por recomendar.
                            App.Usuario.reputacion += solRec.reputacion_otorgada;

                            //Compruebo si el usuario sube nivel.
                            if (App.Usuario.nivel < 10)
                            {
                                int proximoNivel = App.Usuario.nivel + 1;
                                int reputacionProximoNivel = App.ListaNiveles.Where(x => x.nivel == proximoNivel).FirstOrDefault().reputacion_necesaria;
                                if (App.Usuario.reputacion >= reputacionProximoNivel)
                                {
                                    App.Usuario.nivel += 1;

                                    //Notifico la subida de nivel.
                                    Notificaciones notifSubidaNivel = new Notificaciones
                                    {
                                        id_publicacion = 0,
                                        id_tipo = 1, //Comunidad
                                        id_usuario_origen = 3,
                                        id_usuario_destino = App.Usuario.id,
                                        fecha = DateTime.Now,
                                        descripcion = "¡Felicitaciones! Has alcanzado al nivel " + App.Usuario.nivel + ".",
                                        leida = false
                                    };
                                    resultNotif = await EnviarNotificacion(nuevaNotificacion);
                                    if (!resultNotif)
                                    {
                                        //Crear log con errores.
                                    }
                                }
                            }

                            await ActualizarReputacionYNivel(App.Usuario);
                        }

                        CargandoCrearRec.IsBusy = false;
                        CargandoCrearRec.IsVisible = false;
                        await DisplayAlert("Recomendación creada", "¡Gracias por tu aporte, " + App.Usuario.nombre + "! Tu recomendación le será muy útil a muchos usuarios. ¡Ya verás!", "Aceptar");

                        App.MasterD.Detail = new NavigationPage(new RecomendacionesProducto(_idSolicitudRec)) { BarTextColor = Color.FromHex("#FF8000") };
                    }
                    else
                    {
                        CargandoCrearRec.IsBusy = false;
                        CargandoCrearRec.IsVisible = false;
                        btnRecomendar.IsEnabled = true;
                        await DisplayAlert("Error", "Se produjo un error al conectarse con el servidor. Inténtelo nuevamente más tarde", "Aceptar");
                    }
                }           
            }
            else
            {
                CargandoCrearRec.IsBusy = false;
                CargandoCrearRec.IsVisible = false;
                btnRecomendar.IsEnabled = true;
                await DisplayAlert("¡Campos incompletos!", "Complete todos los campos para continuar.", "Aceptar");
            }           
        }

        private async Task<int> GuardarProducto(Productos prod)
        {
            return await client.PostAndGetId<Productos>("http://www.recommenditws.somee.com/api/ProductosApi", prod);
        }

        private async Task<bool> AgregarImagenProducto(ImagenesProductos img)
        {
            bool correcto = await client.Post<ImagenesProductos>("http://www.recommenditws.somee.com/api/ImagenesProductosApi", img);
            return correcto;
        }

        private async void ProductosAutoComplete_Unfocused(object sender, FocusEventArgs e)
        {
            if (!String.IsNullOrEmpty(ProductosAutoComplete.Text.Trim()))
            {
                CargandoFoto.IsBusy = true;
                CargandoFoto.IsVisible = true;
                imgProducto.Source = "product_default.png";
                bool producto_existe = ListaProductos.Exists(x => x.nombre == ProductosAutoComplete.Text);
                if (producto_existe)
                {
                    stackFooterAutoComplete.BackgroundColor = Color.DarkGreen;
                    lblLeyendaFooterAutoComplete.Text = "Producto existente en la comunidad";

                    //Cargo foto al producto (si tiene)
                    Productos prod = ListaProductos.Where(x => x.nombre == ProductosAutoComplete.Text).First();
                    bool tieneFoto = await CargarFotoProducto(prod.id);
                    if (tieneFoto)
                    {
                        btnSubirFoto.IsVisible = false;
                        lblAgregarFoto.Text = "Foto del producto";
                    }
                    else
                    {
                        btnSubirFoto.IsVisible = true;
                        lblAgregarFoto.Text = "Añadir foto al producto (opcional)";
                    }
                }
                else
                {
                    stackFooterAutoComplete.BackgroundColor = Color.FromHex("#FF8000");
                    lblLeyendaFooterAutoComplete.Text = "Producto inexistente. ¡Lo crearemos!";
                    lblAgregarFoto.Text = "Añadir foto al producto (opcional)";
                    btnSubirFoto.IsVisible = true;
                }
                stackFooterAutoComplete.IsVisible = true;
                CargandoFoto.IsBusy = false;
                CargandoFoto.IsVisible = false;
            }
        }

        private async Task<bool> CargarFotoProducto(int id_producto)
        {
            try
            {
                bool tieneFoto = false;
                ListaImagenesProd = await client.GetAll<ImagenesProductos>("http://www.recommenditws.somee.com/api/ImagenesProductosApi");
                ImagenesProductos imgProd = ListaImagenesProd.Where(x => x.id_producto == id_producto).FirstOrDefault();
                var pathImgDescargada = imgProd == null ? null : await App.DescargarImagenCloudinary(imgProd.nombre_imagen);
                imgProducto.Source = imgProd == null ? "product_default.png" : pathImgDescargada;
                tieneFoto = imgProd == null ? false : true;
                App.BorrarImagenLocalmente(pathImgDescargada);
                return tieneFoto;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> EnviarNotificacion(Notificaciones notif)
        {
            try
            {
                await client.Post<Notificaciones>("http://www.recommenditws.somee.com/api/NotificacionesApi", notif);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarSolicitudRecomendaciones()
        {
            try
            {
                ListaSolicitudRecomendaciones = await client.GetAll<SolicitudRecomendaciones>("http://www.recommenditws.somee.com/api/SolicitudRecomendacionesApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> ActualizarSolicitudRecomendaciones(SolicitudRecomendaciones solrec)
        {
            try
            {
                await client.Put<SolicitudRecomendaciones>("http://www.recommenditws.somee.com/api/SolicitudRecomendacionesApi", solrec);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> ActualizarReputacionYNivel(Usuarios usu)
        {
            try
            {
                await client.Put<Usuarios>("http://www.recommenditws.somee.com/api/UsuariosApi", usu);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}