﻿using Answer_It;
using Answer_It.MasterDetail;
using IBM.WatsonDeveloperCloud.Conversation.v1;
using IBM.WatsonDeveloperCloud.Conversation.v1.Model;
using IBM.WatsonDeveloperCloud.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
namespace XFWatsonDemo
{
    public partial class ChatPage : ContentPage
    {
        public ChatPage()
        {
            ChatBotViewModel vm;
            InitializeComponent();

            CargandoWatson.IsRunning = false;
            CargandoWatson.IsVisible = false;
            lblCargando.IsVisible = false;


            BindingContext = vm = new ChatBotViewModel();


            vm.Messages.CollectionChanged += (sender, e) =>
            {
                CargandoWatson.IsVisible = false;
                lblCargando.IsVisible = false;
                var target = vm.Messages[vm.Messages.Count - 1];
                MessagesList.ScrollTo(target, ScrollToPosition.MakeVisible, true);
            };
        }

        protected override bool OnBackButtonPressed()
        {
            App.MasterD.Detail = new NavigationPage(new Inicio()) { BarTextColor = Color.FromHex("#FF8000") };
            return true;
        }

        private void BtnEnviarMensaje_Clicked(object sender, EventArgs e)
        {
            CargandoWatson.IsRunning = true;
            CargandoWatson.IsVisible = true;
            lblCargando.IsVisible = true;
            entryMensaje.Text = null;
        }
    }
}
