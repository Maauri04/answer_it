﻿using Answer_It.Models;
using Answer_It.ViewModels;
using Negocio;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Answer_It.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ValorarProducto_2 : ContentPage
    {
        public string NombreImagenSubida { get; set; }
        public MediaFile ImagenSubida { get; set; }
        public string NombreImagenSubidaSug { get; set; }
        public MediaFile ImagenSubidaSug { get; set; }
        public List<SolicitudValoraciones> ListaSolicitudValoraciones = new List<SolicitudValoraciones>();
        public List<Valoraciones> ListaValoraciones = new List<Valoraciones>();


        RestClient client = new RestClient();

        //Almaceno datos de la pantalla anterior
        public string _tituloProducto { get; set; }
        public string _tituloDescripcion { get; set; }
        public string _descripcion { get; set; }
        public bool _prodExistente { get; set; }
        public Productos _Producto { get; set; }

        public ValorarProducto_2(string tituloProducto, Productos producto, string tituloDescripcion, string descripcion, bool prod_existente)
        {
            InitializeComponent();
            _tituloProducto = tituloProducto;
            _tituloDescripcion = tituloDescripcion;
            _descripcion = descripcion;
            _prodExistente = prod_existente;
            _Producto = producto;
            lblTituloProducto.Text = tituloProducto;
            ratingProducto.RatingSettings.RatedFill = Color.FromHex("#FF8000");
            ratingProducto.RatingSettings.UnRatedFill = Color.White;
            ratingProducto.RatingSettings.BackgroundColor = Color.Black;
            ratingProducto.RatingSettings.RatedStroke = Color.Black;
            Cargando.IsVisible = false;
            Cargando.IsBusy = false;

            imgProducto.Source = "product_default.png";
            if (producto != null)
            {
                CargarFoto(producto.id);
            }
        }

        private async void CargarFoto(int id_producto)
        {
            ImagenesProductos imgDB = await client.GetById<ImagenesProductos>("http://www.recommenditws.somee.com/api/ImagenesProductosApi/" + id_producto);
            var pathImgDescargada = imgDB == null ? null : await App.DescargarImagenCloudinary(imgDB.nombre_imagen);
            if (imgDB != null)
            {
                imgProducto.Source = pathImgDescargada;
                btnSubirFoto.IsVisible = false;
                btnSugerirFoto.IsVisible = true;
            }
            else
            {
                imgProducto.Source = "product_default.png";
            }
            App.BorrarImagenLocalmente(pathImgDescargada);
        }

        private async void btnValorar_Clicked(object sender, EventArgs e)
        {
            Cargando.IsBusy = true;
            Cargando.IsVisible = true;

            bool resultSolVal = await CargarSolicitudValoraciones();
            if (resultSolVal)
            {
                if (ratingProducto.Value >= 1 && ratingProducto.Value <= 5)
                {
                    Valoraciones valoracion = new Valoraciones
                    {
                        id_producto = _Producto.id,
                        id_usuario = App.Usuario.id,
                        fecha = DateTime.Now,
                        titulo = _tituloDescripcion,
                        contenido = _descripcion,
                        eliminado = false,
                        me_gusta = 0,
                        puntaje = ratingProducto.Value
                    };
                    bool correcto = await AgregarValoracion(valoracion);
                    if (correcto)
                    {
                        string idImagen = "";
                        if (ImagenSubida != null)
                        {
                            idImagen = await App.SubirImagenCloudinary(NombreImagenSubida, ImagenSubida.GetStream());
                        }
                        else
                        {
                            if (ImagenSubidaSug != null)
                            {
                                idImagen = await App.SubirImagenCloudinary(NombreImagenSubidaSug, ImagenSubidaSug.GetStream());
                            }
                        }
                        bool result = true;
                        if (idImagen != "") //Si subio alguna imagen del producto..
                        {
                            result = await AgregarImagenProducto(new ImagenesProductos { id_producto = _Producto.id, nombre_imagen = idImagen });
                        }
                        if (!result)
                        {
                            await DisplayAlert("Ups..", "Se produjo un error al subir la foto. No te preocupes, ¡Podrás subirla luego!", "¡De acuerdo!");
                        }

                        SolicitudValoraciones solVal = ListaSolicitudValoraciones.Where(x => x.id_producto == _Producto.id).FirstOrDefault();
                        if (solVal != null)
                        {
                            //El producto valorado fue creado por una Solicitud de valoracion. Creamos la notificacion.
                            Notificaciones nuevaNotificacion = new Notificaciones
                            {
                                id_publicacion = _Producto.id,
                                id_tipo = 2, //Valoracion
                                id_usuario_origen = App.Usuario.id,
                                id_usuario_destino = solVal.id_usuario,
                                fecha = DateTime.Now,
                                descripcion = "El usuario " + App.Usuario.nombre_usuario + " ha valorado tu producto!",
                                leida = false
                            };
                            result = await EnviarNotificacion(nuevaNotificacion);
                            if (!result)
                            {
                                //Crear log con errores.
                            }

                            //Actualizo leyenda de la solicitud de valoracion y le quito los 25 extras a esa solval (125pts -> 100pts).
                            solVal.leyenda = "Obtendrás " + solVal.reputacion_otorgada + " puntos de reputación si valoras este producto.";
                            solVal.reputacion_otorgada = 100; //Al ser ya valorada una vez, baja la recompensa.
                            await ActualizarSolicitudValoraciones(solVal);

                            //Actualizo reputacion del usuario por valorar y los r_points.
                            App.Usuario.reputacion += solVal.reputacion_otorgada;
                            App.Usuario.r_points += solVal.r_points_recompensa != null ? solVal.r_points_recompensa : 0;

                            //Compruebo si el usuario sube nivel.
                            if (App.Usuario.nivel < 10)
                            {
                                int proximoNivel = App.Usuario.nivel + 1;
                                int reputacionProximoNivel = App.ListaNiveles.Where(x => x.nivel == proximoNivel).FirstOrDefault().reputacion_necesaria;
                                if (App.Usuario.reputacion >= reputacionProximoNivel)
                                {
                                    App.Usuario.nivel += 1;

                                    //Notifico la subida de nivel.
                                    Notificaciones notifSubidaNivel = new Notificaciones
                                    {
                                        id_publicacion = 0,
                                        id_tipo = 1, //Comunidad
                                        id_usuario_origen = 3,
                                        id_usuario_destino = App.Usuario.id,
                                        fecha = DateTime.Now,
                                        descripcion = "¡Felicitaciones! Has alcanzado al nivel " + App.Usuario.nivel + ".",
                                        leida = false
                                    };
                                    result = await EnviarNotificacion(nuevaNotificacion);
                                    if (!result)
                                    {
                                        //Crear log con errores.
                                    }
                                }
                            }

                            await ActualizarReputacionYNivel(App.Usuario);
                        }

                        await DisplayAlert("Valoración creada", "¡Gracias por tu aporte, " + App.Usuario.nombre + "! Tu valoración le será muy útil a muchos usuarios. ¡Ya verás!", "Aceptar");
                        App.MasterD.Detail = new NavigationPage(new ValoracionesProducto(_Producto.id)) { BarTextColor = Color.FromHex("#FF8000") };
                    }
                    else
                    {
                        await DisplayAlert("Error", "Se produjo un error al conectarse con el servidor. Inténtelo nuevamente más tarde", "Aceptar");
                    }
                }
                else
                {
                    await DisplayAlert("¡Producto sin valorar!", "Valora de 1 a 5 estrellas el producto para continuar.", "Aceptar");
                }
            }
            else
            {
                await DisplayAlert("Error", "Se produjo un error al conectarse con el servidor. Inténtelo nuevamente más tarde", "Aceptar");
            }

            Cargando.IsBusy = false;
            Cargando.IsVisible = false;
        }

        private async void btnSubirFoto_Clicked(object sender, EventArgs e)
        {
            SubirImagenProducto(false);
        }

        private void btnSugerirFoto_Clicked(object sender, EventArgs e)
        {
            SubirImagenProducto(true);
        }

        private async void SubirImagenProducto(bool es_sugerencia)
        {
            try
            {
                await CrossMedia.Current.Initialize();
                if (!CrossMedia.Current.IsPickPhotoSupported)
                {
                    await DisplayAlert("No disponible", "Tu dispositivo no dispone de esta funcionalidad actualmente.", "Aceptar");
                    return;
                }

                Cargando.IsVisible = true;
                Cargando.IsBusy = true;

                var mediaOptions = new PickMediaOptions()
                {
                    //Para aplicar resize, maxQuality, etc.. sobre la img cargada.
                };

                var selectedImageFile = await CrossMedia.Current.PickPhotoAsync(mediaOptions);

                if (imgProducto == null)
                {
                    await DisplayAlert("Error", "No hemos podido cargar la imagen. Inténtelo nuevamente.", "Aceptar");
                }
                else
                {
                    if (es_sugerencia)
                    {
                        lblHasSugeridoFoto.IsVisible = true;
                        frameImagenSugerida.IsVisible = true;
                        imgCambiarFoto.IsVisible = true;
                        imgProductoSugerida.Source = ImageSource.FromStream(() => selectedImageFile.GetStream());
                        NombreImagenSubidaSug = Path.GetFileName(selectedImageFile.Path);
                        ImagenSubidaSug = selectedImageFile;
                    }
                    else
                    {
                        lblHasSugeridoFoto.IsVisible = false;
                        frameImagenSugerida.IsVisible = false;
                        imgCambiarFoto.IsVisible = false;
                        imgProducto.Source = ImageSource.FromStream(() => selectedImageFile.GetStream());
                        NombreImagenSubida = Path.GetFileName(selectedImageFile.Path);
                        ImagenSubida = selectedImageFile;
                    }
                }

                Cargando.IsVisible = false;
                Cargando.IsBusy = false;
            }
            catch (Exception)
            {
                await DisplayAlert("¡Permisos necesarios!", "Debes otorgarnos los permisos al almacenamiento de tu teléfono para que puedas subir una foto.", "Aceptar");
                Cargando.IsVisible = false;
                Cargando.IsBusy = false;
            }
        }

        private async Task<bool> EnviarNotificacion(Notificaciones notif)
        {
            try
            {
                await client.Post<Notificaciones>("http://www.recommenditws.somee.com/api/NotificacionesApi", notif);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarSolicitudValoraciones()
        {
            try
            {
                ListaSolicitudValoraciones = await client.GetAll<SolicitudValoraciones>("http://www.recommenditws.somee.com/api/SolicitudValoracionesApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarValoraciones()
        {
            try
            {
                ListaValoraciones = await client.GetAll<Valoraciones>("http://www.recommenditws.somee.com/api/ValoracionesApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        protected override bool OnBackButtonPressed()
        {
            App.MasterD.Detail = new NavigationPage(new ValorarProducto(_tituloProducto, _tituloDescripcion, _descripcion, _prodExistente)) { BarTextColor = Color.FromHex("#FF8000") };
            return true;
        }

        private async Task<bool> AgregarImagenProducto(ImagenesProductos img)
        {
            try
            {
                ImagenesProductos imgProducto = await client.GetById<ImagenesProductos>("http://www.recommenditws.somee.com/api/ImagenesProductosApi/" + img.id_producto);
                if (imgProducto != null)
                {
                    await client.Put<ImagenesProductos>("http://www.recommenditws.somee.com/api/ImagenesProductosApi", img);
                }
                else
                {
                    await client.Post<ImagenesProductos>("http://www.recommenditws.somee.com/api/ImagenesProductosApi", img);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> AgregarValoracion(Valoraciones val)
        {
            try
            {
                await client.Post<Valoraciones>("http://www.recommenditws.somee.com/api/ValoracionesApi", val);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> ActualizarReputacionYNivel(Usuarios usu)
        {
            try
            {
                await client.Put<Usuarios>("http://www.recommenditws.somee.com/api/UsuariosApi", usu);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> ActualizarSolicitudValoraciones(SolicitudValoraciones solval)
        {
            try
            {
                await client.Put<SolicitudValoraciones>("http://www.recommenditws.somee.com/api/SolicitudValoracionesApi", solval);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}