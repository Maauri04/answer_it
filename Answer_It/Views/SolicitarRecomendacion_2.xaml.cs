﻿using Syncfusion.XForms.TextInputLayout;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Answer_It.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SolicitarRecomendacion_2 : ContentPage
    {
        public string _tipoProducto { get; set; }
        public string _necesidadBasica { get; set; }
        public List<string> _listaNecesidadesAdicionales = new List<string>();
        public SolicitarRecomendacion_2(string tipoProducto)
        {
            InitializeComponent();
            _tipoProducto = tipoProducto;
        }

        private void frameAgregarNecesidad_Tapped(object sender, EventArgs e)
        {
            int totalNecesidadesCargadas = stackNecesidades.Children.Where(x => x.ClassId == "necesidad").Count();
            if (totalNecesidadesCargadas >= 5)
            {
                if (App.Usuario.nivel < 7)
                {
                    DisplayAlert("¡Lo sentimos!", "Con tu nivel, solamente puedes cargar hasta 5 necesidades adicionales. \r\n \r\n ¡Sube hasta el nivel 7 para habilitar más!", "Aceptar");
                }
                else
                {
                    if (totalNecesidadesCargadas == 8)
                    {
                        DisplayAlert("¡Lo sentimos!", "Solamente puedes cargar hasta hasta 8 necesidades adicionales. ¡Debería ser suficiente!", "Aceptar");
                    }
                    else
                    {
                        CargarNecesidadAdicional();
                    }
                }
            }
            else
            {
                CargarNecesidadAdicional();
            }
        }

        private void CargarNecesidadAdicional()
        {
            SfTextInputLayout textInputNecesidad = new SfTextInputLayout { Hint = "Ej: Secar la ropa", ClassId = "necesidad", ContainerBackgroundColor = Color.Transparent, ContainerType = ContainerType.Outlined, VerticalOptions = LayoutOptions.Center, HorizontalOptions = LayoutOptions.FillAndExpand };
            textInputNecesidad.InputView = new Editor { AutoSize = EditorAutoSizeOption.TextChanges, ClassId = "editorNecesidad" };
            stackNecesidades.Children.Add(textInputNecesidad);
        }

        protected override bool OnBackButtonPressed()
        {
            Navigation.PopAsync();
            return true;
        }

        private void btnSiguiente_Clicked(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(editorNecesidadBasica.Text))
            {
                DisplayAlert("Necesidad básica incompleta", "La necesidad básica está incompleta. ¡Debes escribir al menos una necesidad para que los usuarios puedan recomendarte productos!", "Aceptar");
            }
            else
            {
                //Iterar por todas las necesidades adicionales creadas (hint: foreach stackNecesidades.Children.Where(x => x.ClassId == "necesidad"))
                foreach (var item in stackNecesidades.Children.Where(x => x.ClassId == "necesidad"))
                {
                    var textInputNecesidad = item as SfTextInputLayout;
                    if (textInputNecesidad != null)
                    {
                        var editor = textInputNecesidad.InputView as Editor;
                        _listaNecesidadesAdicionales.Add(editor.Text);
                    }
                }
                _necesidadBasica = editorNecesidadBasica.Text;
                Navigation.PushAsync(new SolicitarRecomendacion_3(_tipoProducto,_necesidadBasica,_listaNecesidadesAdicionales));
            }
        }
    }
}