﻿using Answer_It.Models;
using Negocio;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Answer_It.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PerfilUsuarioCompleto : ContentPage
    {
        //public byte[] imagenConvertida { get; set; }
        public string emailBackup { get; set; }

        //Inicializaciones para la consultar la API
        RestClient client = new RestClient();
        List<Valoraciones> ListaValoraciones = new List<Valoraciones>();
        List<Recomendaciones> ListaRecomendaciones = new List<Recomendaciones>();
        List<Roles> ListaRoles = new List<Roles>();
        List<Usuarios> ListaUsuarios = new List<Usuarios>();
        List<ImagenesUsuarios> ListaImagenes = new List<ImagenesUsuarios>();

        public PerfilUsuarioCompleto()
        {
            InitializeComponent();
            //imgPerfil.Source = "/data/user/0/com.companyname/files/bheowguzyut0awqpnbt6.jpg";

            emailBackup = App.Usuario.email;
            cargandoEmail.IsVisible = false;
            cargandoEmail.IsBusy = true;
            cargandoRol.IsVisible = false;
            cargandoRol.IsBusy = true;
            Cargando.IsVisible = false;
            Cargando.IsBusy = false;
            //ObtenerDatosPerfil();
            CargarPerfil(App.Usuario);
        }

        public async void CargarPerfil(Usuarios user)
        {
            Cargando.IsBusy = true;
            Cargando.IsVisible = true;
            RestClient client = new RestClient();
            ListaImagenes = await client.GetAll<ImagenesUsuarios>("http://www.recommenditws.somee.com/api/ImagenesUsuariosApi");
            ImagenesUsuarios imgUsuario = ListaImagenes.Where(x => x.id_usuario == App.Usuario.id).FirstOrDefault();
            var pathImgDescargada = imgUsuario == null ? null : await App.DescargarImagenCloudinary(imgUsuario.nombre_imagen);
            imgPerfil.Source = imgUsuario == null ? "usuario_default.png" : pathImgDescargada;
            lblPerfilCompleto.Text = user.nombre_usuario;
            lblNombre.Text = user.nombre;
            lblApellido.Text = user.apellido;
            txtEmail.Text = user.email.ToString();
            lblNivel.Text = user.nivel.ToString();
            ListaValoraciones = await client.GetAll<Valoraciones>("http://www.recommenditws.somee.com/api/ValoracionesApi");
            ListaRecomendaciones = await client.GetAll<Recomendaciones>("http://www.recommenditws.somee.com/api/RecomendacionesApi");
            ListaRoles = await client.GetAll<Roles>("http://www.recommenditws.somee.com/api/RolesApi");
            lblValoraciones.Text = ListaValoraciones.Where(x => x.id_usuario == App.Usuario.id).Count().ToString();
            lblRecomendaciones.Text = ListaRecomendaciones.Where(x => x.id_usuario == App.Usuario.id).Count().ToString();
            lblRol.Text = ListaRoles.Where(x => x.id == user.id_rol).FirstOrDefault().nombre;
            if (user.reputacion == 1)
            {
                lblReputacion.Text = user.reputacion.ToString() + " punto";
            }
            else
            {
                lblReputacion.Text = user.reputacion.ToString() + " puntos";
            }
            lblPais.Text = user.pais;
            DateTime fecha = DateTime.Now;
            if (user.fecha_registro != null)
            {
                TimeSpan ts = (DateTime.Now - user.fecha_registro).Value;
                lblFechaRegistro.Text = ts.Days.ToString() + " días";
                if (ts.Days < 2)
                {
                    if (ts.Days == 0)
                    {
                        lblFechaRegistro.Text = "Hoy";
                    }
                    else
                    {
                        lblFechaRegistro.Text = "1 día";
                    }
                }
            }
            App.BorrarImagenLocalmente(pathImgDescargada);
            Cargando.IsBusy = false;
            Cargando.IsVisible = false;
        }

        private async Task<bool> ActualizarEmailPosible(string email) //Comprobar si el nuevo mail aun no existe en otro usuario.
        {
            cargandoEmail.IsBusy = true;
            cargandoEmail.IsVisible = true;
            bool es_posible = false;
            RestClient client = new RestClient();           
            ListaUsuarios = await client.GetAll<Usuarios>("http://www.recommenditws.somee.com/api/UsuariosApi");//URL de la api.
            bool existe = ListaUsuarios.Exists(x => x.email == email);
            if (existe)
            {
                es_posible = false;
            }
            else
            {
                es_posible = true;
            }
            cargandoEmail.IsBusy = false;
            cargandoEmail.IsVisible = false;
            return es_posible;
        }

        private async Task<bool> ActualizarUsuario(Usuarios usu)
        {
            bool regCorrecto = await client.Put<Usuarios>("http://www.recommenditws.somee.com/api/UsuariosApi", usu);
            return regCorrecto;
        }

        private async Task<bool> ActualizarImagenUsuario(ImagenesUsuarios img)
        {
            bool correcto = false;
            //La lista contendra un elemento (la imagen del usuario, si la encontro)
            string url = "http://www.recommenditws.somee.com/api/ImagenesUsuariosApi/" + img.id_usuario;
            ImagenesUsuarios ImgUsuario = await client.GetById<ImagenesUsuarios>(url);
            if (ImgUsuario != null) //El usuario ya tenia una imagen subida, entonces la actualizo.
            {
                correcto = await client.Put<ImagenesUsuarios>("http://www.recommenditws.somee.com/api/ImagenesUsuariosApi", img);
            }
            else //El usuario todavia no tiene imagen. Creo un nuevo registro en la tabla ImagenesUsuarios.
            {
                correcto = await client.Post<ImagenesUsuarios>("http://www.recommenditws.somee.com/api/ImagenesUsuariosApi", img);
            }
            return correcto;
        }

        private async void BtnActualizarEmail_Clicked(object sender, EventArgs e)
        {

            if (!txtEmail.Text.Contains("@"))
            {
                await DisplayAlert("¡ERROR!", "¡El formato de email introducido es incorrecto!", "OK");
            }
            else //Email en formato correcto..
            {
                bool posible = await ActualizarEmailPosible(txtEmail.Text);
                if (posible)
                {
                    App.Usuario.email = txtEmail.Text;
                    bool actualizacionCorrecta = await ActualizarUsuario(App.Usuario);
                    if (actualizacionCorrecta)
                    {
                        await DisplayAlert("Actualización correcta", "Email actualizado correctamente.", "OK");
                    }
                    else
                    {
                        App.Usuario.email = emailBackup;
                        txtEmail.Text = emailBackup;
                        await DisplayAlert("ERROR", "Se produjo un error al conectarse con el servidor. Inténtelo más tarde nuevamente.", "OK");
                    }
                }
                cargandoEmail.IsVisible = false;
                cargandoEmail.IsBusy = false;
            }
        }

        private async void btnSubirFoto_Clicked(object sender, EventArgs e)
        {
            try
            {
                await CrossMedia.Current.Initialize();
                if (!CrossMedia.Current.IsPickPhotoSupported)
                {
                    await DisplayAlert("No disponible", "Tu dispositivo no dispone de esta funcionalidad actualmente.", "Aceptar");
                    return;
                }
                Cargando.IsVisible = true;
                Cargando.IsBusy = true;
                var mediaOptions = new PickMediaOptions()
                {
                    //Para aplicar resize, maxQuality, etc.. sobre la img cargada.
                };

                var selectedImageFile = await CrossMedia.Current.PickPhotoAsync(mediaOptions);

                if (imgPerfil == null)
                {
                    await DisplayAlert("Error", "No hemos podido cargar la imagen. Inténtelo nuevamente.", "Aceptar");
                }
                else
                {
                    //imagenConvertida = App.ConvertirImagenABytes(selectedImageFile.GetStream()); //Guardamos temporalmente la imagen en formato byte, para guardarla en la bd si decide continuar (YA NO!)
                    imgPerfil.Source = ImageSource.FromStream(() => selectedImageFile.GetStream());
                    string nombreImagen = System.IO.Path.GetFileName(selectedImageFile.Path);
                    string idImagen = await App.SubirImagenCloudinary(nombreImagen, selectedImageFile.GetStream());
                    bool result = await ActualizarImagenUsuario(new ImagenesUsuarios{id_usuario = App.Usuario.id, nombre_imagen = idImagen });
                    if (result)
                    {
                        App.Usuario.foto = imgPerfil.Source;
                        await DisplayAlert("Actualización correcta", "Foto actualizada correctamente.", "Aceptar");
                    }
                    else
                    {
                        await DisplayAlert("Error", "Se produjo un error al actualizar la foto. Inténtelo más tarde nuevamente.", "Aceptar");
                    }
                }
                Cargando.IsVisible = false;
                Cargando.IsBusy = false;
            }
            catch (Exception)
            {
                Cargando.IsVisible = false;
                Cargando.IsBusy = false;
                await DisplayAlert("¡Permisos necesarios!", "Debes otorgarnos los permisos al almacenamiento de tu teléfono para que puedas subir una foto.", "Aceptar");
            }
        }

        //EMAIL
        private void editorEmail_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtEmail.Text.Length > 0)
            {
                if (!txtEmail.Text.Contains("@"))
                {
                    inputEmail.HasError = true;
                    inputEmail.ErrorText = "¡Email inválido!";
                }
                else
                {
                    if (txtEmail.Text.Length > 30)
                    {
                        inputEmail.HasError = true;
                        inputEmail.ErrorText = "¡Máximo 40 caracteres!";
                    }
                    else
                    {
                        inputEmail.HasError = false;
                    }
                }
            }
        }
    }
}