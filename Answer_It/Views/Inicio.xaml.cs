﻿using Answer_It.Models;
using Answer_It.ViewModels;
using Answer_It.Views;
using Negocio;
using Newtonsoft.Json;
using Plugin.TextToSpeech;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XFWatsonDemo;

namespace Answer_It.MasterDetail
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Inicio : ContentPage
    {
        //COLORES:
        //Gris background: #383838
        //Naranja original: #FF8000
        //Naranja secundario: #E37200

        RestClient client = new RestClient();
        List<Usuarios> ListaUsuarios = new List<Usuarios>();
        List<SolicitudRecomendaciones> ListaSolicitudRecomendaciones = new List<SolicitudRecomendaciones>();
        List<SolicitudValoraciones> ListaSolicitudValoraciones = new List<SolicitudValoraciones>();
        List<Recomendaciones> ListaRecomendaciones = new List<Recomendaciones>();
        List<NecesidadesRec> ListaNecesidades = new List<NecesidadesRec>();
        List<Valoraciones> ListaValoraciones = new List<Valoraciones>();
        List<Productos> ListaProductos = new List<Productos>();
        List<SolicitudValoracionesViewModel> ListaSolValViewModel = new List<SolicitudValoracionesViewModel>();
        List<SolicitudRecomendacionesViewModel> ListaSolRecViewModel = new List<SolicitudRecomendacionesViewModel>();

        public Inicio()
        {
            InitializeComponent();           
            ObtenerDatos();

            //TESTING PURPOSE

            //CargarSolicitudesValoracion();
            //CargarProductos();
            //CargarValoraciones();
            //CargarNecesidades();
            //CargarSolicitudesRecomendacion();
            //CargarRecomendaciones();
            //CargarNotificaciones();
            //CargarRoles();
            //CargarValoracionesMeGusta();
        }

        private async void ObtenerDatos()
        {
            CargandoVals.IsBusy = true;
            CargandoVals.IsVisible = true;
            CargandoRecs.IsBusy = true;
            CargandoRecs.IsVisible = true;
            await CargarUsuarios();
            await CargarSolicitudValoraciones();
            await CargarSolicitudRecomendaciones();
            await CargarRecomendaciones();
            await CargarNecesidades();
            await CargarValoraciones();
            await CargarProductos();
            await CargarListaSolValoraciones();
            CargarListaSolRecomendaciones();
            CargandoVals.IsBusy = false;
            CargandoVals.IsVisible = false;
            CargandoRecs.IsBusy = false;
            CargandoRecs.IsVisible = false;

            //TESTING PURPOSE
            //App.Usuario = ListaUsuarios.First(x => x.nombre_usuario == "Maauri04");
            //App.Usuario = ListaUsuarios.First(x => x.nombre_usuario == "TestUser");
            if (App.HaySesion())
            {
                App.Usuario.foto = await CargarFotoUsuario(App.Usuario.id);
            }
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override bool OnBackButtonPressed()
        {
            base.OnBackButtonPressed();
            DeseaSalir();
            return true;
        }

        private async void DeseaSalir()
        {
            var result = await DisplayAlert("Salir", "¿Estás seguro que querés salir de Recommend It?", "Sí", "No");
            if (result)
            {
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
        }

        private async Task<bool> CargarListaSolValoraciones()
        {
            foreach (var solval in ListaSolicitudValoraciones)
            {
                Usuarios usuSolVal = ListaUsuarios.Where(x => x.id == solval.id_usuario).First();
                ListaSolValViewModel.Add(new SolicitudValoracionesViewModel
                {
                    id = solval.id,
                    id_producto = solval.id_producto,
                    cant_recomendaciones = ListaRecomendaciones.Where(x => x.id_producto_recomendado == solval.id_producto).Count(),
                    cant_valoraciones = ListaValoraciones.Where(x => x.id_producto == solval.id_producto).Count(),
                    descripcion_publicacion = solval.aspecto_especial,
                    fecha_publicacion = solval.fecha.GetValueOrDefault(DateTime.Now),
                    leyenda_publicacion = solval.leyenda,
                    nombre_producto = ListaProductos.Where(x => x.id == solval.id_producto).First().nombre,
                    nombre_usuario = usuSolVal.nombre_usuario,
                    reputacion_otorgada = solval.reputacion_otorgada,
                    valoracion_producto = ListaProductos.Where(x => x.id == solval.id_producto).First().valoracion.GetValueOrDefault(0),
                    visitas = solval.visitas.GetValueOrDefault(0),
                    sin_skill = solval.sin_skill,
                    skill_destacado = solval.skill_destacado,
                    skill_prisa = solval.skill_prisa,
                    imagen_producto = ImageSource.FromFile(await CargarFotoProducto(solval.id_producto)),
                    imagen_usuario = ImageSource.FromFile(await CargarFotoUsuario(usuSolVal.id)),
                    imagen_reputacion = "reputacion_logo.png"
                });
                App.BorrarImagenLocalmente(ListaSolValViewModel.Last().imagen_producto);
            }

            ListaSolValViewModel = ListaSolValViewModel.OrderByDescending(x => x.skill_prisa).ThenByDescending(x => x.skill_destacado).ThenByDescending(x => x.sin_skill).ThenByDescending(x => x.reputacion_otorgada).ThenByDescending(x => x.fecha_publicacion).ToList();
            ListViewProductos.ItemsSource = null;
            ListViewProductos.ItemsSource = ListaSolValViewModel;
            return true;
        }

        private async void CargarListaSolRecomendaciones()
        {
            foreach (var solrec in ListaSolicitudRecomendaciones)
            {
                Usuarios usuSolRec = ListaUsuarios.Where(x => x.id == solrec.id_usuario).First();
                ListaSolRecViewModel.Add(new SolicitudRecomendacionesViewModel
                {
                    id = solrec.id,
                    cant_recomendaciones = ListaRecomendaciones.Where(x => x.id_solicitud_recomendacion == solrec.id).Count(),
                    contenido = solrec.contenido,
                    fecha_publicacion = solrec.fecha.GetValueOrDefault(DateTime.Now),
                    leyenda_publicacion = solrec.leyenda,
                    nivel_requerido = solrec.nivel_requerido.GetValueOrDefault(0),
                    nombre_usuario = usuSolRec.nombre_usuario,
                    reputacion_otorgada = solrec.reputacion_otorgada,
                    tipo_producto = solrec.tipo_producto,
                    titulo = solrec.titulo,
                    visitas = solrec.visitas.GetValueOrDefault(0),
                    sin_skill = solrec.sin_skill,
                    skill_destacado = solrec.skill_destacado,
                    skill_prisa = solrec.skill_prisa,
                    imagen_usuario = ImageSource.FromFile(await CargarFotoUsuario(usuSolRec.id)),
                    imagen_reputacion = "reputacion_logo.png"
                    //lista_necesidades = ListaNecesidades.Where(x => x.id_solicitud_recomendacion == solrec.id).ToList()
                });
            }

            ListaSolRecViewModel = ListaSolRecViewModel.OrderByDescending(x => x.reputacion_otorgada).ThenByDescending(x => x.fecha_publicacion).ToList();
            ListViewRecomendaciones.ItemsSource = null;
            ListViewRecomendaciones.ItemsSource = ListaSolRecViewModel;
        }

        private async Task<string> CargarFotoProducto(int id_producto)
        {
            string pathFinal;
            ImagenesProductos imgDB = await client.GetById<ImagenesProductos>("http://www.recommenditws.somee.com/api/ImagenesProductosApi/" + id_producto);
            var pathImgDescargada = imgDB == null ? null : await App.DescargarImagenCloudinary(imgDB.nombre_imagen);
            if (imgDB != null)
            {
                pathFinal = pathImgDescargada;
            }
            else
            {
                pathFinal = "product_default.png";
            }
            return pathFinal;
        }

        private async Task<string> CargarFotoUsuario(int id_usuario)
        {
            string pathFinal;
            ImagenesUsuarios imgDB = await client.GetById<ImagenesUsuarios>("http://www.recommenditws.somee.com/api/ImagenesUsuariosApi/" + id_usuario);
            var pathImgDescargada = imgDB == null ? null : await App.DescargarImagenCloudinary(imgDB.nombre_imagen);
            if (imgDB != null)
            {
                pathFinal = pathImgDescargada;
            }
            else
            {
                pathFinal = "usuario_default.png";
            }
            return pathFinal;
        }

        private void BtnAbrirChatBot_Clicked(object sender, EventArgs e)
        {
            if (!App.HaySesion())
            {
                DisplayAlert("Asistente Watson", "¡Debes identificarte para interactuar con Watson!", "Aceptar");
                App.MasterD.Detail = new NavigationPage(new IniciarSesion()) { BarTextColor = Color.FromHex("#FF8000") };
            }
            else
            {
                App.MasterD.Detail = new NavigationPage(new ChatPage()) { BarTextColor = Color.FromHex("#FF8000") };
            }
        }

        //private void ImgPerfil_Clicked(object sender, EventArgs e)
        //{
        //    if (!App.HaySesion())
        //    {
        //        Navigation.PushAsync(new IniciarSesion());
        //        //App.MasterD.Detail = new NavigationPage(new IniciarSesion()) { BarTextColor = Color.FromHex("#FF8000") };
        //    }
        //    else //Hay sesion
        //    {
        //        Navigation.PushAsync(new PerfilUsuario(App.Usuario.nombre_usuario));
        //        //App.MasterD.Detail = new NavigationPage(new PerfilUsuario(App.Usuario.nombre_usuario)) { BarTextColor = Color.FromHex("#FF8000") };
        //    }
        //}

        //private void CargarRoles()
        //{
        //    App.ListaRoles.Add(new Roles { id = 0, nombre = "Usuario" });
        //    App.ListaRoles.Add(new Roles { id = 1, nombre = "Moderador" });
        //    App.ListaRoles.Add(new Roles { id = 2, nombre = "Administrador" });

        //}

        //private void CargarNotificaciones()
        //{
        //    App.ListaNotificaciones.Add(new NotificacionesViewModel { id_tipo_notificacion = 3, id_usuario_origen = 4, id_usuario_destino = 1,descripcion = " Te ha recomendado un producto en tu publicación!", leida = false, fecha = DateTime.Now, id_publicacion = 1 });
        //    App.ListaNotificaciones.Add(new NotificacionesViewModel { id_tipo_notificacion = 2, id_usuario_origen = 4, id_usuario_destino = 1, descripcion = " Te ha valorado un producto que solicitaste!", leida = true, fecha = DateTime.Now, id_publicacion = 1 });
        //    App.ListaNotificaciones.Add(new NotificacionesViewModel { id_tipo_notificacion = 1, id_usuario_origen = -1, id_usuario_destino = 1, descripcion = " ¡Has subido al nivel 5! Sigue así.", leida = true, fecha = DateTime.Now });

        //}

        //private void CargarProductos()
        //{
        //    App.ListaProductos.Add(new ProductosViewModel { id = 1, nombre = "TV Samsung 50 pulgadas smart tv", valoracion = 4.95, cant_votos = 25});
        //    App.ListaProductos.Add(new ProductosViewModel { id = 2, nombre = "Notebook 15 pulgadas ACER core i7", valoracion = 3.50, cant_votos = 15 });
        //    App.ListaProductos.Add(new ProductosViewModel { id = 3, nombre = "Reloj SWATCH rojo con calendario", valoracion = 2.77, cant_votos = 32 });
        //    App.ListaProductos.Add(new ProductosViewModel { id = 4, nombre = "Lavarropas Wirpool con centrifugado", valoracion = 1.20, cant_votos = 8 });
        //    App.ListaProductos.Add(new ProductosViewModel { id = 5, nombre = "Pack de 5 sillas de roble importadas", valoracion = 4.27, cant_votos = 150 });
        //}

        //private void CargarSolicitudesRecomendacion()
        //{
        //    if (App.HaySesion())
        //    {

        //    }
        //    App.ListaSolicitudesRecomendacion.Add(new SolicitudRecomendacionesViewModel { id = 1, sin_skill = true, skill_prisa = false, skill_destacado = false, reputacion_otorgada = 3, cant_recomendaciones = 3, leyenda_publicacion = "Esta publicación aún no tiene recomendaciones. Sé el primero en recomendar y obtendrás reputación extra.", nombre_usuario = "Jose123", fecha_publicacion = DateTime.Now, titulo = "Limpiar el piso", contenido = "Quiero que este producto sea el mejor que compré en mi vida." });
        //    App.ListaSolicitudesRecomendacion.Add(new SolicitudRecomendacionesViewModel { id = 2, sin_skill = true, skill_prisa = false, skill_destacado = false, reputacion_otorgada = 3, cant_recomendaciones = 5, leyenda_publicacion = "Esta publicación aún no tiene recomendaciones. Sé el primero en recomendar y obtendrás reputación extra.", nombre_usuario = "Pablo567", fecha_publicacion = DateTime.Now, titulo = "Computadora para trabajos básicos"});
        //    //Cargo las necesidades de cada solicitud de recomendacion
        //    foreach (var item in App.ListaSolicitudesRecomendacion)
        //    {
        //        item.lista_necesidades.AddRange(App.ListaNecesidades.ToList());
        //        break;
        //    }

        //    App.ListaSolicitudesRecomendacion = App.ListaSolicitudesRecomendacion.OrderByDescending(x => x.reputacion_otorgada).ThenByDescending(x => x.fecha_publicacion).ToList();
        //    ListViewRecomendaciones.ItemsSource = null;
        //    ListViewRecomendaciones.ItemsSource = App.ListaSolicitudesRecomendacion;
        //}

        //private void CargarSolicitudesValoracion()
        //{
        //    if (App.HaySesion())
        //    {

        //    }
        //    App.ListaSolicitudesValoracion.Add(new SolicitudValoracionesViewModel { id = 1, sin_skill = true, skill_prisa = false, skill_destacado = false, reputacion_otorgada = 3, leyenda_publicacion = "Este producto aún no tiene valoraciones. Sé el primero en valorarlo y obtendrás reputación extra.", nombre_usuario = "Carlitos56", fecha_publicacion = DateTime.Now, foto = "ic_perfil.png", cant_valoraciones = 130, cant_recomendaciones = 12, id_producto = 1, nombre_producto = "TV Samsung 50 pulgadas smart tv", valoracion_producto = 4 });
        //    App.ListaSolicitudesValoracion.Add(new SolicitudValoracionesViewModel { id = 2, sin_skill = true, skill_prisa = false, skill_destacado = false, reputacion_otorgada = 3, leyenda_publicacion = "Este producto aún no tiene valoraciones. Sé el primero en valorarlo y obtendrás reputación extra.", nombre_usuario = "Pedro33", fecha_publicacion = DateTime.Now, foto = "ic_perfil.png", cant_valoraciones = 12, cant_recomendaciones = 12, id_producto = 2, nombre_producto = "Notebook 15 pulgadas ACER core i7", valoracion_producto = 2 });
        //    App.ListaSolicitudesValoracion.Add(new SolicitudValoracionesViewModel { id = 3, sin_skill = true, skill_prisa = false, skill_destacado = false, reputacion_otorgada = 3, leyenda_publicacion = "Este producto aún no tiene valoraciones. Sé el primero en valorarlo y obtendrás reputación extra.", nombre_usuario = "Pablolima", fecha_publicacion = DateTime.Now, foto = "ic_perfil.png", cant_valoraciones = 122, cant_recomendaciones = 12, id_producto = 3, nombre_producto = "Reloj SWATCH rojo con calendario", valoracion_producto = 1 } );
        //    App.ListaSolicitudesValoracion.Add(new SolicitudValoracionesViewModel { id = 4, sin_skill = false, skill_prisa = false, skill_destacado = true, reputacion_otorgada = 4, leyenda_publicacion = "Este producto aún no tiene valoraciones. Sé el primero en valorarlo y obtendrás reputación extra.", nombre_usuario = "Tomasito56", fecha_publicacion = DateTime.Now, foto = "ic_perfil.png", cant_valoraciones = 33, cant_recomendaciones = 12, id_producto = 4, nombre_producto = "Lavarropas Wirpool con centrifugado", valoracion_producto = 5 } );
        //    App.ListaSolicitudesValoracion.Add(new SolicitudValoracionesViewModel { id = 5, sin_skill = false, skill_prisa = true, skill_destacado = false, reputacion_otorgada = 5, leyenda_publicacion = "¡Este usuario tiene prisa! Si sabes algo sobre este producto, ¡Ayúdalo y obtendrás reputación extra!", nombre_usuario = "Sebatian con prisa", fecha_publicacion = DateTime.Now, foto = "ic_perfil.png",  cant_valoraciones = 53, cant_recomendaciones = 12, id_producto = 5, nombre_producto = "Pack de 5 sillas de roble importadas", valoracion_producto = 2 } );

        //    App.ListaSolicitudesValoracion = App.ListaSolicitudesValoracion.OrderByDescending(x => x.reputacion_otorgada).ThenByDescending(x => x.fecha_publicacion).ToList();

        //    ListViewProductos.ItemsSource = null;
        //    ListViewProductos.ItemsSource = App.ListaSolicitudesValoracion;
        //}

        //private void CargarValoraciones()
        //{
        //    App.ListaValoraciones.Add(new ValoracionesViewModel { id = 1, id_producto = 1, titulo = "Excelente", contenido = "Estoy muy satisfecho", id_usuario = 2, nombre_usuario = "RecommendIt", puntaje = 5, me_gusta = 0, fecha = DateTime.Today, eliminado = false, estado_cara = null });
        //    App.ListaValoraciones.Add(new ValoracionesViewModel { id = 2, id_producto = 1, titulo = "Genial", contenido = "Estoy muy contento", id_usuario = 2, nombre_usuario = "RecommendIt", puntaje = 4, me_gusta = 3, fecha = DateTime.Today, eliminado = false, estado_cara = null });
        //    App.ListaValoraciones.Add(new ValoracionesViewModel { id = 3, id_producto = 1, titulo = "Horrible", contenido = "Es un producto muy flojo, no es confiable.", id_usuario = 2, nombre_usuario = "RecommendIt", puntaje = 2, me_gusta = -3, fecha = DateTime.Today, eliminado = false, estado_cara = null });
        //}

        //private void CargarNecesidades()
        //{
        //    App.ListaNecesidades.Add(new NecesidadesViewModel { id = 1, id_solicitud_recomendacion = 1, nombre_necesidad = "Limpiar el piso rápidamente" });
        //    App.ListaNecesidades.Add(new NecesidadesViewModel { id = 2, id_solicitud_recomendacion = 1, nombre_necesidad = "Pulir y lustrar el piso" });
        //    App.ListaNecesidades.Add(new NecesidadesViewModel { id = 3, id_solicitud_recomendacion = 1, nombre_necesidad = "Aspirar el piso" });
        //    App.ListaNecesidades.Add(new NecesidadesViewModel { id = 4, id_solicitud_recomendacion = 1, nombre_necesidad = "Descuartizar todo lo que encuentre a su paso" });
        //    App.ListaNecesidades.Add(new NecesidadesViewModel { id = 5, id_solicitud_recomendacion = 1, nombre_necesidad = "Que sea antialérgico para mi perro" });
        //    App.ListaNecesidades.Add(new NecesidadesViewModel { id = 6, id_solicitud_recomendacion = 1, nombre_necesidad = "Que sepa cocinar pollo a la plancha con ensalada de tomate y huevo" });
        //}

        //private void CargarRecomendaciones()
        //{
        //    App.ListaRecomendaciones.Add(new RecomendacionesViewModel { id = 1, id_solicitud_recomendacion = 1, id_usuario = 1, producto_recomendado = new ProductosViewModel { id = 1, nombre = "TV Samsung 50 pulgadas smart tv" }, contenido = "El producto incluso viene en otros colores. Puedes elegir entre varios modelos muy económicos", fecha = DateTime.Now, me_gusta = 5, eliminado = false });
        //    App.ListaRecomendaciones.Add(new RecomendacionesViewModel { id = 1, id_solicitud_recomendacion = 1, id_usuario = 1, producto_recomendado = new ProductosViewModel { id = 2, nombre = "Notebook 15 pulgadas ACER core i7" }, contenido = "Peudes buscar mas informacion de este producto en el siguiente enlace: https://mercadolibre.com/apple-iphone-11", fecha = DateTime.Now, me_gusta = 15, eliminado = false });
        //}

        //private void CargarValoracionesMeGusta()
        //{
        //    App.ListaValoracionesMeGusta.Add(new ValoracionesMeGusta { id = 1, id_usuario_like = 1, id_valoracion = 1, me_gusta = true, no_me_gusta = false });
        //    App.ListaValoracionesMeGusta.Add(new ValoracionesMeGusta { id = 1, id_usuario_like = 1, id_valoracion = 2, me_gusta = false, no_me_gusta = true });
        //}

        private void ListViewProductos_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (!App.HaySesion())
            {
                App.MasterD.Detail = new NavigationPage(new IniciarSesion()) { BarTextColor = Color.FromHex("#FF8000") };
            }
            else
            {
                var solicitud = e.Item as SolicitudValoracionesViewModel;
                if (solicitud != null)
                {
                    Navigation.PushAsync(new ValoracionesProducto(solicitud.id_producto));
                }
            }           
        }

        private void ListViewRecomendaciones_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (!App.HaySesion())
            {
                App.MasterD.Detail = new NavigationPage(new IniciarSesion()) { BarTextColor = Color.FromHex("#FF8000") };
            }
            else
            {
                var solicitud = e.Item as SolicitudRecomendacionesViewModel;
                if (solicitud != null)
                {
                    Navigation.PushAsync(new RecomendacionesProducto(solicitud.id));
                }
            }            
        }

        //NOTIFICACIONES
        private void imgToolBarNotificaciones_Clicked(object sender, EventArgs e)
        {
            if (!App.HaySesion())
            {
                App.MasterD.Detail = new NavigationPage(new IniciarSesion()) { BarTextColor = Color.FromHex("#FF8000") };
            }
            else //Hay sesion
            {
                Navigation.PushAsync(new NotificacionesPage());
            }
        }

        private void FrameSolicitarValoracion_Tapped(object sender, EventArgs e)
        {
            if (!App.HaySesion())
            {
                App.MasterD.Detail = new NavigationPage(new IniciarSesion()) { BarTextColor = Color.FromHex("#FF8000") };
            }
            else //Hay sesion
            {
                Navigation.PushAsync(new SolicitarValoracion());
            }
        }

        private void FrameSolicitarRecomendacion_Tapped(object sender, EventArgs e)
        {
            if (!App.HaySesion())
            {
                App.MasterD.Detail = new NavigationPage(new IniciarSesion()) { BarTextColor = Color.FromHex("#FF8000") };
            }
            else //Hay sesion
            {
                Navigation.PushAsync(new SolicitarRecomendacion());
            }
        }

        private async Task<bool> CargarUsuarios()
        {
            try
            {
                ListaUsuarios = await client.GetAll<Usuarios>("http://www.recommenditws.somee.com/api/UsuariosApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarSolicitudRecomendaciones()
        {
            try
            {
                ListaSolicitudRecomendaciones = await client.GetAll<SolicitudRecomendaciones>("http://www.recommenditws.somee.com/api/SolicitudRecomendacionesApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarSolicitudValoraciones()
        {
            try
            {
                ListaSolicitudValoraciones = await client.GetAll<SolicitudValoraciones>("http://www.recommenditws.somee.com/api/SolicitudValoracionesApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarRecomendaciones()
        {
            try
            {
                ListaRecomendaciones = await client.GetAll<Recomendaciones>("http://www.recommenditws.somee.com/api/RecomendacionesApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarNecesidades()
        {
            try
            {
                ListaNecesidades = await client.GetAll<NecesidadesRec>("http://www.recommenditws.somee.com/api/NecesidadesRecApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarValoraciones()
        {
            try
            {
                ListaValoraciones = await client.GetAll<Valoraciones>("http://www.recommenditws.somee.com/api/ValoracionesApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarProductos()
        {
            try
            {
                ListaProductos = await client.GetAll<Productos>("http://www.recommenditws.somee.com/api/ProductosApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void imgToolBarBuscar_Clicked(object sender, EventArgs e)
        {
            App.MasterD.Detail = new NavigationPage(new Buscar()) { BarTextColor = Color.FromHex("#FF8000") };
        }

        //private void btnBuscarConGoogle_Clicked(object sender, EventArgs e)
        //{
        //    string searchQuery = GoogleSearchBar.Text;
        //    string cx = "a332627d62810c0ca";
        //    string apikey = "AIzaSyDZMsjDu7_X517YZLT_Nfr_9s6Ata2jC9E";
        //    var request = WebRequest.Create("https://www.googleapis.com/customsearch/v1?key=" + apikey + "&cx=" + cx + "&q=" + searchQuery);
        //    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        //    Stream dataStream = response.GetResponseStream();
        //    StreamReader reader = new StreamReader(dataStream);
        //    string responseString = reader.ReadToEnd();
        //    dynamic jsonData = JsonConvert.DeserializeObject(responseString);
        //}
    }
}