﻿using Answer_It.Models;
using Answer_It.ViewModels;
using Negocio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Answer_It.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SolicitarValoracion : ContentPage
    {
        RestClient client = new RestClient();
        List<Productos> ListaProductos = new List<Productos>();
        List<string> ListaProductosAutoComplete = new List<string>();

        public SolicitarValoracion()
        {
            InitializeComponent();
            CargarProductosAsincronicamente();
        }

        private async void CargarProductosAsincronicamente()
        {
            bool cargaCorrecta = await CargarProductos();
            if (cargaCorrecta)
            {
                foreach (var item in ListaProductos)
                {
                    ListaProductosAutoComplete.Add(item.nombre);
                }
                txtProducto.AutoCompleteSource = ListaProductosAutoComplete;
                await DisplayAlert("¡Trabajemos en comunidad!", "» Asegúrate de que tu producto no esté ya valorado en la comunidad (¡Serás penalizado si pides valoración de un producto ya valorado!) \r\n \r\n » Sé conciso y escribe nada más ni menos que el nombre de tu producto. \r\n \r\n » Comprueba que no haya errores ortográficos o gramaticales.", "Aceptar");
            }
            else
            {
                await DisplayAlert("¡Lo sentimos!", "Tenemos un problema y no es posible crear nuevas solicitudes. ¡Inténtelo nuevamente en unos minutos!", "Aceptar");
                await Navigation.PopAsync();
            }
        }

        private async void btnSolicitarValoracion_Clicked(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtProducto.Text))
            {
                Productos producto = ListaProductos.Where(x => x.nombre == txtProducto.Text).FirstOrDefault();
                if (producto != null)
                {
                    await DisplayAlert("¡Ya tenemos tu producto!", "Tu producto ya está siendo valorado en la comunidad. ¡Veamos sus valoraciones!", "Aceptar");
                    await Navigation.PushAsync(new ValoracionesProducto(producto.id));
                }
                else
                {
                    var result = await DisplayAlert("¡Precaución!", "Estás agregando un nuevo producto a la comunidad.\r\n¿Estás seguro que tu producto no se encuentra ya valorado en la comunidad? ¡Recuerda que serás penalizado si agregas un producto ya existente!", "Aceptar", "Cancelar");
                    if (result)
                    {
                        await Navigation.PushAsync(new SolicitarValoracion_2(txtProducto.Text, txtAspectoEspecial.Text));
                    }
                }
            }
            else
            {
                await DisplayAlert("Error", "Escribe tu producto para crear una valoración, o seleccionalo de la lista de sugerencias.", "Aceptar");
            }
        }

        private async Task<bool> CargarProductos()
        {
            try
            {
                ListaProductos = await client.GetAll<Productos>("http://www.recommenditws.somee.com/api/ProductosApi");
                return true;
            }
            catch (Exception)
            {
                await DisplayAlert("Error", "Escribe tu producto para crear una valoración, o seleccionalo de la lista de sugerencias.", "Aceptar");
                return false;
            }
        }

        private void txtProducto_Unfocused(object sender, FocusEventArgs e)
        {
            if (!String.IsNullOrEmpty(txtProducto.Text.Trim()))
            {
                bool producto_existe = ListaProductos.Exists(x => x.nombre == txtProducto.Text);
                if (producto_existe)
                {
                    stackFooterAutoComplete.BackgroundColor = Color.DarkGreen;
                    lblLeyendaFooterAutoComplete.Text = "Producto existente en la comunidad";
                }
                else
                {
                    stackFooterAutoComplete.BackgroundColor = Color.FromHex("#FF8000");
                    lblLeyendaFooterAutoComplete.Text = "Producto inexistente. ¡Lo crearemos!";
                }
                stackFooterAutoComplete.IsVisible = true;
            }
        }
    }
}