﻿using Answer_It.Models;
using Answer_It.ViewModels;
using ImageCircle.Forms.Plugin.Abstractions;
using Negocio;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Answer_It.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RankingUsuarios : ContentPage
    {
        RestClient client = new RestClient();
        public List<Usuarios> ListaTop10Usuarios = new List<Usuarios>();
        public List<Usuarios> ListaUsuarios = new List<Usuarios>();
        public List<ImagenesUsuarios> ListaImagenes = new List<ImagenesUsuarios>();

        public RankingUsuarios()
        {
            InitializeComponent();
            CargarPagina();
        }

        private async void CargarPagina()
        {
            Cargando.IsBusy = true;
            Cargando.IsVisible = true;
            await CargarUsuarios();
            ListaTop10Usuarios = ListaUsuarios.OrderByDescending(x => x.reputacion).Take(10).ToList();
            await CargarRanking();
            Cargando.IsBusy = false;
            Cargando.IsVisible = false;
        }

        public async Task<bool> CargarRanking()
        {
            try
            {
                for (int i = 0; i < ListaTop10Usuarios.Count; i++)
                {
                    Label lblIdUsu = new Label { Text = ListaTop10Usuarios[i].id.ToString(), IsVisible = false };
                    Label lblPos = new Label { Text = (i + 1).ToString(), TextColor = Color.White, FontAttributes = FontAttributes.Bold, VerticalTextAlignment = TextAlignment.Center };
                    CircleImage imgUsu = new CircleImage { Source = await CargarFotoUsuario(ListaTop10Usuarios[i].id), HeightRequest = 28, Margin = new Thickness(4, 0, 0, 0) };
                    Label lblNombreUsu = new Label { Text = ListaTop10Usuarios[i].nombre_usuario, TextColor = Color.FromHex("#FF8000"), FontAttributes = FontAttributes.Bold, FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)), VerticalTextAlignment = TextAlignment.Center, Margin = new Thickness(4, 0, 0, 0) };
                    //Image imgReputacion = new Image { Source = "reputacion_logo.png", HeightRequest = 30 };
                    Label lblReputacion = new Label { Text = ListaTop10Usuarios[i].reputacion.ToString() + " pts.", TextColor = Color.FromHex("#FF8000"), FontAttributes = FontAttributes.Bold, VerticalTextAlignment = TextAlignment.Center, HorizontalOptions = LayoutOptions.EndAndExpand };
                    StackLayout stackUsu = new StackLayout { Orientation = StackOrientation.Horizontal, Padding = 6 };
                    switch (i)
                    {
                        case 0:
                            lblNombreUsu.TextColor = Color.White;
                            lblReputacion.TextColor = Color.White;
                            stackUsu.BackgroundColor = Color.DarkGoldenrod;
                            break;
                        case 1:
                            lblNombreUsu.TextColor = Color.White;
                            lblReputacion.TextColor = Color.White;
                            stackUsu.BackgroundColor = Color.LightSlateGray;
                            break;
                        case 2:
                            lblNombreUsu.TextColor = Color.White;
                            lblReputacion.TextColor = Color.White;
                            stackUsu.BackgroundColor = Color.SaddleBrown;
                            break;
                        default:
                            stackUsu.BackgroundColor = Color.FromHex("#5F5F5F");
                            break;
                    }

                    stackUsu.Children.Add(lblIdUsu);
                    stackUsu.Children.Add(lblPos);
                    stackUsu.Children.Add(imgUsu);
                    stackUsu.Children.Add(lblNombreUsu);
                    //stackUsu.Children.Add(imgReputacion);
                    stackUsu.Children.Add(lblReputacion);
                    var tapGestureRecognizerUsuario = new TapGestureRecognizer();
                    tapGestureRecognizerUsuario.Tapped += (sender, e) =>
                    {
                        Navigation.PushAsync(new PerfilUsuario(lblNombreUsu.Text));
                    };
                    stackUsu.GestureRecognizers.Add(tapGestureRecognizerUsuario);

                    StackRanking.Children.Add(stackUsu);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }           
        }

        private async Task<bool> CargarUsuarios()
        {
            try
            {
                ListaUsuarios = await client.GetAll<Usuarios>("http://www.recommenditws.somee.com/api/UsuariosApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<string> CargarFotoUsuario(int idi_usu)
        {
            ListaImagenes = await client.GetAll<ImagenesUsuarios>("http://www.recommenditws.somee.com/api/ImagenesUsuariosApi");
            ImagenesUsuarios imgUsuarioDb = ListaImagenes.Where(x => x.id_usuario == idi_usu).FirstOrDefault();
            var pathImgDescargada = imgUsuarioDb == null ? null : await App.DescargarImagenCloudinary(imgUsuarioDb.nombre_imagen);
            return imgUsuarioDb == null ? "usuario_default.png" : pathImgDescargada;
        }
    }
}