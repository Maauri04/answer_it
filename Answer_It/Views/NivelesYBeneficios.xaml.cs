﻿using Answer_It.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Answer_It.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NivelesYBeneficios : ContentPage
    {
        List<NivelesBeneficios> ListaNivelesBeneficios = new List<NivelesBeneficios>();
        public NivelesYBeneficios()
        {
            InitializeComponent();
            ListaNivelesBeneficios = App.ListaNiveles.ToList();
            CargarBeneficios();
        }

        private void CargarBeneficios()
        {
            switch (App.Usuario.nivel)
            {
                case 1:
                    stepNivel2.Status = Syncfusion.XForms.ProgressBar.StepStatus.InProgress;
                    lblBeneficios1.Text = ListaNivelesBeneficios.Where(x => x.nivel == 1).FirstOrDefault().beneficios;
                    lblBeneficios2.Text = "Conocerás los beneficios\r\ncuando llegues a este nivel.";
                    break;
                case 2:
                    stepNivel3.Status = Syncfusion.XForms.ProgressBar.StepStatus.InProgress;
                    lblBeneficios1.Text = ListaNivelesBeneficios.Where(x => x.nivel == 1).FirstOrDefault().beneficios;
                    lblBeneficios2.Text = ListaNivelesBeneficios.Where(x => x.nivel == 2).FirstOrDefault().beneficios;
                    lblBeneficios3.Text = "Conocerás los beneficios\r\ncuando llegues a este nivel.";
                    break;
                case 3:
                    stepNivel4.Status = Syncfusion.XForms.ProgressBar.StepStatus.InProgress;
                    lblBeneficios1.Text = ListaNivelesBeneficios.Where(x => x.nivel == 1).FirstOrDefault().beneficios;
                    lblBeneficios2.Text = ListaNivelesBeneficios.Where(x => x.nivel == 2).FirstOrDefault().beneficios;
                    lblBeneficios3.Text = ListaNivelesBeneficios.Where(x => x.nivel == 3).FirstOrDefault().beneficios;
                    lblBeneficios4.Text = "Conocerás los beneficios\r\ncuando llegues a este nivel.";
                    break;
                case 4:
                    stepNivel5.Status = Syncfusion.XForms.ProgressBar.StepStatus.InProgress;
                    lblBeneficios1.Text = ListaNivelesBeneficios.Where(x => x.nivel == 1).FirstOrDefault().beneficios;
                    lblBeneficios2.Text = ListaNivelesBeneficios.Where(x => x.nivel == 2).FirstOrDefault().beneficios;
                    lblBeneficios3.Text = ListaNivelesBeneficios.Where(x => x.nivel == 3).FirstOrDefault().beneficios;
                    lblBeneficios4.Text = ListaNivelesBeneficios.Where(x => x.nivel == 4).FirstOrDefault().beneficios;
                    lblBeneficios5.Text = "Conocerás los beneficios\r\ncuando llegues a este nivel.";
                    break;
                case 5:
                    stepNivel6.Status = Syncfusion.XForms.ProgressBar.StepStatus.InProgress;
                    lblBeneficios1.Text = ListaNivelesBeneficios.Where(x => x.nivel == 1).FirstOrDefault().beneficios;
                    lblBeneficios2.Text = ListaNivelesBeneficios.Where(x => x.nivel == 2).FirstOrDefault().beneficios;
                    lblBeneficios3.Text = ListaNivelesBeneficios.Where(x => x.nivel == 3).FirstOrDefault().beneficios;
                    lblBeneficios4.Text = ListaNivelesBeneficios.Where(x => x.nivel == 4).FirstOrDefault().beneficios;
                    lblBeneficios5.Text = ListaNivelesBeneficios.Where(x => x.nivel == 5).FirstOrDefault().beneficios;
                    lblBeneficios6.Text = "Conocerás los beneficios\r\ncuando llegues a este nivel.";
                    break;
                case 6:
                    stepNivel7.Status = Syncfusion.XForms.ProgressBar.StepStatus.InProgress;
                    lblBeneficios1.Text = ListaNivelesBeneficios.Where(x => x.nivel == 1).FirstOrDefault().beneficios;
                    lblBeneficios2.Text = ListaNivelesBeneficios.Where(x => x.nivel == 2).FirstOrDefault().beneficios;
                    lblBeneficios3.Text = ListaNivelesBeneficios.Where(x => x.nivel == 3).FirstOrDefault().beneficios;
                    lblBeneficios4.Text = ListaNivelesBeneficios.Where(x => x.nivel == 4).FirstOrDefault().beneficios;
                    lblBeneficios5.Text = ListaNivelesBeneficios.Where(x => x.nivel == 5).FirstOrDefault().beneficios;
                    lblBeneficios6.Text = ListaNivelesBeneficios.Where(x => x.nivel == 6).FirstOrDefault().beneficios;
                    lblBeneficios7.Text = "Conocerás los beneficios\r\ncuando llegues a este nivel.";
                    break;
                case 7:
                    stepNivel8.Status = Syncfusion.XForms.ProgressBar.StepStatus.InProgress;
                    lblBeneficios1.Text = ListaNivelesBeneficios.Where(x => x.nivel == 1).FirstOrDefault().beneficios;
                    lblBeneficios2.Text = ListaNivelesBeneficios.Where(x => x.nivel == 2).FirstOrDefault().beneficios;
                    lblBeneficios3.Text = ListaNivelesBeneficios.Where(x => x.nivel == 3).FirstOrDefault().beneficios;
                    lblBeneficios4.Text = ListaNivelesBeneficios.Where(x => x.nivel == 4).FirstOrDefault().beneficios;
                    lblBeneficios5.Text = ListaNivelesBeneficios.Where(x => x.nivel == 5).FirstOrDefault().beneficios;
                    lblBeneficios6.Text = ListaNivelesBeneficios.Where(x => x.nivel == 6).FirstOrDefault().beneficios;
                    lblBeneficios7.Text = ListaNivelesBeneficios.Where(x => x.nivel == 7).FirstOrDefault().beneficios;
                    lblBeneficios8.Text = "Conocerás los beneficios\r\ncuando llegues a este nivel.";
                    break;
                case 8:
                    stepNivel9.Status = Syncfusion.XForms.ProgressBar.StepStatus.InProgress;
                    lblBeneficios1.Text = ListaNivelesBeneficios.Where(x => x.nivel == 1).FirstOrDefault().beneficios;
                    lblBeneficios2.Text = ListaNivelesBeneficios.Where(x => x.nivel == 2).FirstOrDefault().beneficios;
                    lblBeneficios3.Text = ListaNivelesBeneficios.Where(x => x.nivel == 3).FirstOrDefault().beneficios;
                    lblBeneficios4.Text = ListaNivelesBeneficios.Where(x => x.nivel == 4).FirstOrDefault().beneficios;
                    lblBeneficios5.Text = ListaNivelesBeneficios.Where(x => x.nivel == 5).FirstOrDefault().beneficios;
                    lblBeneficios6.Text = ListaNivelesBeneficios.Where(x => x.nivel == 6).FirstOrDefault().beneficios;
                    lblBeneficios7.Text = ListaNivelesBeneficios.Where(x => x.nivel == 7).FirstOrDefault().beneficios;
                    lblBeneficios8.Text = ListaNivelesBeneficios.Where(x => x.nivel == 8).FirstOrDefault().beneficios;
                    lblBeneficios9.Text = "Conocerás los beneficios\r\ncuando llegues a este nivel.";
                    break;
                case 9:
                    stepNivel10.Status = Syncfusion.XForms.ProgressBar.StepStatus.InProgress;
                    lblBeneficios1.Text = ListaNivelesBeneficios.Where(x => x.nivel == 1).FirstOrDefault().beneficios;
                    lblBeneficios2.Text = ListaNivelesBeneficios.Where(x => x.nivel == 2).FirstOrDefault().beneficios;
                    lblBeneficios3.Text = ListaNivelesBeneficios.Where(x => x.nivel == 3).FirstOrDefault().beneficios;
                    lblBeneficios4.Text = ListaNivelesBeneficios.Where(x => x.nivel == 4).FirstOrDefault().beneficios;
                    lblBeneficios5.Text = ListaNivelesBeneficios.Where(x => x.nivel == 5).FirstOrDefault().beneficios;
                    lblBeneficios6.Text = ListaNivelesBeneficios.Where(x => x.nivel == 6).FirstOrDefault().beneficios;
                    lblBeneficios7.Text = ListaNivelesBeneficios.Where(x => x.nivel == 7).FirstOrDefault().beneficios;
                    lblBeneficios8.Text = ListaNivelesBeneficios.Where(x => x.nivel == 8).FirstOrDefault().beneficios;
                    lblBeneficios9.Text = ListaNivelesBeneficios.Where(x => x.nivel == 9).FirstOrDefault().beneficios;
                    lblBeneficios10.Text = "Conocerás los beneficios\r\ncuando llegues a este nivel.";
                    break;
                case 10:
                    stepNivel10.Status = Syncfusion.XForms.ProgressBar.StepStatus.Completed;
                    lblBeneficios1.Text = ListaNivelesBeneficios.Where(x => x.nivel == 1).FirstOrDefault().beneficios;
                    lblBeneficios2.Text = ListaNivelesBeneficios.Where(x => x.nivel == 2).FirstOrDefault().beneficios;
                    lblBeneficios3.Text = ListaNivelesBeneficios.Where(x => x.nivel == 3).FirstOrDefault().beneficios;
                    lblBeneficios4.Text = ListaNivelesBeneficios.Where(x => x.nivel == 4).FirstOrDefault().beneficios;
                    lblBeneficios5.Text = ListaNivelesBeneficios.Where(x => x.nivel == 5).FirstOrDefault().beneficios;
                    lblBeneficios6.Text = ListaNivelesBeneficios.Where(x => x.nivel == 6).FirstOrDefault().beneficios;
                    lblBeneficios7.Text = ListaNivelesBeneficios.Where(x => x.nivel == 7).FirstOrDefault().beneficios;
                    lblBeneficios8.Text = ListaNivelesBeneficios.Where(x => x.nivel == 8).FirstOrDefault().beneficios;
                    lblBeneficios9.Text = ListaNivelesBeneficios.Where(x => x.nivel == 9).FirstOrDefault().beneficios;
                    lblBeneficios10.Text = ListaNivelesBeneficios.Where(x => x.nivel == 10).FirstOrDefault().beneficios;
                    break;
                default:
                    break;
            }
        }
    }
}