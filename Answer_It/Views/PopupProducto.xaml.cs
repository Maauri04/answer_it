﻿using Answer_It.ViewModels;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Answer_It.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopupProducto : PopupPage
    {
        public PopupProducto(MercadoLibreViewModel meli)
        {
            InitializeComponent();

            TituloProd.Text = meli.title;
            ImagenProd.Source = meli.thumbnail.Replace("http://", "https://");
            PrecioProd.Text = meli.price;
            NivelVendedorProd.Text = meli.power_seller_status;
            RatingsPositivosProd.Text = meli.transactions_rating_positive;
            CantidadProd.Text = meli.available_quantity;
            CondicionProd.Text = meli.condition;
            RatingLevel.Text = meli.level_id;

            foreach (var attr in meli.attributes)
            {
                ScrollView scrollableView = new ScrollView();
                StackLayout stackUnAtributo = new StackLayout { 
                    Orientation = StackOrientation.Horizontal
                };
                Label lblNameAttr = new Label
                {
                    Text = attr.name + ":",
                    TextColor = Color.FromHex("#FF8000")
                };
                Label lblValueAttr = new Label
                {
                    Text = attr.value_name,
                    TextColor = Color.White,
                    FontSize = 15,
                    Margin = new Thickness(2, 0, 0, 0)
                };
                stackUnAtributo.Children.Add(lblNameAttr);
                stackUnAtributo.Children.Add(lblValueAttr);
                StackProducto.Children.Add(scrollableView);
                StackProducto.Children.Add(stackUnAtributo);
            }

            //URL del producto
            UrlProd.Text = meli.permalink;
            var tapGestureRecognizer = new TapGestureRecognizer();
            tapGestureRecognizer.Tapped += async (s, e) => {
                await Launcher.OpenAsync(new Uri(((Label)s).Text));
            };
            UrlProd.GestureRecognizers.Add(tapGestureRecognizer);

            EstilosVarios();
        }

        private void EstilosVarios()
        {
            EstilosNivelVendedor(); //power_seller_status styles
            EstilosRatingVendedor(); //level_id styles (progress bar with colors of reputations)
        }

        private void EstilosNivelVendedor()
        {
            switch (NivelVendedorProd.Text)
            {
                case "silver":
                    NivelVendedorProd.TextColor = Color.Silver;
                    NivelVendedorProd.FontAttributes = FontAttributes.Bold;
                    break;
                case "gold":
                    NivelVendedorProd.TextColor = Color.GreenYellow;
                    NivelVendedorProd.FontAttributes = FontAttributes.Bold;
                    break;
                case "platinum":
                    NivelVendedorProd.TextColor = Color.Green;
                    NivelVendedorProd.FontAttributes = FontAttributes.Bold;
                    break;
                default:
                    break;
            }
        }

        private void EstilosRatingVendedor()
        {
            switch (RatingLevel.Text)
            {
                case "1":
                    ProgressBarRating.Progress = 20;
                    break;
                case "2":
                    ProgressBarRating.Progress = 40;
                    break;
                case "3":
                    ProgressBarRating.Progress = 60;
                    break;
                case "4":
                    ProgressBarRating.Progress = 80;
                    break;
                case "5":
                    ProgressBarRating.Progress = 100;
                    break;
                default:
                    break;
            }
        }

        private async void btnOK_Clicked(object sender, EventArgs e)
        {
            await PopupNavigation.Instance.PopAsync();
        }
    }
}