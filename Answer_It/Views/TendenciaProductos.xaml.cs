﻿using Answer_It.Models;
using Answer_It.ViewModels;
using Negocio;
using Syncfusion.SfRating.XForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Answer_It.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TendenciaProductos : ContentPage
    {
        RestClient client = new RestClient();
        List<ProductosViewModel> ListaTop10Productos = new List<ProductosViewModel>();
        List<Productos> ListaProductos = new List<Productos>();
        List<Valoraciones> ListaValoraciones = new List<Valoraciones>();
        List<ImagenesProductos> ListaImagenes = new List<ImagenesProductos>();

        public TendenciaProductos()
        {
            InitializeComponent();
            CargarPagina();
        }

        private async void CargarPagina()
        {
            Cargando.IsVisible = true;
            Cargando.IsBusy = true;
            await CargarValoraciones();
            await CargarProductos();
            await CargarRanking();
            Cargando.IsBusy = false;
            Cargando.IsVisible = false;
        }

        private async Task<bool> CargarRanking()
        {
            try
            {
                foreach (var item in ListaProductos)
                {
                    ListaTop10Productos.Add(new ProductosViewModel
                    {
                        id = item.id,
                        nombre = item.nombre,
                        valoracion = (double)CalcularValoracionPromedio(item.id),
                        cant_votos = item.cant_votos.GetValueOrDefault(0)
                    });
                }
                ListaTop10Productos = ListaTop10Productos.OrderByDescending(x => x.valoracion).Take(10).ToList();
                for (int i = 0; i < ListaTop10Productos.Count; i++)
                {
                    Label lblIdProd = new Label { Text = ListaTop10Productos[i].id.ToString(), IsVisible = false };
                    Label lblPos = new Label { Text = (i + 1).ToString(), TextColor = Color.White, FontAttributes = FontAttributes.Bold, VerticalTextAlignment = TextAlignment.Center };
                    Image imgProd = new Image { Source = ImageSource.FromFile(await CargarFotoProducto(ListaTop10Productos[i].id)), HeightRequest = 33, WidthRequest = 33, Margin = new Thickness(4, 0, 0, 0) };
                    Label lblNombreProd = new Label { Text = ListaTop10Productos[i].nombre, TextColor = Color.FromHex("#FF8000"), FontAttributes = FontAttributes.Bold, FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)), VerticalTextAlignment = TextAlignment.Center, Margin = new Thickness(4, 0, 0, 0) };
                    SfRating ratingProducto = new SfRating { Value = ListaTop10Productos[i].valoracion, ReadOnly = true, ItemSize = 15, HorizontalOptions = LayoutOptions.EndAndExpand, RatingSettings = new SfRatingSettings { RatedFill = Color.FromHex("#FF8000"), RatedStrokeWidth = 2, RatedStroke = Color.FromHex("#C06000") }, Precision = Precision.Exact };
                    Label lblPuntaje = new Label { Text = "(" + (Math.Truncate(100 * ListaTop10Productos[i].valoracion) / 100).ToString() + ")", TextColor = Color.White, FontSize = 8, VerticalTextAlignment = TextAlignment.Center, HorizontalOptions = LayoutOptions.EndAndExpand };
                    StackLayout stackPuntaje = new StackLayout { HorizontalOptions = LayoutOptions.EndAndExpand, Orientation = StackOrientation.Vertical, Spacing = 3 };
                    stackPuntaje.Children.Add(ratingProducto); stackPuntaje.Children.Add(lblPuntaje);

                    StackLayout stackProd = new StackLayout { Orientation = StackOrientation.Horizontal, Padding = 6 };
                    switch (i)
                    {
                        case 0:
                            lblNombreProd.TextColor = Color.White;
                            lblPuntaje.TextColor = Color.White;
                            stackProd.BackgroundColor = Color.DarkGoldenrod;
                            break;
                        case 1:
                            lblNombreProd.TextColor = Color.White;
                            lblPuntaje.TextColor = Color.White;
                            stackProd.BackgroundColor = Color.LightSlateGray;
                            break;
                        case 2:
                            lblNombreProd.TextColor = Color.White;
                            lblPuntaje.TextColor = Color.White;
                            stackProd.BackgroundColor = Color.SaddleBrown;
                            break;
                        default:
                            lblNombreProd.TextColor = Color.White;
                            stackProd.BackgroundColor = Color.FromHex("#5F5F5F");
                            break;
                    }

                    stackProd.Children.Add(lblIdProd);
                    stackProd.Children.Add(lblPos);
                    stackProd.Children.Add(imgProd);
                    stackProd.Children.Add(lblNombreProd);
                    stackProd.Children.Add(stackPuntaje);

                    var tapGestureRecognizerProducto = new TapGestureRecognizer();
                    tapGestureRecognizerProducto.Tapped += (sender, e) =>
                    {
                        Navigation.PushAsync(new ValoracionesProducto(Convert.ToInt32(lblIdProd.Text)));
                    };
                    stackProd.GestureRecognizers.Add(tapGestureRecognizerProducto);

                    StackRanking.Children.Add(stackProd);
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }           
        }

        private decimal CalcularValoracionPromedio(int idProd)
        {
            double sumaVals = 0;
            foreach (var vals in ListaValoraciones)
            {
                if (vals.id_producto == idProd)
                {
                    sumaVals += vals.puntaje;
                }
            }
            int cant = ListaValoraciones.Where(x => x.id_producto == idProd).Count();
            if (cant == 0)
            {
                return 0;
            }
            decimal promedio = (decimal)sumaVals / cant;
            return promedio;
        }

        private async Task<string> CargarFotoProducto(int id_prod)
        {
            ListaImagenes = await client.GetAll<ImagenesProductos>("http://www.recommenditws.somee.com/api/ImagenesProductosApi");
            ImagenesProductos imgProductoDb = ListaImagenes.Where(x => x.id_producto == id_prod).FirstOrDefault();
            var pathImgDescargada = imgProductoDb == null ? null : await App.DescargarImagenCloudinary(imgProductoDb.nombre_imagen);
            return imgProductoDb == null ? "product_default.png" : pathImgDescargada;
        }

        private async Task<bool> CargarProductos()
        {
            try
            {
                ListaProductos = await client.GetAll<Productos>("http://www.recommenditws.somee.com/api/ProductosApi");               
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private async Task<bool> CargarValoraciones()
        {
            try
            {
                ListaValoraciones = await client.GetAll<Valoraciones>("http://www.recommenditws.somee.com/api/ValoracionesApi");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}