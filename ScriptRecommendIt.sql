USE [RecommendIt]
GO
/****** Object:  Table [dbo].[DenunciasRecomendaciones]    Script Date: 12/2/2023 20:47:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DenunciasRecomendaciones](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_recomendacion] [int] NOT NULL,
	[id_usuario_denunciante] [int] NOT NULL,
	[id_usuario_denunciado] [int] NOT NULL,
	[motivo] [varchar](100) NOT NULL,
	[fecha_denuncia] [datetime] NULL,
	[reputacion_descontada] [int] NULL,
 CONSTRAINT [PK_DenunciasRecomendaciones] PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[id_recomendacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DenunciasValoraciones]    Script Date: 12/2/2023 20:47:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DenunciasValoraciones](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_valoracion] [int] NOT NULL,
	[id_usuario_denunciante] [int] NOT NULL,
	[id_usuario_denunciado] [int] NOT NULL,
	[motivo] [varchar](100) NOT NULL,
	[fecha_denuncia] [datetime] NULL,
	[reputacion_descontada] [int] NULL,
 CONSTRAINT [PK_DenunciasValoraciones_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ImagenesProductos]    Script Date: 12/2/2023 20:47:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ImagenesProductos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre_imagen] [varchar](50) NOT NULL,
	[id_producto] [int] NOT NULL,
 CONSTRAINT [PK_ImagenesProductos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ImagenesUsuarios]    Script Date: 12/2/2023 20:47:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ImagenesUsuarios](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre_imagen] [varchar](50) NOT NULL,
	[id_usuario] [int] NOT NULL,
 CONSTRAINT [PK_ImagenesUsuarios] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NecesidadesRec]    Script Date: 12/2/2023 20:47:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NecesidadesRec](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre_necesidad] [varchar](100) NOT NULL,
	[id_solicitud_recomendacion] [int] NOT NULL,
	[es_necesidad_primaria] [bit] NOT NULL,
 CONSTRAINT [PK_NecesidadesRec] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NivelesBeneficios]    Script Date: 12/2/2023 20:47:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NivelesBeneficios](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nivel] [int] NOT NULL,
	[reputacion_necesaria] [int] NOT NULL,
	[beneficios] [nvarchar](300) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Notificaciones]    Script Date: 12/2/2023 20:47:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notificaciones](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_usuario_origen] [int] NOT NULL,
	[id_usuario_destino] [int] NOT NULL,
	[descripcion] [nvarchar](200) NOT NULL,
	[leida] [bit] NOT NULL,
	[id_tipo] [int] NOT NULL,
	[id_publicacion] [int] NULL,
	[fecha] [datetime] NULL,
 CONSTRAINT [PK_Notificaciones] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NotificacionesTipo]    Script Date: 12/2/2023 20:47:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificacionesTipo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tipo] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_NotificacionesTipo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Productos]    Script Date: 12/2/2023 20:47:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Productos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](100) NOT NULL,
	[valoracion] [decimal](2, 0) NULL,
	[cant_votos] [int] NULL,
	[fecha_creacion] [date] NULL,
 CONSTRAINT [PK_Productos] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Recomendaciones]    Script Date: 12/2/2023 20:47:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Recomendaciones](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[contenido] [nvarchar](max) NOT NULL,
	[id_usuario] [int] NOT NULL,
	[id_solicitud_recomendacion] [int] NOT NULL,
	[me_gusta] [int] NOT NULL,
	[eliminado] [bit] NOT NULL,
	[fecha] [datetime] NULL,
	[id_producto_recomendado] [int] NOT NULL,
 CONSTRAINT [PK_Recomendaciones] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 12/2/2023 20:47:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SolicitudRecomendaciones]    Script Date: 12/2/2023 20:47:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SolicitudRecomendaciones](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[titulo] [nvarchar](max) NOT NULL,
	[leyenda] [nvarchar](256) NOT NULL,
	[id_usuario] [int] NOT NULL,
	[visitas] [int] NULL,
	[eliminado] [bit] NOT NULL,
	[fecha] [datetime] NULL,
	[reputacion_otorgada] [int] NOT NULL,
	[sin_skill] [bit] NOT NULL,
	[skill_prisa] [bit] NOT NULL,
	[skill_destacado] [bit] NOT NULL,
	[nivel_requerido] [int] NULL,
	[contenido] [nvarchar](max) NULL,
	[tipo_producto] [nvarchar](300) NULL,
 CONSTRAINT [PK_SolicitudRecomendaciones] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SolicitudValoraciones]    Script Date: 12/2/2023 20:47:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SolicitudValoraciones](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[leyenda] [nvarchar](256) NOT NULL,
	[id_usuario] [int] NOT NULL,
	[visitas] [int] NULL,
	[eliminado] [bit] NOT NULL,
	[fecha] [datetime] NULL,
	[cant_recomendaciones] [int] NOT NULL,
	[reputacion_otorgada] [int] NOT NULL,
	[sin_skill] [bit] NOT NULL,
	[skill_prisa] [bit] NOT NULL,
	[skill_destacado] [bit] NOT NULL,
	[id_producto] [int] NOT NULL,
	[aspecto_especial] [nvarchar](200) NULL,
	[r_points_recompensa] [int] NULL,
 CONSTRAINT [PK_Preguntas] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 12/2/2023 20:47:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[email] [nvarchar](256) NULL,
	[nombre_usuario] [nvarchar](256) NOT NULL,
	[contrasena] [nvarchar](256) NOT NULL,
	[apellido] [nvarchar](max) NOT NULL,
	[nombre] [nvarchar](256) NOT NULL,
	[id_rol] [int] NOT NULL,
	[pais] [nvarchar](256) NULL,
	[localidad] [nvarchar](256) NULL,
	[reputacion] [int] NOT NULL,
	[nivel] [int] NOT NULL,
	[eliminado] [bit] NOT NULL,
	[fecha_registro] [datetime] NULL,
	[r_points] [int] NULL,
	[visitas_al_perfil] [int] NULL,
	[votos_positivos] [int] NULL,
	[votos_negativos] [int] NULL,
	[es_verificado] [bit] NULL,
	[edad] [int] NULL,
 CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Valoraciones]    Script Date: 12/2/2023 20:47:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Valoraciones](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[titulo] [nvarchar](100) NOT NULL,
	[contenido] [nvarchar](max) NOT NULL,
	[id_usuario] [int] NOT NULL,
	[id_producto] [int] NOT NULL,
	[puntaje] [int] NOT NULL,
	[eliminado] [bit] NOT NULL,
	[fecha] [datetime] NULL,
	[me_gusta] [int] NOT NULL,
 CONSTRAINT [PK_Valoraciones] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ValoracionesMeGusta]    Script Date: 12/2/2023 20:47:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ValoracionesMeGusta](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_valoracion] [int] NOT NULL,
	[id_usuario_like] [int] NOT NULL,
	[me_gusta] [bit] NOT NULL,
	[no_me_gusta] [bit] NOT NULL,
 CONSTRAINT [PK_ValoracionesMeGusta_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DenunciasRecomendaciones]  WITH CHECK ADD  CONSTRAINT [FK_DenunciasRecomendaciones_Recomendaciones] FOREIGN KEY([id_recomendacion])
REFERENCES [dbo].[Recomendaciones] ([id])
GO
ALTER TABLE [dbo].[DenunciasRecomendaciones] CHECK CONSTRAINT [FK_DenunciasRecomendaciones_Recomendaciones]
GO
ALTER TABLE [dbo].[DenunciasRecomendaciones]  WITH CHECK ADD  CONSTRAINT [FK_DenunciasRecomendaciones_Usuarios1] FOREIGN KEY([id_usuario_denunciante])
REFERENCES [dbo].[Usuarios] ([id])
GO
ALTER TABLE [dbo].[DenunciasRecomendaciones] CHECK CONSTRAINT [FK_DenunciasRecomendaciones_Usuarios1]
GO
ALTER TABLE [dbo].[DenunciasRecomendaciones]  WITH CHECK ADD  CONSTRAINT [FK_DenunciasRecomendaciones_Usuarios2] FOREIGN KEY([id_usuario_denunciado])
REFERENCES [dbo].[Usuarios] ([id])
GO
ALTER TABLE [dbo].[DenunciasRecomendaciones] CHECK CONSTRAINT [FK_DenunciasRecomendaciones_Usuarios2]
GO
ALTER TABLE [dbo].[DenunciasValoraciones]  WITH CHECK ADD  CONSTRAINT [FK_DenunciasValoraciones_Usuarios1] FOREIGN KEY([id_usuario_denunciante])
REFERENCES [dbo].[Usuarios] ([id])
GO
ALTER TABLE [dbo].[DenunciasValoraciones] CHECK CONSTRAINT [FK_DenunciasValoraciones_Usuarios1]
GO
ALTER TABLE [dbo].[DenunciasValoraciones]  WITH CHECK ADD  CONSTRAINT [FK_DenunciasValoraciones_Usuarios2] FOREIGN KEY([id_usuario_denunciado])
REFERENCES [dbo].[Usuarios] ([id])
GO
ALTER TABLE [dbo].[DenunciasValoraciones] CHECK CONSTRAINT [FK_DenunciasValoraciones_Usuarios2]
GO
ALTER TABLE [dbo].[DenunciasValoraciones]  WITH CHECK ADD  CONSTRAINT [FK_DenunciasValoraciones_Valoraciones] FOREIGN KEY([id_valoracion])
REFERENCES [dbo].[Valoraciones] ([id])
GO
ALTER TABLE [dbo].[DenunciasValoraciones] CHECK CONSTRAINT [FK_DenunciasValoraciones_Valoraciones]
GO
ALTER TABLE [dbo].[ImagenesProductos]  WITH CHECK ADD  CONSTRAINT [FK_ImagenesProductos_Productos] FOREIGN KEY([id_producto])
REFERENCES [dbo].[Productos] ([id])
GO
ALTER TABLE [dbo].[ImagenesProductos] CHECK CONSTRAINT [FK_ImagenesProductos_Productos]
GO
ALTER TABLE [dbo].[ImagenesUsuarios]  WITH CHECK ADD  CONSTRAINT [FK_ImagenesUsuarios_Usuarios] FOREIGN KEY([id_usuario])
REFERENCES [dbo].[Usuarios] ([id])
GO
ALTER TABLE [dbo].[ImagenesUsuarios] CHECK CONSTRAINT [FK_ImagenesUsuarios_Usuarios]
GO
ALTER TABLE [dbo].[NecesidadesRec]  WITH CHECK ADD  CONSTRAINT [FK_NecesidadesRec_SolicitudRecomendaciones] FOREIGN KEY([id_solicitud_recomendacion])
REFERENCES [dbo].[SolicitudRecomendaciones] ([id])
GO
ALTER TABLE [dbo].[NecesidadesRec] CHECK CONSTRAINT [FK_NecesidadesRec_SolicitudRecomendaciones]
GO
ALTER TABLE [dbo].[Notificaciones]  WITH CHECK ADD  CONSTRAINT [FK_Notificaciones_NotificacionesTipo] FOREIGN KEY([id_tipo])
REFERENCES [dbo].[NotificacionesTipo] ([id])
GO
ALTER TABLE [dbo].[Notificaciones] CHECK CONSTRAINT [FK_Notificaciones_NotificacionesTipo]
GO
ALTER TABLE [dbo].[Notificaciones]  WITH CHECK ADD  CONSTRAINT [FK_Notificaciones_Usuarios] FOREIGN KEY([id_usuario_origen])
REFERENCES [dbo].[Usuarios] ([id])
GO
ALTER TABLE [dbo].[Notificaciones] CHECK CONSTRAINT [FK_Notificaciones_Usuarios]
GO
ALTER TABLE [dbo].[Notificaciones]  WITH CHECK ADD  CONSTRAINT [FK_Notificaciones_Usuarios2] FOREIGN KEY([id_usuario_destino])
REFERENCES [dbo].[Usuarios] ([id])
GO
ALTER TABLE [dbo].[Notificaciones] CHECK CONSTRAINT [FK_Notificaciones_Usuarios2]
GO
ALTER TABLE [dbo].[Recomendaciones]  WITH CHECK ADD  CONSTRAINT [FK_Recomendaciones_Productos] FOREIGN KEY([id_producto_recomendado])
REFERENCES [dbo].[Productos] ([id])
GO
ALTER TABLE [dbo].[Recomendaciones] CHECK CONSTRAINT [FK_Recomendaciones_Productos]
GO
ALTER TABLE [dbo].[Recomendaciones]  WITH CHECK ADD  CONSTRAINT [FK_Recomendaciones_SolicitudRecomendaciones] FOREIGN KEY([id_solicitud_recomendacion])
REFERENCES [dbo].[SolicitudRecomendaciones] ([id])
GO
ALTER TABLE [dbo].[Recomendaciones] CHECK CONSTRAINT [FK_Recomendaciones_SolicitudRecomendaciones]
GO
ALTER TABLE [dbo].[Recomendaciones]  WITH CHECK ADD  CONSTRAINT [FK_Recomendaciones_Usuarios] FOREIGN KEY([id_usuario])
REFERENCES [dbo].[Usuarios] ([id])
GO
ALTER TABLE [dbo].[Recomendaciones] CHECK CONSTRAINT [FK_Recomendaciones_Usuarios]
GO
ALTER TABLE [dbo].[SolicitudRecomendaciones]  WITH CHECK ADD  CONSTRAINT [FK_SolicitudRecomendaciones_Usuarios] FOREIGN KEY([id_usuario])
REFERENCES [dbo].[Usuarios] ([id])
GO
ALTER TABLE [dbo].[SolicitudRecomendaciones] CHECK CONSTRAINT [FK_SolicitudRecomendaciones_Usuarios]
GO
ALTER TABLE [dbo].[SolicitudValoraciones]  WITH CHECK ADD  CONSTRAINT [FK_SolicitudValoraciones_Productos] FOREIGN KEY([id_producto])
REFERENCES [dbo].[Productos] ([id])
GO
ALTER TABLE [dbo].[SolicitudValoraciones] CHECK CONSTRAINT [FK_SolicitudValoraciones_Productos]
GO
ALTER TABLE [dbo].[SolicitudValoraciones]  WITH CHECK ADD  CONSTRAINT [FK_SolicitudValoraciones_Usuarios] FOREIGN KEY([id_usuario])
REFERENCES [dbo].[Usuarios] ([id])
GO
ALTER TABLE [dbo].[SolicitudValoraciones] CHECK CONSTRAINT [FK_SolicitudValoraciones_Usuarios]
GO
ALTER TABLE [dbo].[Usuarios]  WITH CHECK ADD  CONSTRAINT [FK_Usuarios_Roles] FOREIGN KEY([id_rol])
REFERENCES [dbo].[Roles] ([id])
GO
ALTER TABLE [dbo].[Usuarios] CHECK CONSTRAINT [FK_Usuarios_Roles]
GO
ALTER TABLE [dbo].[Valoraciones]  WITH CHECK ADD  CONSTRAINT [FK_Valoraciones_Productos] FOREIGN KEY([id_producto])
REFERENCES [dbo].[Productos] ([id])
GO
ALTER TABLE [dbo].[Valoraciones] CHECK CONSTRAINT [FK_Valoraciones_Productos]
GO
ALTER TABLE [dbo].[Valoraciones]  WITH CHECK ADD  CONSTRAINT [FK_Valoraciones_Usuarios] FOREIGN KEY([id_usuario])
REFERENCES [dbo].[Usuarios] ([id])
GO
ALTER TABLE [dbo].[Valoraciones] CHECK CONSTRAINT [FK_Valoraciones_Usuarios]
GO
ALTER TABLE [dbo].[ValoracionesMeGusta]  WITH CHECK ADD  CONSTRAINT [FK_ValoracionesMeGusta_Usuarios] FOREIGN KEY([id_usuario_like])
REFERENCES [dbo].[Usuarios] ([id])
GO
ALTER TABLE [dbo].[ValoracionesMeGusta] CHECK CONSTRAINT [FK_ValoracionesMeGusta_Usuarios]
GO
ALTER TABLE [dbo].[ValoracionesMeGusta]  WITH CHECK ADD  CONSTRAINT [FK_ValoracionesMeGusta_Valoraciones] FOREIGN KEY([id_valoracion])
REFERENCES [dbo].[Valoraciones] ([id])
GO
ALTER TABLE [dbo].[ValoracionesMeGusta] CHECK CONSTRAINT [FK_ValoracionesMeGusta_Valoraciones]
GO
